/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package api.tools.numbers;

import api.tools.text.StringUtil;

import javax.annotation.ParametersAreNonnullByDefault;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static com.google.common.base.Preconditions.checkNotNull;

@ParametersAreNonnullByDefault
public class NumberUtil {
    private static final NumberFormat nf = NumberFormat.getInstance(Locale.US);

    private NumberUtil() {
    }

    /**
     * @param val the String to parse
     * @return the parsed double
     * @throws IllegalArgumentException if the String does not contain a parseable double
     */
    public static double parseDouble(final String val) {
        try {
            return nf.parse(checkNotNull(val)).doubleValue();
        } catch (ParseException e) {
            throw new IllegalArgumentException("String could not be parsed as double: " + val);
        }
    }

    /**
     * @param val the String to parse, may include k's to signify *1000
     * @return the parsed double
     * @throws IllegalArgumentException if the String does not contain a parseable double
     */
    public static double parseDoubleWithK(final String val) {
        try {
            int power = StringUtil.countOccurance(checkNotNull(val.toLowerCase()), 'k');
            return Math.pow(1000, power) * nf.parse(val.replace("k", "")).doubleValue();
        } catch (ParseException e) {
            throw new IllegalArgumentException("String could not be parsed as double: " + val);
        }
    }

    /**
     * @param val the String to parse, may include k's to signify *1000 or m's to signify *1000000
     * @return the parsed double
     * @throws IllegalArgumentException if the String does not contain a parseable double
     */
    public static double parseDoubleWithKOrM(final String val) {
        try {
            if (val.toLowerCase().contains("k")) {
                int power = StringUtil.countOccurance(checkNotNull(val), 'k');
                return Math.pow(1000, power) * nf.parse(val.replace("k", "")).doubleValue();
            } else {
                int power = StringUtil.countOccurance(checkNotNull(val), 'm');
                return Math.pow(1000000, power) * nf.parse(val.replace("m", "")).doubleValue();
            }
        } catch (ParseException e) {
            throw new IllegalArgumentException("String could not be parsed as double: " + val);
        }
    }

    /**
     * @param val the String to parse
     * @return the parsed int
     * @throws IllegalArgumentException if the String does not contain a parseable int
     */
    public static int parseInt(final String val) {
        try {
            return nf.parse(checkNotNull(val)).intValue();
        } catch (ParseException e) {
            throw new IllegalArgumentException("String could not be parsed as int: " + val);
        }
    }

    /**
     * @param val the String to parse
     * @return the parsed long
     * @throws IllegalArgumentException if the String does not contain a parseable long
     */
    public static long parseLong(final String val) {
        try {
            return nf.parse(checkNotNull(val)).longValue();
        } catch (ParseException e) {
            throw new IllegalArgumentException("String could not be parsed as long: " + val);
        }
    }

    public static int parseWordNumber(final String val) {
        int result = 0;
        int finalResult = 0;
        List<String> allowedStrings = Arrays.asList
                (
                        "zero", "one", "two", "three", "four", "five", "six", "seven",
                        "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen",
                        "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty",
                        "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety",
                        "hundred", "thousand", "million", "billion", "trillion"
                );

        String input = val;
        if (input != null && input.length() > 0) {
            input = input.replaceAll("-", " ");
            input = input.toLowerCase().replaceAll(" and", " ");
            String[] splitParts = input.trim().split("\\s+");

            for (String str : splitParts) {
                if (!allowedStrings.contains(str)) {
                    throw new IllegalArgumentException("Invalid word found: " + val);
                }
            }
            for (String str : splitParts) {
                if (str.equalsIgnoreCase("zero")) {
                    result += 0;
                } else if (str.equalsIgnoreCase("one")) {
                    result += 1;
                } else if (str.equalsIgnoreCase("two")) {
                    result += 2;
                } else if (str.equalsIgnoreCase("three")) {
                    result += 3;
                } else if (str.equalsIgnoreCase("four")) {
                    result += 4;
                } else if (str.equalsIgnoreCase("five")) {
                    result += 5;
                } else if (str.equalsIgnoreCase("six")) {
                    result += 6;
                } else if (str.equalsIgnoreCase("seven")) {
                    result += 7;
                } else if (str.equalsIgnoreCase("eight")) {
                    result += 8;
                } else if (str.equalsIgnoreCase("nine")) {
                    result += 9;
                } else if (str.equalsIgnoreCase("ten")) {
                    result += 10;
                } else if (str.equalsIgnoreCase("eleven")) {
                    result += 11;
                } else if (str.equalsIgnoreCase("twelve")) {
                    result += 12;
                } else if (str.equalsIgnoreCase("thirteen")) {
                    result += 13;
                } else if (str.equalsIgnoreCase("fourteen")) {
                    result += 14;
                } else if (str.equalsIgnoreCase("fifteen")) {
                    result += 15;
                } else if (str.equalsIgnoreCase("sixteen")) {
                    result += 16;
                } else if (str.equalsIgnoreCase("seventeen")) {
                    result += 17;
                } else if (str.equalsIgnoreCase("eighteen")) {
                    result += 18;
                } else if (str.equalsIgnoreCase("nineteen")) {
                    result += 19;
                } else if (str.equalsIgnoreCase("twenty")) {
                    result += 20;
                } else if (str.equalsIgnoreCase("thirty")) {
                    result += 30;
                } else if (str.equalsIgnoreCase("forty")) {
                    result += 40;
                } else if (str.equalsIgnoreCase("fifty")) {
                    result += 50;
                } else if (str.equalsIgnoreCase("sixty")) {
                    result += 60;
                } else if (str.equalsIgnoreCase("seventy")) {
                    result += 70;
                } else if (str.equalsIgnoreCase("eighty")) {
                    result += 80;
                } else if (str.equalsIgnoreCase("ninety")) {
                    result += 90;
                } else if (str.equalsIgnoreCase("hundred")) {
                    result *= 100;
                } else if (str.equalsIgnoreCase("thousand")) {
                    result *= 1000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("million")) {
                    result *= 1000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("billion")) {
                    result *= 1000000000;
                    finalResult += result;
                    result = 0;
                } else if (str.equalsIgnoreCase("trillion")) {
                    result *= 1000000000000L;
                    finalResult += result;
                    result = 0;
                }
            }

            finalResult += result;
            }
        return finalResult;
        }
    }
