package api.database;

import api.database.transactions.Transactional;
import api.tools.database.DBUtil;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.CriteriaSpecification;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class GenericDAO {

    private final Provider<Session> sessionProvider;

    @Inject
    public GenericDAO(final Provider<Session> sessionProvider) {
        this.sessionProvider = checkNotNull(sessionProvider);
    }

    @Transactional
    public void delete(final Object object) {
        delete(Lists.newArrayList(checkNotNull(object)));
    }

    @Transactional
    public void delete(final Collection<Object> objects) {
        try {
            for (final Object object : objects) {
                sessionProvider.get().delete(object);
            }
        } catch (Exception e) {
            throw new DBException(e);
        }
    }

    @Transactional
    public void deleteAll(final Class<?> type) {
        try {
            sessionProvider.get().createQuery("delete from " + type.getSimpleName()).executeUpdate();
        } catch (Exception e) {
            throw new DBException(e);
        }
    }

    @Transactional
    @Nullable
    public <T> T get(final Class<T> clazz, @Nullable final Object... criterion) {
        List<T> list = find(checkNotNull(clazz), criterion);
        return list == null || list.isEmpty() ? null : list.get(0);
    }

    @Transactional
    public <T> List<T> find(final Class<T> clazz, @Nullable final Object... criterion) {
        try {
            Criteria criteria = sessionProvider.get().createCriteria(checkNotNull(clazz)).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY);
            criteria = DBUtil.resolveAndAddCriterion(criteria, criterion);
            return listAndCast(clazz, criteria);
        } catch (Exception e) {
            throw new DBException(e);
        }
    }


    @SuppressWarnings("unchecked")
    private static <T> List<T> listAndCast(final Class<T> clazz, final Criteria criteria) {
        try {
            return criteria.list();
        } catch (HibernateException e) {
            throw new DBException(e);
        }
    }

    @Transactional
    public void save(final Object object) {
        try {
            sessionProvider.get().saveOrUpdate(checkNotNull(object));
        } catch (Exception e) {
            throw new DBException(e);
        }
    }

    @Transactional
    public void save(final Collection<Object> objects) {
        for (final Object object : objects) {
            save(object);
        }
    }

}
