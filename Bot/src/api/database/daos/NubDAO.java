/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package api.database.daos;

import api.database.AbstractDAO;
import api.database.transactions.Transactional;
import com.google.inject.Provider;
import api.database.models.Nub;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

@ParametersAreNonnullByDefault
public class NubDAO extends AbstractDAO<Nub> {
    @Inject
    public NubDAO(final Provider<Session> sessionProvider) {
        super(Nub.class, sessionProvider);
    }

    /**
     * @return nub object of the current nub
     */
    @Transactional
    @Nullable
    public Nub getNub() {
        List<Nub> nubs = find();
        if (nubs.size() == 0) return null;
        return nubs.get(nubs.size() - 1);
    }

    /**
     * @return nub object of the specified nub id
     */
    @Transactional
    @Nullable
    public Nub getNubById(final Long id) {
        return get(Restrictions.idEq(id));
    }

    /**
     * @return A nub object of the last time the specified user was nub
     */
    @Transactional
    @Nullable
    public Nub getUserLastNub(final String user) {
        List<Nub> nubs = find(Restrictions.eq("nubName", user));
        if (nubs.size() == 0) return null;
        return nubs.get(nubs.size() - 1);
    }

    /**
     * @return A list of nub objects of the named nub
     */
    @Transactional
    @Nullable
    public List<Nub> getUser(final String user) {
        return find(Restrictions.eq("nubName", user));
    }

    /**
     * @return A list of all nub objects
     */
    @Transactional
    @Nullable
    public List<Nub> getAllNubs() {
        return find();
    }

    @Transactional
    public Nub createNub(final String user, final String reason, final String setter) {
            Nub nub = new Nub(user, reason, setter, new Date());
            super.save(nub);
        return nub;
    }
}