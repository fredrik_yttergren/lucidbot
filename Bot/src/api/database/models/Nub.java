/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package api.database.models;

import java.util.Date;
import api.common.HasNumericId;

import lombok.AccessLevel;
import lombok.*;
import javax.persistence.*;


@Entity
@Table(name = "nub")
@NoArgsConstructor
@EqualsAndHashCode(of = "nubName")
@Getter
@Setter
public final class Nub implements HasNumericId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    /**
     * The bot user in question
     */
    @Column(name = "nub_name", updatable = false, nullable = false, length = 50)
    private String nubName;

    /**
     * Reason that said user in a nub
     */
    @Column(name = "reason", length = 5000)
    private String reason;

    /**
     * Person who set the nub status for the user
     */
    @Column(name = "setter", length = 50)
    private String setter;

    /**
     * DateTime when the user was set as nub
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "set_time")
    private Date setTime;

    public Nub(final String nubName, final String reason, final String setter, final Date setTime) {
        this.nubName = nubName;
        this.reason = reason;
        this.setter = setter;
        this.setTime = setTime;
    }
}
