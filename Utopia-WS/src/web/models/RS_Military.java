package web.models;

import api.common.HasNumericId;
import com.sun.jersey.server.linking.Ref;
import database.models.Military;
import web.tools.ISODateTimeAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.URI;
import java.util.Date;

@XmlRootElement(name = "Military")
@XmlType(name = "Military")
@XmlAccessorType(XmlAccessType.FIELD)
public class RS_Military implements HasNumericId {
    @XmlElement(name = "ID")
    private Long id;

    @Ref("intel/military/{id}")
    @XmlElement(name = "Link")
    private URI link;

    @XmlElement(required = true, name = "Province")
    private RS_Province province;

    @XmlElement(name = "SpecCredits")
    private Integer specCredits;

    @XmlElement(name = "WageRate")
    private Integer wageRate;

    @XmlElement(name = "DraftTarget")
    private Integer draftTarget;

    @XmlElement(name = "DraftRate")
    private Double draftRate;

    @XmlJavaTypeAdapter(ISODateTimeAdapter.class)
    @XmlElement(name = "LastUpdated")
    private Date lastUpdated;

    @XmlElement(name = "SavedBy")
    private String savedBy;

    public RS_Military() {
    }

    private RS_Military(final Long id, final RS_Province province, final Date lastUpdated) {
        this.id = id;
        this.province = province;
        this.lastUpdated = lastUpdated;
    }

    private RS_Military(final Military military) {
        this(military.getId(), RS_Province.fromProvince(military.getProvince(), false), military.getLastUpdated());
        this.specCredits = military.getSpecCredits();
        this.wageRate = military.getWageRate();
        this.draftTarget = military.getDraftTarget();
        this.draftRate = military.getDraftRate();
    }

    public static RS_Military fromMilitary(final Military military, final boolean full) {
        return full ? new RS_Military(military) : new RS_Military(military.getId(), RS_Province.fromProvince(military.getProvince(), false), military.getLastUpdated());
    }

    public Long getId() {
        return id;
    }

    public RS_Province getProvince() {
        return province;
    }

    public Integer getSpecCredits() {
        return specCredits;
    }

    public Integer getWageRate() {
        return wageRate;
    }

    public Integer getDraftTarget() {
        return draftTarget;
    }

    public Double getDraftRate() {
        return draftRate;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public String getSavedBy() {
        return savedBy;
    }

}
