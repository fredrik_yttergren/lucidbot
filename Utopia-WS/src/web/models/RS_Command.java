/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package web.models;

import api.commands.Command;
import api.common.HasNumericId;
import api.database.models.AccessLevel;
import com.sun.jersey.server.linking.Ref;
import database.models.HelpTopic;
import database.models.HelpTopicCollection;
import org.hibernate.validator.constraints.NotEmpty;
import tools.validation.ExistsInDB;
import web.validation.Add;
import web.validation.Update;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;

@XmlRootElement(name = "Command")
@XmlAccessorType(XmlAccessType.FIELD)
public class RS_Command {
    /**
     * The name of the command, which is also the primary way to invoke it
     */
    @XmlElement(name = "name")
    private String name;

    /**
     * A description of how to use the command
     */
    @XmlElement(name = "syntax")
    private String syntax = "";

    /**
     * A help text for the command, should describe what it does
     */
    @XmlElement(name = "helpText")
    private String helpText = "";

    /**
     * The type of command this is. Useful for grouping commands together. Defaults to 'unspecified'
     */
    @XmlElement(name = "commandType")
    private String commandType = "unspecified";

    /**
     * The lowest access level required to use this command
     */
    @XmlElement(name = "requiredAccessLevel")
    private AccessLevel requiredAccessLevel = AccessLevel.USER;

    public RS_Command() {
    }

    public RS_Command(final String name, final String syntax, final String helpText, final String commandType,
                      final AccessLevel requiredAccessLevel) {
        this.name = name;
        this.syntax = syntax;
        this.helpText = helpText;
        this.commandType = commandType;
        this.requiredAccessLevel = requiredAccessLevel;
    }

    public static RS_Command fromCommand(final Command command) {
        return new RS_Command(command.getName(), command.getSyntax(), command.getHelpText(),
                command.getCommandType(), command.getRequiredAccessLevel());
    }

    public String getName() {
        return name;
    }

    public String getSyntax() {
        return syntax;
    }

    public String getHelpText() {
        return helpText;
    }
    public String commandType() {
        return commandType;
    }
    public AccessLevel requiredAccessLevel() {
        return requiredAccessLevel;
    }
}
