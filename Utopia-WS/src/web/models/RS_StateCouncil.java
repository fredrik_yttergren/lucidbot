/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package web.models;

import api.common.HasNumericId;
import com.sun.jersey.server.linking.Ref;
import database.models.SoT;
import database.models.StateCouncil;
import web.tools.ISODateTimeAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.URI;
import java.util.Date;

import static api.tools.text.StringUtil.isNullOrEmpty;

@XmlRootElement(name = "StateCouncil")
@XmlType(name = "StateCouncil")
@XmlAccessorType(XmlAccessType.FIELD)
public class RS_StateCouncil implements HasNumericId {
    @XmlElement(name = "ID")
    private Long id;

    @Ref("intel/state_council/{id}")
    @XmlElement(name = "Link")
    private URI link;

    @XmlElement(required = true, name = "Province")
    private RS_Province province;

    @XmlElement(name = "MaxPopulation")
    private Integer maxPopulation;

    @XmlElement(name = "TotalPopulation")
    private Integer totalPopulation;

    @XmlElement(name = "UnemployedPeasants")
    private Integer unemployedPeasants;

    @XmlElement(name = "UnfilledJobs")
    private Integer unfilledJobs;

    @XmlElement(name = "Employment")
    private Integer employment;

    @XmlElement(name = "Income")
    private Integer income;

    @XmlElement(name = "Wages")
    private Integer wages;

    @XmlElement(name = "DraftCost")
    private Integer draftCost;

    @XmlElement(name = "Honor")
    private Integer honor;

    @XmlElement(name = "NetIncome")
    private Integer netIncome;

    @XmlElement(name = "PeasantsBorn")
    private Integer peasantsBorn;

    @XmlElement(name = "FoodGrown")
    private Integer foodGrown;

    @XmlElement(name = "FoodNeeded")
    private Integer foodNeeded;

    @XmlElement(name = "FoodDecayed")
    private Integer foodDecayed;

    @XmlElement(name = "netFood")
    private Integer netFood;

    @XmlElement(name = "RunesProduced")
    private Integer runesProduced;

    @XmlElement(name = "RunesDecayed")
    private Integer runesDecayed;

    @XmlElement(name = "NetRunes")
    private Integer netRunes;

    @XmlJavaTypeAdapter(ISODateTimeAdapter.class)
    @XmlElement(name = "LastUpdated")
    private Date lastUpdated;

    @XmlElement(name = "SavedBy")
    private String savedBy;

    public RS_StateCouncil() {
    }

    private RS_StateCouncil(final Long id, final RS_Province province, final Date lastUpdated) {
        this.id = id;
        this.province = province;
        this.lastUpdated = lastUpdated;
    }

    private RS_StateCouncil(final StateCouncil sc) {
        this(sc.getId(), RS_Province.fromProvince(sc.getProvince(), false), sc.getLastUpdated());
        this.maxPopulation = sc.getMaxPopulation();
        this.totalPopulation = sc.getTotalPopulation();
        this.unemployedPeasants = sc.getUnemployedPeasants();
        this.unfilledJobs = sc.getUnfilledJobs();
        this.employment = sc.getEmployment();
        this.income = sc.getIncome();
        this.wages = sc.getWages();
        this.draftCost = sc.getDraftCost();
        this.honor = sc.getHonor();
        this.netIncome = sc.getNetIncome();
        this.peasantsBorn = sc.getPeasantsBorn();
        this.foodGrown = sc.getFoodGrown();
        this.foodNeeded = sc.getFoodNeeded();
        this.foodDecayed = sc.getFoodDecayed();
        this.netFood = sc.getNetFood();
        this.runesProduced = sc.getRunesProduced();
        this.runesDecayed = sc.getRunesDecayed();
        this.netRunes = sc.getNetRunes();
    }

    public static RS_StateCouncil fromStateCouncil(final StateCouncil sc, final boolean full) {
        return full ? new RS_StateCouncil(sc) : new RS_StateCouncil(sc.getId(), RS_Province.fromProvince(sc.getProvince(), false), sc.getLastUpdated());
    }

    public Long getId() {
        return id;
    }

    public RS_Province getProvince() {
        return province;
    }

    public Integer getMaxPopulation() {
        return maxPopulation;
    }

    public Integer getTotalPopulation() {
        return totalPopulation;
    }

    public Integer getUnemployedPeasants() {
        return unemployedPeasants;
    }

    public Integer getUnfilledJobs() {
        return unfilledJobs;
    }

    public Integer getEmployment() {
        return employment;
    }

    public Integer getIncome() {
        return income;
    }

    public Integer getWages() {
        return wages;
    }

    public Integer getDraftCost() { return draftCost; }

    public Integer getHonor() {
        return honor;
    }

    public Integer getNetIncome() {
        return netIncome;
    }

    public Integer getPeasantsBorn() {
        return peasantsBorn;
    }

    public Integer getFoodGrown() {
        return foodGrown;
    }

    public Integer getFoodNeeded() {
        return foodNeeded;
    }

    public Integer getFoodDecayed() {
        return foodDecayed;
    }

    public Integer getNetFood() {
        return netFood;
    }

    public Integer getRunesProduced() {
        return runesProduced;
    }

    public Integer getRunesDecayed() {
        return runesDecayed;
    }

    public Integer getNetRunes() {
        return netRunes;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public String getSavedBy() {
        return savedBy;
    }

}
