package web.models;

import api.common.HasNumericId;
import com.sun.jersey.server.linking.Ref;
import database.models.Record;
import web.tools.ISODateTimeAdapter;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.URI;

import static api.tools.text.StringUtil.isNullOrEmpty;

@XmlRootElement(name = "Record")
@XmlType(name = "Record")
@XmlAccessorType(XmlAccessType.FIELD)
public class RS_Record implements HasNumericId {
    @XmlElement(name = "ID")
    private Long id;

    @Ref("record/{id}")
    @XmlElement(name = "Link")
    private URI link;

    @XmlElement(name = "UserName")
    private String userName;

    @XmlElement(name = "RecordName")
    private String recordName;

    @XmlElement(name = "RecordValue")
    private Integer recordValue;

    @XmlElement(name = "AllTimeRecord")
    private Boolean allTimeRecord;

    public RS_Record() {
    }

    private RS_Record(final Long id, final String userName, final String recordName,
                      final int recordValue, final boolean allTimeRecord) {
        this.id = id;
        this.userName = userName;
        this.recordName = recordName;
        this.recordValue = recordValue;
        this.allTimeRecord = allTimeRecord;
    }

    public static RS_Record fromRecord(final Record record) {
        return new RS_Record(record.getId(), record.getUserName(), record.getRecordName(),
                record.getRecordValue(), record.getAllTimeRecord());
    }

    public Long getId() {
        return id;
    }

    public String getUserName() { return userName; }

    public String getRecordName() { return recordName; }

    public int getRecordValue() { return recordValue; }

    public boolean getAllTimeRecord() { return allTimeRecord; }

}