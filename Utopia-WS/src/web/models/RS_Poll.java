package web.models;

import com.sun.jersey.server.linking.Ref;
import database.models.Poll;
import database.models.PollOption;
import org.hibernate.validator.constraints.NotEmpty;
import tools.validation.ValidBindings;
import web.tools.ISODateTimeAdapter;
import web.validation.Add;
import web.validation.Update;

import javax.validation.Valid;
import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Poll")
@XmlAccessorType(XmlAccessType.FIELD)
public class RS_Poll {
    /**
     * The id for this poll. The id is set by the database, so clients will only use it in the URL's.
     * <p/>
     * The server will simply ignore this value if you send it in with some request.
     */
    @XmlElement(name = "ID")
    private Long id;

    /**
     * A convenience link to this entity. Only used for navigation.
     * <p/>
     * The server will simply ignore this value if you send it in with some request.
     */
    @Ref("polls/{id}")
    @XmlElement(name = "Link")
    private URI link;

    /**
     * The poll description.
     * <p/>
     * This value is updatable, so it will always be overwritten when you do update calls. I.e. it always has to be specified, regardless of it's an
     * update or add request.
     */
    @NotEmpty(message = "The poll description may not be null or empty", groups = {Add.class, Update.class})
    @XmlElement(required = true, name = "Description")
    private String description;

    /**
     * The poll options available to vote for in this poll.
     * <p/>
     * This value has to be specified on add, but is otherwise not updatable via the poll object itself.
     */
    @NotEmpty(message = "You must specify poll options when adding a poll", groups = Add.class)
    @Valid
    @XmlElementWrapper(name = "PollOptions")
    @XmlElement(name = "PollOption")
    private List<RS_PollOption> pollOptions;

    /**
     * Whether the poll is closed or not.
     * <p/>
     * This value is updatable, so it will always be overwritten when you do update calls. On add calls it's ignored however.
     */
    @NotEmpty(message = "You must specify if the poll is closed or not", groups = Update.class)
    @XmlElement(name = "Closed")
    private Boolean closed;

    /**
     * The date and time at which the poll was first added.
     * <p/>
     * This value is not updatable and is set automatically on add operations, so it never needs to be specified.
     */
    @XmlJavaTypeAdapter(ISODateTimeAdapter.class)
    @XmlElement(name = "Added")
    private Date added;

    /**
     * The nick of the user that first added the poll.
     * <p/>
     * This value is not updatable and is set automatically on add operations, so it never needs to be specified.
     */
    @XmlElement(name = "AddedBy")
    private String addedBy;

    /**
     * Bindings for the poll, allowing you to specify which users (as an example) the event is for.
     * <p/>
     * Not updatable, so it will be ignored for update operations. Not mandatory for add operations either.
     */
    @ValidBindings(message = "Invalid bindings", nillable = true, groups = Add.class)
    @XmlElement(name = "Bindings")
    private RS_Bindings bindings;

    public RS_Poll() {
    }

    public RS_Poll(final Long id,
                   final String description,
                   final List<RS_PollOption> pollOptions,
                   final boolean closed,
                   final Date added,
                   final String addedBy,
                   final RS_Bindings bindings) {
        this.id = id;
        this.description = description;
        this.pollOptions = pollOptions;
        this.closed = closed;
        this.added = added;
        this.addedBy = addedBy;
        this.bindings = bindings;
    }

    public static RS_Poll fromPoll(final Poll poll) {
        List<RS_PollOption> pollOptions = new ArrayList<>(poll.getPollOptions().size());
        for (PollOption pollOption : poll.getPollOptions()) {
            pollOptions.add(RS_PollOption.fromPollOption(pollOption));
        }
        return new RS_Poll(poll.getId(), poll.getDescription(), pollOptions, poll.isClosed(), poll.getAdded(), poll.getAddedBy(),
                RS_Bindings.fromBindings(poll.getBindings()));
    }

    public Long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public List<RS_PollOption> getPollOptions() {
        return pollOptions;
    }

    public Boolean isClosed() {
        return closed;
    }

    public Date getAdded() {
        return added;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public RS_Bindings getBindings() {
        return bindings;
    }
}
