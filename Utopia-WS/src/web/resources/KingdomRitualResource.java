package web.resources;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import lombok.extern.log4j.Log4j;
import tools.parsing.DragonParser;
import tools.parsing.KingdomRitualParser;
import web.documentation.Documentation;
import web.tools.WebContext;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import static api.tools.text.StringUtil.nullToEmpty;

@Log4j
@ValidationEnabled
@Path("kingdom_ritual")
public class KingdomRitualResource {
    private final KingdomRitualParser kingdomRitualParser;
    private final BotUserDAO botUserDAO;

    @Inject
    public KingdomRitualResource(final KingdomRitualParser kingdomRitualParser, final BotUserDAO botUserDAO) {
        this.kingdomRitualParser = kingdomRitualParser;
        this.botUserDAO = botUserDAO;
    }

    @Documentation("Used for submitting kingdom ritual actions. Takes unparsed kingdom ritual related messages from inside the game")
    @POST
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public void addKingdomRitual(@Documentation(value = "The unparsed kingdom ritual info to add", itemName = "kingdom_ritual")
                          final String kingdomRitual,
                          @Context
                          final WebContext context) {
        BotUser user = botUserDAO.getUser(context.getName());

        kingdomRitualParser.parse(user.getMainNick(), nullToEmpty(kingdomRitual).trim());
    }
}
