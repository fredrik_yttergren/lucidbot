package web.resources;

import api.database.transactions.Transactional;
import api.irc.ValidationType;
import api.tools.validation.ValidationEnabled;
import tools.parsing.UtopiaValidationType;
import web.documentation.Documentation;
import web.tools.WebContext;

import org.hibernate.validator.constraints.NotEmpty;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.inject.Provider;
import lombok.extern.log4j.Log4j;

import java.net.URLDecoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Preconditions.checkArgument;

@Log4j
@ValidationEnabled
@Path("utopia_ingame_intel")
public class UtopiaInGameParserResource {
    private final Provider<UtopiaInGameParserSubResource> utopiaInGameParserSubResource;
    private static final String SUCCESS_STRING = "{\"success\": true}";

    @Inject
    public UtopiaInGameParserResource(final Provider<UtopiaInGameParserSubResource> utopiaInGameParserSubResource) {
        this.utopiaInGameParserSubResource = utopiaInGameParserSubResource;
    }

    @Documentation("Parses the incoming text and returns whatever intel was parsed and saved from it (of any type)")
    @POST
    @Produces({MediaType.TEXT_PLAIN})
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    @Transactional
    public String addIntel(@Documentation(value = "", itemName = "newIntel")
                           @NotEmpty(message = "The intel may not be null or empty")
                           final String newIntel,
                           @Context final WebContext webContext) throws Exception {
        String result;
        try {
            result = URLDecoder.decode(cleanURLEncoding(newIntel), "UTF-8");
        } catch (Exception e) {
            log.error(newIntel);
            log.error("failed to Decode URL with error: " + e);
            return SUCCESS_STRING;
        }
        return utopiaInGameParserSubResource.get().addIntel(result, webContext);
    }

    private String cleanURLEncoding(final String URL) {
        try {
            return URL.replaceAll("%u[0-9A-F]{4}", "");
        } catch (Exception e) {
            log.error("Exception while cleaning: " + e);
            return URL;
        }
    }
}
