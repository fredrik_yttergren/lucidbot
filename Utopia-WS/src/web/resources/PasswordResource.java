package web.resources;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.settings.PropertiesCollection;
import api.tools.validation.ValidationEnabled;
import com.google.inject.Provider;
import com.sun.jersey.api.JResponse;
import org.hibernate.validator.constraints.NotEmpty;
import web.documentation.Documentation;
import web.tools.WebContext;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import static api.tools.text.StringUtil.nullToEmpty;
import static com.google.common.base.Preconditions.checkNotNull;

import static api.settings.PropertiesConfig.BCRYPT;
import static api.settings.PropertiesConfig.DEFAULT_PASSWORD_ACCESS_ENABLED;

import lombok.extern.log4j.Log4j;

@Log4j
@ValidationEnabled
@Path("password")
public class PasswordResource {
    private final BotUserDAO userDAO;
    private final PropertiesCollection properties;
    private final boolean defaultPasswordIsAllowed;

    @Inject
    public PasswordResource(final BotUserDAO userDAO,
                            final PropertiesCollection properties,
                            @Named(DEFAULT_PASSWORD_ACCESS_ENABLED) final boolean defaultPasswordIsAllowed) {
        this.userDAO = userDAO;
        this.properties = properties;
        this.defaultPasswordIsAllowed = defaultPasswordIsAllowed;
    }

    @Documentation("Sets the users's password. Optional query param to specify user (Admin Only). If no user is specified, it is the logged in user.")
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public String setPassword(@Documentation(value = "The password to set", itemName = "password")
                            @NotEmpty(message = "The password may not be null or empty")
                            final String password,
                            @Documentation("Optional user Id for admins to specify whose password they want to change.")
                            @QueryParam("userId")
                            final String userId,
                            @Context final WebContext context) throws Exception {

        BotUser targetUser;
        BotUser user = context.getBotUser();

        if (userId != null && user.isAdmin()) {
            targetUser = userDAO.getUser(Long.parseLong(userId));
        } else if (userId != null && !user.isAdmin()) {
            throw new IllegalArgumentException("You are attempting to set a password for another user but you are not an admin");
        } else targetUser = user;

        if (!defaultPasswordIsAllowed && "password".equals(password))
            throw new IllegalArgumentException("That password is not allowed, please select a different one");

        targetUser.setPassword(password, properties.getBoolean(BCRYPT));

        return "Password set successfully";
    }
}
