/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package web.resources;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import com.google.inject.Provider;
import com.sun.jersey.api.JResponse;
import database.daos.KingdomRitualProjectDAO;
import database.daos.KingdomRitualDAO;
import database.models.*;
import web.documentation.Documentation;
import web.models.RS_KingdomRitual;
import web.models.RS_KingdomRitualAction;
import web.models.RS_KingdomRitualProject;
import web.validation.Update;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static api.tools.text.StringUtil.prettifyEnumName;
import static api.tools.validation.ValidationUtil.validate;
import static com.google.common.base.Preconditions.checkArgument;

@ValidationEnabled
@Path("kingdom_rituals")
public class KingdomRitualsResource {
    private final Provider<KingdomRitualDAO> kingdomRitualDAOProvider;
    private final Provider<BotUserDAO> userDAOProvider;
    private final KingdomRitualProjectDAO kingdomRitualProjectDAO;
    private final Provider<Validator> validatorProvider;

    @Inject
    public KingdomRitualsResource(final Provider<KingdomRitualDAO> kingdomRitualDAOProvider, final Provider<BotUserDAO> userDAOProvider,
                                  final KingdomRitualProjectDAO kingdomRitualProjectDAO, final Provider<Validator> validatorProvider) {
        this.kingdomRitualDAOProvider = kingdomRitualDAOProvider;
        this.userDAOProvider = userDAOProvider;
        this.kingdomRitualProjectDAO = kingdomRitualProjectDAO;
        this.validatorProvider = validatorProvider;
    }

    @Documentation("Returns the kingdom ritual with the specified id")
    @Path("{id : \\d+}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_KingdomRitual getKingdomRitual(@PathParam("id") final long id) {
        KingdomRitual kingdomRitual = kingdomRitualDAOProvider.get().getKingdomRitual(id);

        if (kingdomRitual == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        return RS_KingdomRitual.fromKingdomRitual(kingdomRitual, true);
    }

    @Documentation("Returns all kingdom rituals")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public JResponse<List<RS_KingdomRitual>> getKingdomRituals() {
        List<RS_KingdomRitual> kingdomRituals = new ArrayList<>();

        for (KingdomRitual kingdomRitual : kingdomRitualDAOProvider.get().getAllKingdomRituals()) {
            kingdomRituals.add(RS_KingdomRitual.fromKingdomRitual(kingdomRitual, true));
        }

        return JResponse.ok(kingdomRituals).build();
    }

    @Documentation("Adds a new kingdom rituals project and returns the saved object")
    @Path("projects")
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_KingdomRitualProject addKingdomRitualProject(@Documentation(value = "The kingdom rituals project to add", itemName = "newProject")
                                             @Valid final RS_KingdomRitualProject newProject) {
        KingdomRitualProject kingdomRitualProject = new KingdomRitualProject(KingdomRitualProjectType.fromName(newProject.getType()), newProject.getOriginalStatus());
        kingdomRitualProjectDAO.save(kingdomRitualProject);
        return RS_KingdomRitualProject.fromKingdomRitualProject(kingdomRitualProject, true);
    }

    @Documentation("Returns the kingdom rituals project with the specified id")
    @Path("projects/{id : \\d+}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_KingdomRitualProject getKingdomRitualProject(@PathParam("id") final long id) {
        KingdomRitualProject project = kingdomRitualProjectDAO.getProject(id);
        if (project == null) throw new WebApplicationException(Response.Status.NOT_FOUND);
        return RS_KingdomRitualProject.fromKingdomRitualProject(project, true);
    }

    @Documentation("Returns all existing kingdom rituals projects, regardless if they're active or not")
    @Path("projects")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public JResponse<List<RS_KingdomRitualProject>> getKingdomRitualProjects() {
        List<RS_KingdomRitualProject> projects = new ArrayList<>();
        for (KingdomRitualProject project : kingdomRitualProjectDAO.getAllProjects()) {
            projects.add(RS_KingdomRitualProject.fromKingdomRitualProject(project, false));
        }
        return JResponse.ok(projects).build();
    }

    @Documentation("Updates the specified kingdom rituals project and returns the updated object")
    @Path("projects/{id : \\d+}")
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_KingdomRitualProject updateKingdomRitualProject(@PathParam("id") final long id,
                                                @Documentation(value = "The updated kingdom rituals project", itemName = "updatedProject")
                                                final RS_KingdomRitualProject updatedProject) {
        KingdomRitualProject project = kingdomRitualProjectDAO.getProject(id);
        if (project == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        validate(updatedProject).using(validatorProvider.get()).forGroups(Update.class).throwOnFailedValidation();

        RS_KingdomRitualProject.toKingdomRitualProject(project, updatedProject);

        return RS_KingdomRitualProject.fromKingdomRitualProject(project, true);
    }

    @Documentation("Deletes the specified kingdom ritual project")
    @Path("projects/{id : \\d+}")
    @DELETE
    @Transactional
    public void deleteKingdomRitualProject(@PathParam("id") final long id) {
        KingdomRitualProject project = kingdomRitualProjectDAO.getProject(id);
        if (project == null) throw new WebApplicationException(Response.Status.NOT_FOUND);
        kingdomRitualProjectDAO.delete(project);
    }

    @Documentation("Registers a kingdom ritual project action (meaning someone casting)")
    @Path("projects/actions")
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public void registerKingdomRitualAction(@Documentation(value = "The action to register", itemName = "newAction")
                                     final RS_KingdomRitualAction newAction) {
        validate(newAction).using(validatorProvider.get()).throwOnFailedValidation();

        KingdomRitualProject project = kingdomRitualProjectDAO.getProject(newAction.getKingdomRitualProject().getId());
        BotUser user = userDAOProvider.get().getUser(newAction.getUser().getId());

        registerKingdomRitualAction(project, user, newAction.getContribution());
    }

    private void registerKingdomRitualAction(final KingdomRitualProject project, final BotUser user, final int contribution) {
        int actualContribution = Math.min(contribution, project.getStatus());
        checkArgument(actualContribution > 0, "Impossible contribution");
        user.incrementStat("Kingdom Ritual " + prettifyEnumName(project.getType()), actualContribution);

        Set<KingdomRitualAction> actions = project.getActions();
        Date now = new Date();
        for (KingdomRitualAction action : actions) {
            if (action.getUser().equals(user)) {
                action.setContribution(action.getContribution() + actualContribution);
                project.setStatus(Math.max(0, project.getStatus() - contribution));
                project.setUpdated(now);
                action.setUpdated(now);
                return;
            }
        }

        actions.add(new KingdomRitualAction(user, actualContribution, now, project));
        project.setStatus(Math.max(0, project.getStatus() - contribution));
        project.setUpdated(now);
    }
}
