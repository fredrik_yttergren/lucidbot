package web.resources;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import database.daos.ProvinceDAO;
import database.models.Province;
import tools.parsing.AidParser;
import web.documentation.Documentation;
import web.tools.WebContext;
import lombok.extern.log4j.Log4j;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import static api.tools.text.StringUtil.nullToEmpty;
import static com.google.common.base.Preconditions.checkArgument;

@Log4j
@ValidationEnabled
@Path("aiding")
public class AidingResource {
    private final AidParser aidParser;
    private final BotUserDAO botUserDAO;

    @Inject
    public AidingResource(final AidParser aidParser, final BotUserDAO botUserDAO) {
        this.aidParser = aidParser;
        this.botUserDAO = botUserDAO;
    }

    @Documentation("Used for submitting aid actions. Takes unparsed aid related messages from inside the game")
    @POST
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public void addAid(@Documentation(value = "The unparsed aid info to add", itemName = "aid")
                          final String aid,
                          @Context
                          final WebContext context) {
        BotUser user = botUserDAO.getUser(context.getName());
        aidParser.parse(user.getMainNick(), nullToEmpty(aid).trim());
    }

}
