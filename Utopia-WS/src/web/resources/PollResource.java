package web.resources;

import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import com.google.common.base.Supplier;
import com.google.inject.Provider;
import com.sun.jersey.api.JResponse;
import database.daos.PollDAO;
import database.daos.UserActivitiesDAO;
import database.models.*;
import events.PollStartedEvent;
import tools.BindingsManager;
import web.documentation.Documentation;
import web.models.RS_Poll;
import web.models.RS_PollOption;
import web.tools.AfterCommitEventPoster;
import web.tools.BindingsParser;
import web.tools.WebContext;
import web.validation.Update;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.validation.Validator;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

import static api.tools.time.DateUtil.isBefore;
import static api.tools.validation.ValidationUtil.validate;
import static com.google.common.base.Preconditions.checkArgument;
import static web.tools.SecurityHandler.ADMIN_ROLE;

@ValidationEnabled
@Path("polls")
public class PollResource {
    private final PollDAO pollDAO;
    private final Provider<BindingsParser> bindingsParserProvider;
    private final Provider<BindingsManager> bindingsManagerProvider;
    private final Provider<AfterCommitEventPoster> afterCommitEventPosterProvider;
    private final Provider<Validator> validatorProvider;
    private final Provider<UserActivitiesDAO> userActivitiesDAOProvider;

    @Inject
    public PollResource(final PollDAO pollDAO,
                        final Provider<BindingsParser> bindingsParserProvider,
                        final Provider<BindingsManager> bindingsManagerProvider,
                        final Provider<AfterCommitEventPoster> afterCommitEventPosterProvider,
                        final Provider<Validator> validatorProvider,
                        final Provider<UserActivitiesDAO> userActivitiesDAOProvider) {
        this.pollDAO = pollDAO;
        this.bindingsParserProvider = bindingsParserProvider;
        this.bindingsManagerProvider = bindingsManagerProvider;
        this.afterCommitEventPosterProvider = afterCommitEventPosterProvider;
        this.validatorProvider = validatorProvider;
        this.userActivitiesDAOProvider = userActivitiesDAOProvider;
    }

    @Documentation("Adds a poll, fires off an PollStartedEvent and returns the saved object. Admin only request")
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_Poll addPoll(@Documentation(value = "The new poll to add", itemName = "newPoll")
                           @Valid final RS_Poll newPoll,
                           @Context final WebContext webContext) {
        if (!webContext.isInRole(ADMIN_ROLE)) throw new WebApplicationException(Response.Status.FORBIDDEN);
        checkArgument(newPoll.getPollOptions().size() > 1, "You must provide at least 2 voting options");

        Bindings bindings = bindingsParserProvider.get().parse(newPoll.getBindings());

        final Poll poll = new Poll(newPoll.getDescription(), bindings, webContext.getName());
        poll.setStarted(true);
        for (RS_PollOption pollOption : newPoll.getPollOptions()) {
            poll.addPollOption(new PollOption(pollOption.getDescription(), poll));
        }
        afterCommitEventPosterProvider.get().addEventToPost(new Supplier<Object>() {
            @Override
            public Object get() {
                return new PollStartedEvent(poll.getId(), null);
            }
        });
        return RS_Poll.fromPoll(poll);
    }

    @Documentation("Returns the poll with the specified id")
    @Path("{id : \\d+}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_Poll getPoll(@PathParam("id") final long id,
                           @Context final WebContext webContext) {
        Poll poll = pollDAO.getPoll(id);

        if (poll == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        BotUser botUser = webContext.getBotUser();
        if (!botUser.isAdmin() && !bindingsManagerProvider.get().matchesBindings(poll.getBindings(), botUser))
            throw new WebApplicationException(Response.Status.FORBIDDEN);

        return RS_Poll.fromPoll(poll);
    }

    @Documentation("Returns polls. Admins may use an override parameter to see all, but other users can only see the polls whose bindings allow them to.")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public JResponse<List<RS_Poll>> getPolls(@Documentation("Whether to only show polls added since the last time the user checked")
                                             @QueryParam("newOnly")
                                             final boolean newOnly,
                                             @Documentation("Whether to include closed polls in the result. If left out they will not show")
                                             @QueryParam("showClosed")
                                             final boolean showClosed,
                                             @Documentation("Whether to ignore bindings and show all polls. Admin only")
                                             @QueryParam("ignoreBindings")
                                             final boolean ignoreBindings,
                                             @Context final WebContext webContext) {
        List<RS_Poll> out = new ArrayList<>();

        BotUser botUser = webContext.getBotUser();
        List<Poll> polls = new LinkedList<>();
        if (botUser.isAdmin() && ignoreBindings) {
            polls.addAll(pollDAO.getAllPolls());
        } else {
            polls.addAll(pollDAO.getPollsForUser(botUser, bindingsManagerProvider.get()));
        }

        if (newOnly) {
            UserActivities userActivities = userActivitiesDAOProvider.get().getUserActivities(botUser);
            removeOld(polls, userActivities.getLastPollsCheck());
        }
        if (!showClosed) {
            removeClosed(polls);
        }

        for (Poll poll : polls) {
            out.add(RS_Poll.fromPoll(poll));
        }

        return JResponse.ok(out).build();
    }

    private static void removeOld(final Collection<Poll> polls, final Date lastPollsCheck) {
        Iterator<Poll> iterator = polls.iterator();
        while (iterator.hasNext()) {
            Poll next = iterator.next();
            if (isBefore(next.getAdded(), lastPollsCheck)) iterator.remove();
        }
    }

    private static void removeClosed(final Collection<Poll> polls) {
        Iterator<Poll> iterator = polls.iterator();
        while (iterator.hasNext()) {
            Poll next = iterator.next();
            if (next.isClosed()) iterator.remove();
        }
    }

    @Documentation("Updates the specified poll and returns the updated object. Admin only request")
    @Path("{id : \\d+}")
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_Poll updatePoll(@PathParam("id") final long id,
                              @Documentation(value = "The updated poll", itemName = "updatedPoll")
                              final RS_Poll updatedPoll,
                              @Context final WebContext webContext) {
        if (!webContext.isInRole(ADMIN_ROLE)) throw new WebApplicationException(Response.Status.FORBIDDEN);

        Poll poll = pollDAO.getPoll(id);
        if (poll == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        validate(updatedPoll).using(validatorProvider.get()).forGroups(Update.class).throwOnFailedValidation();

        poll.setDescription(updatedPoll.getDescription());
        poll.setClosed(updatedPoll.isClosed());
        poll = pollDAO.save(poll);
        return RS_Poll.fromPoll(poll);
    }

    @Documentation("Updates the specified poll option and returns the updated object. Admin only request")
    @Path("{pollId : \\d+}/options/{optionId : \\d+}")
    @PUT
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_PollOption updatePollOption(@PathParam("pollId") final long pollId,
                                          @PathParam("optionId") final long optionId,
                                          @Documentation(value = "The updated poll option", itemName = "updatedPollOption")
                                          final RS_PollOption updatedPollOption,
                                          @Context final WebContext webContext) {
        if (!webContext.isInRole(ADMIN_ROLE)) throw new WebApplicationException(Response.Status.FORBIDDEN);

        Poll poll = pollDAO.getPoll(pollId);
        if (poll == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        PollOption pollOption = null;
        for (PollOption option : poll.getPollOptions()) {
            if (option.getId().equals(optionId)) {
                pollOption = option;
                break;
            }
        }
        if (pollOption == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        validate(updatedPollOption).using(validatorProvider.get()).forGroups(Update.class).throwOnFailedValidation();

        pollOption.setDescription(updatedPollOption.getDescription());
        pollDAO.save(poll);
        return RS_PollOption.fromPollOption(pollOption);
    }

    @Documentation("Deletes the specified poll. Admin only request")
    @Path("{id : \\d+}")
    @DELETE
    @Transactional
    public void deletePoll(@PathParam("id") final long id,
                           @Context final WebContext webContext) {
        if (!webContext.isInRole(ADMIN_ROLE)) throw new WebApplicationException(Response.Status.FORBIDDEN);

        Poll poll = pollDAO.getPoll(id);
        if (poll == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        pollDAO.delete(poll);
    }

    @Documentation("Adds your vote to the specified option, if the bindings allow you access to the poll in question. Returns the poll")
    @Path("{pollId : \\d+}/options/{optionId : \\d+}/vote")
    @POST
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_Poll vote(@PathParam("pollId") final long pollId,
                        @PathParam("optionId") final long optionId,
                        @Context final WebContext webContext) {
        Poll poll = pollDAO.getPoll(pollId);
        if (poll == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        BotUser botUser = webContext.getBotUser();
        if (!bindingsManagerProvider.get().matchesBindings(poll.getBindings(), botUser))
            throw new WebApplicationException(Response.Status.FORBIDDEN);

        PollOption pollOption = null;
        for (PollOption option : poll.getPollOptions()) {
            if (option.getId().equals(optionId)) {
                pollOption = option;
                break;
            }
        }
        if (pollOption == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        pollOption.removeVote(botUser);
        pollOption.addVote(new PollVote(pollOption, botUser));
        pollDAO.save(poll);
        return RS_Poll.fromPoll(poll);
    }
}
