package web.resources;

import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import database.daos.RecordDAO;
import database.models.Record;
import web.documentation.Documentation;
import web.models.RS_Record;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sun.jersey.api.JResponse;

/**
 * Created by Kyle on 6/25/2016.
 */
@ValidationEnabled
@Path("record")
public class RecordResource {
    private final RecordDAO recordDAO;

    @Inject
    public RecordResource(final RecordDAO recordDAO) {
        this.recordDAO = recordDAO;
    }

    @Documentation("Returns the record with the specified id")
    @Path("{id : \\d+}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_Record getRecord(@PathParam("id") final long id) {
        Record record = recordDAO.getRecordById(id);

        if (record == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        return RS_Record.fromRecord(record);
    }

    @Documentation("Returns all records")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public JResponse<List<RS_Record>> getRecords() {
        List<RS_Record> records = new ArrayList<>();

        for (Record record : recordDAO.getAllRecords()) {
            records.add(RS_Record.fromRecord(record));
        }

        return JResponse.ok(records).build();
    }
}
