package web.resources;

import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import com.google.inject.Provider;
import com.sun.jersey.api.JResponse;
import database.daos.IntelDAO;
import database.daos.KingdomDAO;
import database.models.Kingdom;
import database.models.SoT;
import database.models.StateCouncil;
import intel.Intel;
import intel.IntelParser;
import intel.IntelParserManager;
import web.models.RS_SoT;
import web.models.RS_StateCouncil;
import web.tools.WebContext;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class StateCouncilSubResource {
    private final IntelDAO intelDAO;
    private final Provider<IntelParserManager> intelParserManagerProvider;
    private final Provider<KingdomDAO> kingdomDAOProvider;
    private final Provider<DelayedEventPoster> delayedEventPosterProvider;

    @Inject
    public StateCouncilSubResource(final IntelDAO intelDAO,
                          final Provider<IntelParserManager> intelParserManagerProvider,
                          final Provider<KingdomDAO> kingdomDAOProvider,
                          final Provider<DelayedEventPoster> delayedEventPosterProvider) {
        this.intelDAO = intelDAO;
        this.intelParserManagerProvider = intelParserManagerProvider;
        this.kingdomDAOProvider = kingdomDAOProvider;
        this.delayedEventPosterProvider = delayedEventPosterProvider;
    }

    RS_StateCouncil addStateCouncil(final String newStateCouncil, final WebContext webContext) throws Exception {
        Map<String, IntelParser<?>> parsers = intelParserManagerProvider.get().getParsers(newStateCouncil);
        if (parsers.isEmpty()) throw new IllegalArgumentException("Data is not parsable");

        IntelParser<?> intelParser = parsers.values().iterator().next();
        if (!intelParser.getIntelTypeHandled().equals(StateCouncil.class.getSimpleName()))
            throw new IllegalArgumentException("Data is not recognized as a State Council");

        BotUser botUser = webContext.getBotUser();
        Intel parsedStateCouncil = null;
        try {
            parsedStateCouncil = intelParser.parse(botUser.getMainNick(), newStateCouncil);
        } catch (ParseException e) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        if (parsedStateCouncil == null) throw new WebApplicationException(Response.Status.NOT_MODIFIED);
        DelayedEventPoster delayedEventPoster = delayedEventPosterProvider.get();
        intelDAO.saveIntel(parsedStateCouncil, botUser.getId(), delayedEventPoster);
        delayedEventPoster.execute();

        return RS_StateCouncil.fromStateCouncil((StateCouncil) parsedStateCouncil, false);
    }

    RS_StateCouncil getStateCouncil(final long id) {
        StateCouncil stateCouncil = intelDAO.getStateCouncil(id);

        if (stateCouncil == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        return RS_StateCouncil.fromStateCouncil(stateCouncil, true);
    }

    JResponse<List<RS_StateCouncil>> getStateCouncils(final String kingdomId) {
        checkNotNull(kingdomId, "No kingdom specified");

        List<RS_StateCouncil> stateCouncils = new ArrayList<>();

        Kingdom kingdom = kingdomDAOProvider.get().getKingdomByIdOrLoc(kingdomId);
        checkNotNull(kingdom, "No such kingdom");
        List<StateCouncil> stateCouncilsForKD = intelDAO.getStateCouncilsForKD(kingdom.getLocation());

        for (StateCouncil stateCouncil : stateCouncilsForKD) {
            stateCouncils.add(RS_StateCouncil.fromStateCouncil(stateCouncil, true));
        }

        return JResponse.ok(stateCouncils).build();
    }

    void deleteStateCouncil(final long id) {
        StateCouncil stateCouncil = intelDAO.getStateCouncil(id);
        if (stateCouncil == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        intelDAO.deleteIntel(stateCouncil);
    }
}
