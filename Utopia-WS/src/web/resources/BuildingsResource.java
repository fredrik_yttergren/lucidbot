package web.resources;

import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import com.sun.jersey.api.JResponse;
import database.daos.BuildingDAO;
import database.models.Building;
import database.models.HonorTitle;
import web.documentation.Documentation;
import web.models.RS_Building;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@ValidationEnabled
@Path("buildings")
public class BuildingsResource {
    private final BuildingDAO buildingDAO;

    @Inject
    public BuildingsResource(final BuildingDAO buildingDAO) {
        this.buildingDAO = buildingDAO;
    }

    @Documentation("Returns the building with the specified id")
    @Path("{id : \\d+}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public RS_Building getBuilding(@PathParam("id") final long id) {
        Building building = buildingDAO.getBuilding(id);
        if (building == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        return RS_Building.fromBuilding(building);
    }

    @Documentation("Returns all buildings")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Transactional
    public JResponse<List<RS_Building>> getBuildings() {
        List<RS_Building> buildings = new ArrayList<>();

        Collection<Building> allBuildings = buildingDAO.getAllBuildings();
        for (Building building : allBuildings) {
            buildings.add(RS_Building.fromBuilding(building));
        }

        return JResponse.ok(buildings).build();
    }
}
