package web.resources;

import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import com.google.inject.Provider;
import com.sun.jersey.api.JResponse;
import database.daos.IntelDAO;
import database.daos.KingdomDAO;
import database.models.Kingdom;
import database.models.Military;
import database.models.Military;
import intel.Intel;
import intel.IntelParser;
import intel.IntelParserManager;
import web.models.RS_Military;
import web.tools.WebContext;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public class MilitarySubResource {
    private final IntelDAO intelDAO;
    private final Provider<IntelParserManager> intelParserManagerProvider;
    private final Provider<KingdomDAO> kingdomDAOProvider;
    private final Provider<DelayedEventPoster> delayedEventPosterProvider;

    @Inject
    public MilitarySubResource(final IntelDAO intelDAO,
                               final Provider<IntelParserManager> intelParserManagerProvider,
                               final Provider<KingdomDAO> kingdomDAOProvider,
                               final Provider<DelayedEventPoster> delayedEventPosterProvider) {
        this.intelDAO = intelDAO;
        this.intelParserManagerProvider = intelParserManagerProvider;
        this.kingdomDAOProvider = kingdomDAOProvider;
        this.delayedEventPosterProvider = delayedEventPosterProvider;
    }

    RS_Military addMilitary(final String newMilitary, final WebContext webContext) throws Exception {
        Map<String, IntelParser<?>> parsers = intelParserManagerProvider.get().getParsers(newMilitary);
        if (parsers.isEmpty()) throw new IllegalArgumentException("Data is not parsable");

        IntelParser<?> intelParser = parsers.values().iterator().next();
        if (!intelParser.getIntelTypeHandled().equals(Military.class.getSimpleName()))
            throw new IllegalArgumentException("Data is not recognized as a State Council");

        BotUser botUser = webContext.getBotUser();
        Intel parsedMilitary = null;
        try {
            parsedMilitary = intelParser.parse(botUser.getMainNick(), newMilitary);
        } catch (ParseException e) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        if (parsedMilitary == null) throw new WebApplicationException(Response.Status.NOT_MODIFIED);
        DelayedEventPoster delayedEventPoster = delayedEventPosterProvider.get();
        intelDAO.saveIntel(parsedMilitary, botUser.getId(), delayedEventPoster);
        delayedEventPoster.execute();

        return RS_Military.fromMilitary((Military) parsedMilitary, false);
    }

    RS_Military getMilitary(final long id) {
        Military Military = intelDAO.getMilitary(id);

        if (Military == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        return RS_Military.fromMilitary(Military, true);
    }

    JResponse<List<RS_Military>> getMilitaries(final String kingdomId) {
        checkNotNull(kingdomId, "No kingdom specified");

        List<RS_Military> militaries = new ArrayList<>();

        Kingdom kingdom = kingdomDAOProvider.get().getKingdomByIdOrLoc(kingdomId);
        checkNotNull(kingdom, "No such kingdom");
        List<Military> militariesForKD = intelDAO.getMilitariesForKD(kingdom.getLocation());

        for (Military Military : militariesForKD) {
            militaries.add(RS_Military.fromMilitary(Military, true));
        }

        return JResponse.ok(militaries).build();
    }

    void deleteMilitary(final long id) {
        Military military = intelDAO.getMilitary(id);
        if (military == null) throw new WebApplicationException(Response.Status.NOT_FOUND);

        intelDAO.deleteIntel(military);
    }
}
