package web.resources;

import api.database.daos.BotUserDAO;
import com.google.common.base.Function;
import com.google.inject.Provider;

import java.io.StringWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Nullable;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import database.daos.IntelDAO;
import database.daos.KingdomDAO;
import database.daos.NewsItemDAO;
import database.daos.ProvinceDAO;
import database.models.*;
import events.DragonNewsEvent;
import events.IncomingAttacksEvent;
import events.KingdomRitualNewsEvent;
import events.WarNewsEvent;
import intel.Intel;
import intel.IntelParser;
import intel.IntelParserManager;
import lombok.extern.log4j.Log4j;
import tools.calculators.WPACalculator;
import tools.parsing.*;
import tools.target_locator.CharacterDrivenTargetLocatorFactory;
import tools.target_locator.TargetLocator;
import tools.target_locator.TargetLocatorFactory;
import web.models.*;
import web.tools.WebContext;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Multimap;
import com.google.common.eventbus.EventBus;

import static com.google.common.base.Preconditions.checkArgument;
import static api.tools.text.StringUtil.nullToEmpty;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * A foreword on this class - It is way too big.  The resulting code duplication is a big piece of technical debt.
 * However, refactoring to remove the need for the duplication represents a rather large piece of work and I'm not sure which direction to take with it.
 * For now, I'm settling on punting that effort to the future, when I've had more time to see this code operate.
 *
 * The crux of the issue is that there's too much brains inside of the Resource classes and that needs to be stripped out.
 *
 */

@Log4j
public class UtopiaInGameParserSubResource {

    private static final String SUCCESS_STRING = "{\"success\": true}";
    private static final Pattern
            INFILTRATE = Pattern.compile("Our thieves have infiltrated the Thieves' Guilds of (?<target>[^(]+" +
            UtopiaValidationType.KDLOC.getPatternString() +
            "). They appear to have about (?<result>" + ValidationType.INT.getPattern() +
            ") thieves employed across their lands");
    private static final Pattern IN_GAME_INTEL = Pattern.compile("url=(?<url>.*)&data_html=(?<html>.*)&data_simple=(?<data>.*)&key=(?<key>.*)&prov=(?<prov>.*)");
    private static final Pattern INTEL_OPS = Pattern.compile("SPY_ON_THRONE|SPY_ON_MILITARY|SURVEY|SPY_ON_SCIENCES");
    private static final Pattern INTEL_URLS = Pattern.compile("kingdom_details|province_operations|thievery|sorcery|charms|enchantment");
    private static final Pattern SELF_INTEL_URLS = Pattern.compile("throne|council_|science|train_army");
    private static final Pattern INVALID_URLS = Pattern.compile("province_news|wizards|build|preferences|ranking|monarchy|war_forum|kingdom_forum|mail");
    private static final Pattern TARGET_KINGDOM_PATTERN = Pattern.compile("Target kingdom is [^(]+(" +
            UtopiaValidationType.KDLOC.getPatternString() + ")");

    private final IntelDAO intelDAO;
    private final ProvinceDAO provinceDAO;
    private final Provider<BotUserDAO> botUserDAOProvider;
    private final Provider<IntelParserManager> intelParserManagerProvider;
    private final Provider<DelayedEventPoster> delayedEventPosterProvider;
    private final Provider<NewsParser> newsParserProvider;

    private final NewsItemDAO newsItemDAO;
    private final EventBus eventBus;

    private final SpellsOpsParser spellsOpsParser;
    private final CharacterDrivenTargetLocatorFactory characterDrivenTargetLocatorFactory;

    private final AttackParser attackParser;
    private final DragonParser dragonParser;
    private final AidParser aidParser;
    private final KingdomRitualParser kingdomRitualParser;

    private final WPACalculator wpaCalculator;

    @Inject
    public UtopiaInGameParserSubResource(final IntelDAO intelDAO,
                                         final ProvinceDAO provinceDAO,
                                         final Provider<IntelParserManager> intelParserManagerProvider,
                                         final Provider<DelayedEventPoster> delayedEventPosterProvider,
                                         final Provider<BotUserDAO> botUserDAOProvider,
                                         final Provider<NewsParser> newsParserProvider,
                                         final NewsItemDAO newsItemDAO,
                                         final EventBus eventBus,
                                         final SpellsOpsParser spellsOpsParser,
                                         final CharacterDrivenTargetLocatorFactory characterDrivenTargetLocatorFactory,
                                         final AttackParser attackParser,
                                         final DragonParser dragonParser,
                                         final AidParser aidParser,
                                         final KingdomRitualParser kingdomRitualParser,
                                         final WPACalculator wpaCalculator) {
        this.intelDAO = intelDAO;
        this.provinceDAO = provinceDAO;
        this.intelParserManagerProvider = intelParserManagerProvider;
        this.delayedEventPosterProvider = delayedEventPosterProvider;
        this.botUserDAOProvider = botUserDAOProvider;
        this.newsParserProvider = newsParserProvider;
        this.newsItemDAO = newsItemDAO;
        this.eventBus = eventBus;
        this.spellsOpsParser = spellsOpsParser;
        this.characterDrivenTargetLocatorFactory = characterDrivenTargetLocatorFactory;
        this.attackParser = attackParser;
        this.dragonParser = dragonParser;
        this.aidParser = aidParser;
        this.kingdomRitualParser = kingdomRitualParser;
        this.wpaCalculator = wpaCalculator;

    }

    String addIntel(final String newIntel, final WebContext webContext) throws Exception {
        String cleanedIntel = newIntel.replace('\r', ' ').replace('\n', ' ').trim();
        Matcher matcher = IN_GAME_INTEL.matcher(cleanedIntel);
        matcher.find();
        //Identify the posting user and verify they have a valid key.
        String key = matcher.group("key");
        String prov = matcher.group("prov");
        String url = matcher.group("url");

        BotUser botUser = botUserDAOProvider.get().getUserByKey(key);
        if (botUser == null) {
            log.error("Invalid in-game intel key used:" + key +
                        "Sending Province: " + prov +
                        "URL: " + url);
            throw new WebApplicationException(Response.status(Response.Status.BAD_REQUEST)
                    .entity("User key not valid").build());
        }

        Province province = provinceDAO.getProvince(prov);
        Province userProv = provinceDAO.getProvinceForUser(botUser);
        //Check if the user is sitting
        if (province != null && userProv != null && !userProv.getName().equals(province.getName())) {
            botUser = province.getProvinceOwner() == null ? botUserDAOProvider.get().getUserByKey(key) : province.getProvinceOwner();
        }

        cleanedIntel = matcher.group("data");
        Document doc = Jsoup.parse(matcher.group("html"));

        //This can probably be a switch statement, but I'm not familiar with all the logic.
        //I'm creating a complex chain of logic like this to help speed up parsing.
        if (INVALID_URLS.matcher(url).find()) {
            //Can't do anything with these, return to prevent an error message
            return SUCCESS_STRING;
        }
        if (INTEL_URLS.matcher(url).find()) {
            String targetProvince = null;
            String targetKingdom = null;
            try {
                targetProvince = doc.select("div#uniform-id_target_province > span").first().text();
                matcher = TARGET_KINGDOM_PATTERN.matcher(cleanedIntel);
                if (matcher.find()) {
                    targetKingdom = matcher.group(1);
                }
            } catch (Exception e) {
            }

            if (url.contains("SNATCH_NEWS") || url.contains("kingdom_news")) {
                return parseNews(cleanedIntel);
            }
            else if (url.contains("INFILTRATE")) {
                return parseInfiltrates(cleanedIntel);
            }
            else if (INTEL_OPS.matcher(url).find()) {
                return parseIntel(cleanedIntel, botUser);
            }
            //Must be a spell/op
            return parseSpellOps(cleanedIntel, botUser, targetProvince, targetKingdom);
        }
        else if (url.contains("kingdom_news")) {
            return parseNews(cleanedIntel);
        }
        else if (url.contains("send_armies")) {
            return parseAttack(cleanedIntel, botUser);
        }
        else if (url.contains("_dragon")) {
            return parseDragon(cleanedIntel, botUser);
        }
        else if (url.contains("aid")) {
            return parseAid(cleanedIntel, botUser);
        }
        else if (url.contains("kingdom_ritual") || url.contains("cast_ritual")) {
            return parseKingdomRitual(cleanedIntel, botUser);
        }
        else if (url.contains("army_training")) {
            return parseIntel(matcher.group("html"), botUser);
        }
        else if (SELF_INTEL_URLS.matcher(url).find()) {
            return parseIntel(cleanedIntel, botUser);
        }

        log.error("Unrecognized URL: " + url);
        return SUCCESS_STRING;
    }

    private String parseKingdomRitual(final String cleanedIntel, final BotUser botUser) {
        kingdomRitualParser.parse(botUser.getMainNick(), nullToEmpty(cleanedIntel).trim());
        return SUCCESS_STRING;
    }

    private String parseAid(final String cleanedIntel, final BotUser botUser) {
        aidParser.parse(botUser.getMainNick(), nullToEmpty(cleanedIntel).trim());
        return SUCCESS_STRING;
    }

    private String parseDragon(final String cleanedIntel, final BotUser botUser) {
        dragonParser.parse(botUser.getMainNick(), nullToEmpty(cleanedIntel).trim());
        return SUCCESS_STRING;
    }

    private String parseAttack(final String cleanedIntel, final BotUser botUser) {
        Province attacker = provinceDAO.getProvinceForUser(botUser);
        checkArgument(attacker != null, "You can't register attacks when you don't have a province yourself");
        attackParser.parse(botUser.getMainNick(), nullToEmpty(cleanedIntel).trim());
        return SUCCESS_STRING;
    }

    private String parseSpellOps(final String cleanedIntel, final BotUser botUser, final String targetName, final String targetKingdom) {
        try {
            spellsOpsParser.parseSingle(botUser, cleanedIntel, createTargetLocatorFactory(targetName, targetKingdom));
        } catch(Exception e) {
            log.error("Error during spell op parsing:" + e);
        }
        return SUCCESS_STRING;
    }

    private String parseNews(final String cleanedIntel) {
        NewsParser newsParser = newsParserProvider.get();
        Collection<NewsItem> newsItems = newsParser.parseNews(cleanedIntel);

        newsItems = newsItemDAO.save(newsItems);
        newsParser.sendNewsEvents(newsItems);

        if (!newsItems.isEmpty()) {
            Multimap<String, NewsItem> incomingAttacksParsed = newsParser.extractIncomingAttacks(newsItems);
            if (!incomingAttacksParsed.isEmpty()) {
                eventBus.post(new IncomingAttacksEvent(incomingAttacksParsed));
            }
        }
        return SUCCESS_STRING;
    }

    private String parseInfiltrates(final String cleanedIntel) {

        Matcher matcher = INFILTRATE.matcher(cleanedIntel);
        while (matcher.find()) {
            String target = matcher.group("target");
            final String provinceName = target.substring(0, target.indexOf('(')).trim();
            final String kingdom = target.substring(target.indexOf('('));
            final int result = NumberUtil.parseInt(matcher.group("result"));

            try {
                saveInfiltrate(provinceName, kingdom, result);
            } catch (final Exception e) {
                throw new WebApplicationException(Response.serverError().entity("Failed to update tpa").build());
            }
        }
        return SUCCESS_STRING;
    }

    private String saveInfiltrate(final String provinceName, final String kingdom, final int result) {
        Province province = provinceDAO.getOrCreateProvince(provinceName, kingdom);
        province.setThieves(result);
        province.setThievesLastUpdated(new Date());
        province.setThievesAccurate(true);
        wpaCalculator.updateWizards(province);
        province.setLastUpdated(province.getThievesLastUpdated(), true);
        return SUCCESS_STRING;
    }

    private String parseIntel(final String cleanedIntel, final BotUser botUser) {
        Map<String, IntelParser<?>> parsers = intelParserManagerProvider.get().getParsers(cleanedIntel);
        for (Map.Entry<String, IntelParser<?>> entry : parsers.entrySet()) {
            String rawIntel = entry.getKey();
            IntelParser<?> parser = entry.getValue();
            try {
                Intel parsedIntel = parser.parse(botUser.getMainNick(), rawIntel);
                if (parsedIntel != null) {
                    DelayedEventPoster delayedEventPoster = delayedEventPosterProvider.get();
                    intelDAO.saveIntel(parsedIntel, botUser.getId(), delayedEventPoster);
                    delayedEventPoster.execute();
                } else {
                    //Parsed intel was null, but shouldn't have been...
                    log.error(cleanedIntel);
                }
            } catch (Exception e) {
                UtopiaInGameParserSubResource.log.error("Failed to parse or save intel posted from web service", e);
            }
        }
        return SUCCESS_STRING;
    }

    private TargetLocatorFactory createTargetLocatorFactory(final String targetName, final String kingdomLoc) {
        return new TargetLocatorFactory() {
            @Override
            public TargetLocator createLocator(final SpellOpCharacter spellOpCharacter) {
                if (targetName != null) {
                    return new TargetLocator() {
                        @Nullable
                        @Override
                        public Province locateTarget(final BotUser user, final Matcher matchedRegex) {
                            return kingdomLoc == null ? provinceDAO.getProvince(targetName) : provinceDAO.getOrCreateProvince(targetName, kingdomLoc);
                        }
                    };
                } else
                    return characterDrivenTargetLocatorFactory.createLocator(spellOpCharacter);
            }
        };
    }
}
