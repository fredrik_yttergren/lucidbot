package web.resources;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.Transactional;
import api.tools.validation.ValidationEnabled;
import database.daos.ProvinceDAO;
import database.models.Province;
import tools.parsing.DragonParser;
import web.documentation.Documentation;
import web.tools.WebContext;
import lombok.extern.log4j.Log4j;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import static api.tools.text.StringUtil.nullToEmpty;
import static com.google.common.base.Preconditions.checkArgument;

@Log4j
@ValidationEnabled
@Path("dragon")
public class DragonResource {
    private final DragonParser dragonParser;
    private final BotUserDAO botUserDAO;

    @Inject
    public DragonResource(final DragonParser dragonParser, final BotUserDAO botUserDAO) {
        this.dragonParser = dragonParser;
        this.botUserDAO = botUserDAO;
    }

    @Documentation("Used for submitting dragon actions. Takes unparsed dragon related messages from inside the game")
    @POST
    @Consumes({MediaType.TEXT_PLAIN})
    @Transactional
    public void addDragon(@Documentation(value = "The unparsed dragon info to add", itemName = "dragon")
                          final String dragon,
                          @Context
                          final WebContext context) {
        BotUser user = botUserDAO.getUser(context.getName());

        dragonParser.parse(user.getMainNick(), nullToEmpty(dragon).trim());
    }
}
