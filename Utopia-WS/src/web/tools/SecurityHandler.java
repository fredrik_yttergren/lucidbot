/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package web.tools;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import com.google.common.collect.Sets;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.security.ServerAuthException;
import org.eclipse.jetty.server.Authentication;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.util.security.Constraint;

import javax.inject.Inject;
import java.util.Collections;

import web.tools.CrossOriginConstraintSecurityHandler;

public class SecurityHandler {
    public static final String USER_ROLE = "user";
    public static final String ADMIN_ROLE = "admin";
    private static final String[] ROLES = {USER_ROLE, ADMIN_ROLE};

    private final LoginHandler loginHandler;

    @Inject
    public SecurityHandler(final LoginHandler loginHandler) {
        this.loginHandler = loginHandler;
    }

    public void setupSecurity(final ServletContextHandler contextHandler) {
        Constraint constraint = new Constraint();
        constraint.setName(Constraint.__BASIC_AUTH);
        constraint.setAuthenticate(true);
        constraint.setRoles(ROLES);

        ConstraintMapping mapping = new ConstraintMapping();
        mapping.setPathSpec("/*");
        mapping.setConstraint(constraint);

        ConstraintSecurityHandler security = new CrossOriginConstraintSecurityHandler();
        security.setConstraintMappings(Collections.singletonList(mapping), Sets.newHashSet(ROLES));
        security.setAuthenticator(new BasicCORSAuthenticator());
        security.setLoginService(loginHandler);

        contextHandler.setSecurityHandler(security);
    }

    /**
     * A simple extended implementation of org.eclipse.jetty.security.authentication.BasicAuthenticator
     * that makes sure all authentication responses include CORS headers.
     */
    public class BasicCORSAuthenticator extends BasicAuthenticator {
        /**
         * Check Basic Authenticator decision and append headers if required.
         */
        public Authentication validateRequest(ServletRequest req, ServletResponse res, boolean mandatory)
            throws ServerAuthException
        {
            HttpServletResponse response = (HttpServletResponse)res;
            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, OPTIONS");
            response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Accept-Encoding, " +
                    "Accept-Language, Connection, Content-Length, Host, Referrer, User-Agent, Authorization");
            response.setHeader("Access-Control-Max-Age" , "1000");
            return super.validateRequest(req, res, mandatory);
        }
    }
}
