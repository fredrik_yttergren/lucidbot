/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package parsers;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.irc.ValidationType;
import api.tools.collections.MapFactory;
import api.tools.numbers.NumberUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.CommonEntitiesAccess;
import database.daos.ProvinceDAO;
import database.models.Province;
import database.models.ScienceType;
import database.models.SoS;
import database.models.SoSEntry;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.calculators.MaxPopulationCalculator;
import tools.calculators.TPACalculator;
import tools.calculators.WPACalculator;
import tools.parsing.UtopiaValidationType;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Singleton
@Log4j
class SoSParser implements IntelParser<SoS> {

    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;
    private final CommonEntitiesAccess commonEntitiesAccess;
    private final WPACalculator wpaCalculator;
    private final TPACalculator tpaCalculator;
    private final MaxPopulationCalculator maxPopulationCalculator;

    private Pattern provincePattern;
    private Pattern sciencePattern;
    private Pattern selfSciencePattern;
    private Pattern scienceCategoryPattern;

    private Pattern identifierPattern;
    private Pattern accuracyPattern;

    @Inject
    SoSParser(final CommonEntitiesAccess commonEntitiesAccess, final Provider<ProvinceDAO> provinceDAOProvider,
              final Provider<BotUserDAO> botUserDAOProvider, final EventBus eventBus,
              final WPACalculator wpaCalculator, final TPACalculator tpaCalculator,
              final MaxPopulationCalculator maxPopulationCalculator) {
        this.commonEntitiesAccess = commonEntitiesAccess;
        this.provinceDAOProvider = provinceDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;
        this.wpaCalculator = wpaCalculator;
        this.tpaCalculator = tpaCalculator;
        this.maxPopulationCalculator = maxPopulationCalculator;

        compilePatterns();
        eventBus.register(this);
    }

    private void compilePatterns() {
        provincePattern = Pattern.compile("Our thieves visit the research centers of ([^(]+)(" +
                UtopiaValidationType.KDLOC.getPatternString() + ')');
        sciencePattern = Pattern.compile('(' + commonEntitiesAccess.getScienceTypeGroup() +
                ")\\s+(" + ValidationType.INT.getPattern() + ")\\s+[+-]?(" +
                ValidationType.DOUBLE.getPattern() + ")%");
        scienceCategoryPattern = Pattern.compile('(' + commonEntitiesAccess.getScienceTypeGroup() +
                ")\\s+-\\s+(" + ValidationType.INT.getPattern() + ")\\s+scientists\\s+-\\s+(" +
                ValidationType.INT.getPattern() + ")\\s+books");
        selfSciencePattern = Pattern.compile('(' + commonEntitiesAccess.getScienceTypeGroup() +
                ")\\s+(" + ValidationType.INT.getPattern() + ")\\s+[+\\-]?(" +
                ValidationType.DOUBLE.getPattern() + ")%");
        identifierPattern = Pattern.compile("(?:(?:" + provincePattern.pattern() +
                ")|Mastering the Arts & Sciences makes our people stronger, smarter, and more efficient. We can grow in strength without growing in size)");
        accuracyPattern = Pattern.compile("we have\\s*(" + ValidationType.INT.getPattern() + ")%\\s*confidence in the information retrieved");

    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    @Override
    public Pattern getIdentifierPattern() {
        return identifierPattern;
    }

    @Override
    public SoS parse(final String savedBy, final String text) throws Exception {
        SoS sos = new SoS();
        boolean selfSos = false;

        Matcher matcher = provincePattern.matcher(text);
        if (matcher.find()) {
            ProvinceDAO provinceDao = provinceDAOProvider.get();
            String name = matcher.group(1).trim();
            Province province = provinceDao.getOrCreateProvince(name, matcher.group(2));
            province.setName(name);
            if (province.getSos() != null) {
                sos = province.getSos();
            } else sos.setProvince(province);
        } else {
            selfSos = true;
            BotUser user = botUserDAOProvider.get().getUser(savedBy);
            Province province = provinceDAOProvider.get().getProvinceForUser(user);
            if (province == null)
                throw new ParseException("Self sos contains no province name, and user " + user.getMainNick() + " has no registered province", 0);
            if (province.getSos() != null) sos = province.getSos();
            else sos.setProvince(province);
        }

        Map<String, ScienceType> sciences = MapFactory.newNameToObjectMapping(commonEntitiesAccess.getAllScienceTypes());
        Set<SoSEntry> entries = new HashSet<>();

        matcher = !selfSos ? sciencePattern.matcher(text) : selfSciencePattern.matcher(text);

        while (matcher.find()) {
            ScienceType type = sciences.get(matcher.group(1));
            int books = NumberUtil.parseInt(matcher.group(2));
            double percent = NumberUtil.parseDouble(matcher.group(3));
            entries.add(SoSEntryUtil.registerEntry(sos, SoSEntry.SoSEntryType.BOOKS, type, books));
            entries.add(SoSEntryUtil.registerEntry(sos, SoSEntry.SoSEntryType.EFFECT, type, percent));
        }

        matcher = scienceCategoryPattern.matcher(text);

        while (matcher.find()) {
            ScienceType type = sciences.get(matcher.group(1));
            int scientists = NumberUtil.parseInt(matcher.group(2));
            int books = NumberUtil.parseInt(matcher.group(3));
            entries.add(SoSEntryUtil.registerEntry(sos, SoSEntry.SoSEntryType.UNALLOCATED_BOOKS, type, books));
            entries.add(SoSEntryUtil.registerEntry(sos, SoSEntry.SoSEntryType.SCIENTISTS, type, scientists));
        }

        for (Iterator<SoSEntry> iter = sos.getSciences().iterator(); iter.hasNext(); ) {
            SoSEntry entry = iter.next();
            if (!entries.contains(entry)) iter.remove();
        }

        matcher = accuracyPattern.matcher(text);
        if (matcher.find()) {
            sos.setAccuracy(NumberUtil.parseInt(matcher.group(1)));
        }

        sos.setSavedBy(savedBy);
        sos.setLastUpdated(new Date());
        sos.getProvince().setLastUpdated(sos.getLastUpdated(), true);
        sos.calcTotalBooks();

        maxPopulationCalculator.updateMaxPopulation(sos.getProvince(), sos);
        tpaCalculator.updateThieves(sos.getProvince(), sos);
        wpaCalculator.updateWizards(sos.getProvince(), sos);

        sos.setExportLine(null);

        return sos;
    }

    @Override
    public String getIntelTypeHandled() {
        return SoS.class.getSimpleName();
    }
}
