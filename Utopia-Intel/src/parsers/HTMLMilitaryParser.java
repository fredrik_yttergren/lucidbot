package parsers;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import api.tools.text.StringUtil;
import api.tools.time.DateUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.ProvinceDAO;
import database.models.*;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.parsing.UtopiaValidationType;
import tools.time.UtopiaTime;
import tools.time.UtopiaTimeFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Singleton
@Log4j
class HTMLMilitaryParser implements IntelParser<Military> {
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;

    private Pattern identifierPattern;
    private Pattern draftRatePattern;
    private Pattern specCreditsPattern;

    @Inject
    HTMLMilitaryParser(final Provider<ProvinceDAO> provinceDAOProvider, final EventBus eventBus,
                   final Provider<BotUserDAO> botUserDAOProvider) {
        this.provinceDAOProvider = provinceDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;

        compilePatterns();
        eventBus.register(this);
    }

    private void compilePatterns() {
        identifierPattern = Pattern.compile("<a href=\"/wol/game/council_military\">Military Advisor</a> page");
        specCreditsPattern = Pattern.compile("Free .*? credits left\\s*(" + ValidationType.INT.getPattern() + ")");
        draftRatePattern = Pattern.compile("Draft\\s*rate:\\s*(" + UtopiaValidationType.DRAFTRATES.getPattern() + ")\\s* " +
                "\\((" + ValidationType.DOUBLE.getPattern() + ")% Rate");
    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    @Override
    public Pattern getIdentifierPattern() {
        return identifierPattern;
    }

    @Override
    public Military parse(final String savedBy, final String text) throws Exception {
        Military military = new Military();

        Matcher matcher;
        Province province;
        BotUser user = botUserDAOProvider.get().getUser(savedBy);
        province = provinceDAOProvider.get().getProvinceForUser(user);
        if (province == null)
            throw new ParseException("Military page contains no province name, and user " + user.getMainNick() + " has no registered province", 0);
        if (province.getMilitary() != null) military = province.getMilitary();
        else military.setProvince(province);

        //ArrayList<Elements> data = new ArrayList<>();
        Document doc = Jsoup.parse(text);
        Element table = doc.select("table").get(1);
        Elements rows = table.select("tr");

        for (int j = 1; j < rows.size(); j++) {
            matcher = specCreditsPattern.matcher(rows.get(j).select("th").get(1).text());
            if (matcher.find()) {
                military.setSpecCredits(NumberUtil.parseInt(rows.get(j).select("td").get(1).text()));
                break;
            }
        }

        Elements selected = doc.select("select option[selected]");
        matcher = draftRatePattern.matcher(selected.get(0).text());
        if (matcher.find()) {
            military.setDraftRate(NumberUtil.parseInt(matcher.group(1)));
        }

        Element draftTargetInput = doc.select("input[name=draft_target]").first();
        military.setDraftTarget(NumberUtil.parseInt(draftTargetInput.attr("value")));

        Element wageRateInput = doc.select("input[name=wage_rate]").first();
        military.setWageRate(NumberUtil.parseInt(wageRateInput.attr("value")));

        military.setSavedBy(savedBy);
        military.setLastUpdated(new Date());

        return military;
    }


    @Override
    public String getIntelTypeHandled() {
        return Military.class.getSimpleName();
    }
}
