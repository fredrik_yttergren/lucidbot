/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package parsers;

import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import api.tools.text.StringUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.CommonEntitiesAccess;
import database.daos.HonorTitleDAO;
import database.daos.KingdomDAO;
import database.daos.ProvinceDAO;
import database.daos.RaceDAO;
import database.models.HonorTitle;
import database.models.Kingdom;
import database.models.Province;
import database.models.Race;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.calculators.TPACalculator;
import tools.calculators.WPACalculator;
import tools.parsing.UtopiaValidationType;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Singleton
@Log4j
class KingdomParser implements IntelParser<Kingdom> {
    private final Provider<RaceDAO> raceDAOProvider;
    private final Provider<HonorTitleDAO> honorTitleDAOProvider;
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<KingdomDAO> kingdomDAOProvider;
    private final CommonEntitiesAccess commonEntitiesAccess;
    private final WPACalculator wpaCalculator;
    private final TPACalculator tpaCalculator;

    private Pattern nameLocPattern;
    private Pattern amountOfProvsPattern;
    private Pattern provListStartPattern;
    private Pattern provincePattern;
    private Pattern warPattern;
    private Pattern invalidProvincePattern;
    private Pattern selfKDprovincePattern;
    private Pattern selfKDprovListStartPattern;
    private Pattern oldProvListStart;
    private Pattern oldProvincePattern;

    @Inject
    KingdomParser(final CommonEntitiesAccess commonEntitiesAccess, final Provider<HonorTitleDAO> honorTitleDAOProvider,
                  final Provider<RaceDAO> raceDAOProvider, final Provider<ProvinceDAO> provinceDAOProvider,
                  final Provider<KingdomDAO> kingdomDAOProvider, final EventBus eventBus,
                  final WPACalculator wpaCalculator, final TPACalculator tpaCalculator) {
        this.honorTitleDAOProvider = honorTitleDAOProvider;
        this.raceDAOProvider = raceDAOProvider;
        this.provinceDAOProvider = provinceDAOProvider;
        this.kingdomDAOProvider = kingdomDAOProvider;
        this.commonEntitiesAccess = commonEntitiesAccess;
        this.wpaCalculator = wpaCalculator;
        this.tpaCalculator = tpaCalculator;

        compilePatterns();
        eventBus.register(this);
    }

    private void compilePatterns() {
        nameLocPattern = Pattern.compile("(kingdom of|Current kingdom is) ([^(]+)(" + UtopiaValidationType.KDLOC.getPatternString() + ')');
        amountOfProvsPattern = Pattern.compile("Total Provinces\\s*(" + ValidationType.INT.getPattern() + ')');
        warPattern = Pattern.compile("This kingdom is currently at war with ([^(]+) (" + UtopiaValidationType.KDLOC.getPatternString() + ')');

        oldProvListStart = Pattern.compile("Slot\\n?\\s+Province\\n?\\s+Race\\n?\\s+Land\\n?\\s+(?:Networth|Net Worth)\\n?\\s+(?:NW/Acre|Net Worth/Acre)\\n?\\s+(?:Honour|Nobility)(.+)", Pattern.DOTALL);
        oldProvincePattern = Pattern.compile("\\d{1,2}(?!\\s+-\\s+-\\s+-)\\s+(.+?)(\\*|\\+|\\^|~|\\+| \\(M\\)| \\(S\\))*\\n?" +
                "\\s+(" + commonEntitiesAccess.getRaceGroup() + ')' + "\\n?" +
                "\\s+(" + ValidationType.INT.getPattern() + ") acres" + "\\n?" +
                "\\s+(" + ValidationType.INT.getPattern() + ")gc" + "\\n?" +
                "\\s+" + ValidationType.INT.getPattern() + "gc" + "\\n?" +
                "\\s+(" + commonEntitiesAccess.getHonorTitleGroup() + ')');

        provListStartPattern = Pattern.compile("On / Graphs\\n?\\s+Province\\n?\\s+Race\\n?\\s+Land\\n?\\s+(?:Networth|Net Worth)\\n?\\s+(?:NW/Acre|Net Worth/Acre|NWPA)\\n?\\s+(?:Honour|Nobility)\\s+(?:Gains)(.+)", Pattern.DOTALL);
        provincePattern = Pattern.compile("(?!\\s+-\\s+-\\s+-)\\s+(.+?)(\\*|\\+|\\^|~|\\+| \\(M\\)| \\(S\\))*\\n?" +
                "\\s+(" + commonEntitiesAccess.getRaceGroup() + ')' + "\\n?" +
                "\\s+(" + ValidationType.INT.getPattern() + ")a" + "\\n?" +
                "\\s+(" + ValidationType.INT.getPattern() + ")gc" + "\\n?" +
                "\\s+" + ValidationType.INT.getPattern() + "gc" + "\\n?" +
                "\\s+(" + commonEntitiesAccess.getHonorTitleGroup() + ')' +
                "(?:\\s+\\d+)");

        selfKDprovListStartPattern = Pattern.compile("On / Graphs\\n?\\s+Province\\n?\\s+Race\\n?\\s+Land\\n?\\s+(?:Networth|Net Worth)\\n?\\s+(?:NW/Acre|Net Worth/Acre|NWPA)\\n?\\s+(?:Honour|Nobility)(.+)", Pattern.DOTALL);
        selfKDprovincePattern = Pattern.compile("(?!\\s+-\\s+-\\s+-)\\s+(.+?)(\\*|\\+|\\^|~|\\+| \\(M\\)| \\(S\\))*\\n?" +
                "\\s+(" + commonEntitiesAccess.getRaceGroup() + ')' + "\\n?" +
                "\\s+(" + ValidationType.INT.getPattern() + ")a" + "\\n?" +
                "\\s+(" + ValidationType.INT.getPattern() + ")gc" + "\\n?" +
                "\\s+" + ValidationType.INT.getPattern() + "gc" + "\\n?" +
                "\\s+(" + commonEntitiesAccess.getHonorTitleGroup() + ')');

        invalidProvincePattern = Pattern.compile("/|<|>|\"");
    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    @Override
    public Pattern getIdentifierPattern() {
        return nameLocPattern;
    }

    @Override
    public Kingdom parse(final String savedBy, final String text) throws ParseException {
        Kingdom kingdom;
        Pattern provPattern;
        Matcher matcher = nameLocPattern.matcher(text);
        if (matcher.find()) {
            kingdom = kingdomDAOProvider.get().getOrCreateKingdom(matcher.group(3));
            kingdom.setName(matcher.group(2).trim());
        } else throw new ParseException("KD to be parsed does not contain name and location", 0);

        Map<String, Province> parsedProvs = new HashMap<>();
        Map<String, Province> existingProvs = new HashMap<>();
        for (Province province : kingdom.getProvinces()) {
            existingProvs.put(StringUtil.lowerCase(province.getName()), province);
        }

        matcher = amountOfProvsPattern.matcher(text);
        int amountOfProvs = matcher.find() ? NumberUtil.parseInt(matcher.group(1)) : 0;

        matcher = warPattern.matcher(text);
        if (matcher.find()) { kingdom.setWarTarget(matcher.group(2)); } else kingdom.setWarTarget(null);

        //Need to refactor all this to a single iterator
        matcher = provListStartPattern.matcher(text);
        provPattern = provincePattern;

        if (!matcher.find()) {
            matcher = selfKDprovListStartPattern.matcher(text);
            provPattern = selfKDprovincePattern;
            if (!matcher.find()) {
                matcher = oldProvListStart.matcher(text);
                provPattern = oldProvincePattern;
                if (!matcher.find()) throw new ParseException("KD pasted does not contain a recognized KD header", 0);
            }
        }

        String provList = matcher.group(1);

        ProvinceDAO provinceDAO = provinceDAOProvider.get();
        matcher = provPattern.matcher(provList);
        while (matcher.find()) {
            String name = matcher.group(1).trim().replaceAll(" +", " ");
            Matcher invalidProvinceMatcher = invalidProvincePattern.matcher(name);
            if (invalidProvinceMatcher.find()) throw new ParseException("KD pasted contains provinces with invalid characters.", 0);

            Province province = existingProvs.get(StringUtil.lowerCase(name));
            if (province == null) {
                Province existingInOtherKingdom = provinceDAO.getProvince(name);
                if (existingInOtherKingdom == null) province = provinceDAO.save(new Province(name, kingdom));
                else {
                    provinceDAO.delete(existingInOtherKingdom);
                    province = provinceDAO.save(new Province(name, kingdom));
                }
                kingdom.getProvinces().add(province);
                parsedProvs.put(StringUtil.lowerCase(province.getName()), province);
            } else {
                province.setName(name);
                province.setKingdom(kingdom);
                parsedProvs.put(StringUtil.lowerCase(province.getName()), province);
            }
            if (matcher.group(2) != null && matcher.group(2).trim().contains("*")) province.setLastSeen(new Date());
            Race race = raceDAOProvider.get().getRace(matcher.group(3).trim());
            province.setRace(race);
            province.setLand(NumberUtil.parseInt(matcher.group(4).trim()));
            province.setNetworth(NumberUtil.parseInt(matcher.group(5).trim()));
            String honorTitle = matcher.group(6).trim();
            HonorTitle title = StringUtil.isNullOrEmpty(honorTitle) ? commonEntitiesAccess.getLowestRankingHonorTitle() :
                    commonEntitiesAccess.getHonorTitle(honorTitle);
            province.setHonorTitle(title);
        }

        Set<Province> removedProvinces = new HashSet<>();
        for (String prov : existingProvs.keySet()) {
            if (!parsedProvs.containsKey(prov)) {
                removedProvinces.add(existingProvs.get(prov));
            }
        }

        if (parsedProvs.size() != amountOfProvs)
            throw new ParseException("Parsed " + parsedProvs.size() + " provinces, although " +
                    "there should have been " + amountOfProvs, 0);

        provinceDAO.delete(removedProvinces);
        kingdom.getProvinces().removeAll(removedProvinces);

        kingdom.setSavedBy(savedBy);
        //TODO: Took this out because it makes figuring out who the kingdom is most interested in lately difficult when we are posting every kingdom page visit via web addon.
        //TODO: To fix we'll wanna make kingdom page updating distinct from updating province intel or otherwise handle this via the API by making more info available.
//        kingdom.setLastUpdated(new Date());
//        for (Province province : kingdom.getProvinces()) {
//            province.setLastUpdated(kingdom.getLastUpdated(), false);
//        }
        return kingdom;
    }

    @Override
    public String getIntelTypeHandled() {
        return Kingdom.class.getSimpleName();
    }
}
