package parsers;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import api.tools.text.StringUtil;
import api.tools.time.DateUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.ProvinceDAO;
import database.models.*;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.parsing.UtopiaValidationType;
import tools.time.UtopiaTime;
import tools.time.UtopiaTimeFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Singleton
@Log4j
class MilitaryParser implements IntelParser<Military> {
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;

    private Pattern identifierPattern;
    private Pattern draftRatePattern;
    private Pattern draftTargetPattern;
    private Pattern wageRatePattern;
    private Pattern specCreditsPattern;

    private static final String EMPTY_STRING = "";

    @Inject
    MilitaryParser(final Provider<ProvinceDAO> provinceDAOProvider, final EventBus eventBus,
                       final Provider<BotUserDAO> botUserDAOProvider) {
        this.provinceDAOProvider = provinceDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;

        compilePatterns();
        eventBus.register(this);
    }

    private void compilePatterns() {
        identifierPattern = Pattern.compile("A strong and sustainable military is the key to survival, so train your troops wisely\\.\\s*" +
                "If you feel our military is too large, you can also release troops back into the peasantry at any time");
        specCreditsPattern = Pattern.compile("Free .*? credits left\\s*(" + ValidationType.INT.getPattern() + ")");
        draftRatePattern = Pattern.compile("Draft\\s*rate:\\s*(" + UtopiaValidationType.DRAFTRATES.getPattern() + ")\\s* " +
                        "\\((" + ValidationType.DOUBLE.getPattern() + ")% Rate");
        draftTargetPattern = Pattern.compile("Draft target \\(%\\):\\s*(" + ValidationType.INT.getPattern() + ")");
        wageRatePattern = Pattern.compile("Wage\\s*\\(%\\):\\s*(" + ValidationType.INT.getPattern() + ")");
    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    @Override
    public Pattern getIdentifierPattern() {
        return identifierPattern;
    }

    @Override
    public Military parse(final String savedBy, final String text) throws Exception {
        Military military = new Military();

        Province province;
        BotUser user = botUserDAOProvider.get().getUser(savedBy);
        province = provinceDAOProvider.get().getProvinceForUser(user);
        if (province == null)
            throw new ParseException("Military page contains no province name, and user " + user.getMainNick() + " has no registered province", 0);
        if (province.getMilitary() != null) military = province.getMilitary();
        else military.setProvince(province);

        Matcher matcher = specCreditsPattern.matcher(text);
        if (matcher.find()) {
            military.setSpecCredits(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = draftRatePattern.matcher(text);
        if (matcher.find()) {
            military.setDraftRate(NumberUtil.parseDouble(matcher.group(2)));
        }
        matcher = draftTargetPattern.matcher(text);
        if (matcher.find()) {
            military.setDraftTarget(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = wageRatePattern.matcher(text);
        if (matcher.find()) {
            military.setWageRate(NumberUtil.parseInt(matcher.group(1)));
        }

        military.setSavedBy(savedBy);
        military.setLastUpdated(new Date());

        return military;
    }

    @Override
    public String getIntelTypeHandled() {
        return Military.class.getSimpleName();
    }
}
