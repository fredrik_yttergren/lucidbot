/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package parsers;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Singleton;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import api.tools.text.RegexUtil;
import api.tools.time.DateUtil;
import database.CommonEntitiesAccess;
import database.daos.ArmyDAO;
import database.daos.ProvinceDAO;
import database.models.*;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.calculators.TPACalculator;
import tools.calculators.WPACalculator;
import tools.parsing.UtopiaValidationType;

@Singleton
@Log4j
class SoMParser implements IntelParser<SoM> {
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;
    private final Provider<ArmyDAO> armyDAOProvider;
    private final CommonEntitiesAccess commonEntitiesAccess;
    private final WPACalculator wpaCalculator;
    private final TPACalculator tpaCalculator;

    private Pattern provincePattern;
    private Pattern netOffPattern;
    private Pattern netDefPattern;
    private Pattern omePattern;
    private Pattern dmePattern;
    private Pattern armyReturnPattern;
    private Pattern generalsPattern;
    private Pattern soldiersPattern;
    private Pattern offSpecsPattern;
    private Pattern defSpecsPattern;
    private Pattern elitesPattern;
    private Pattern horsesPattern;
    private Pattern thievesPattern;
    private Pattern landPattern;

    private Pattern identifierPattern;
    private Pattern accuracyPattern;

    @Inject
    SoMParser(final CommonEntitiesAccess commonEntitiesAccess,
              final Provider<ProvinceDAO> provinceDAOProvider,
              final Provider<BotUserDAO> botUserDAOProvider,
              final Provider<ArmyDAO> armyDAOProvider,
              final EventBus eventBus,
              final WPACalculator wpaCalculator,
              final TPACalculator tpaCalculator) {
        this.provinceDAOProvider = provinceDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;
        this.armyDAOProvider = armyDAOProvider;
        this.commonEntitiesAccess = commonEntitiesAccess;
        this.wpaCalculator = wpaCalculator;
        this.tpaCalculator = tpaCalculator;

        compilePatterns();
        eventBus.register(this);
    }

    private void compilePatterns() {
        provincePattern = Pattern.compile("Our thieves listen in on a report from the Military Elders of ([^(]+)(" +
                UtopiaValidationType.KDLOC.getPatternString() + ')');
        netOffPattern = Pattern.compile("Net Offensive Points at Home\\s*(" +
                ValidationType.INT.getPattern() + ')');
        netDefPattern = Pattern.compile("Net Defensive Points at Home\\s*(" +
                ValidationType.INT.getPattern() + ')');
        omePattern = Pattern.compile("Offensive Military Effectiveness\\s*(" +
                ValidationType.STRICT_DOUBLE.getPattern() + ')');
        dmePattern = Pattern.compile("Defensive Military Effectiveness\\s*(" +
                ValidationType.STRICT_DOUBLE.getPattern() + ')');
        armyReturnPattern = Pattern.compile('(' + ValidationType.DOUBLE.getPattern() + ") days left");
        generalsPattern = Pattern.compile("Generals\\s+((?:" + UtopiaValidationType.GENERAL.getPatternString() + "\\s+)+)");
        soldiersPattern = Pattern.compile("Soldiers\\s+((?:" + ValidationType.INT.getPattern() + "\\s+)+)");
        offSpecsPattern = Pattern.compile("(?:" + commonEntitiesAccess.getOffSpecGroup() +
                ")\\s+((?:" + ValidationType.INT.getPattern() + "\\s+)+)");
        defSpecsPattern = Pattern.compile("(?:" + commonEntitiesAccess.getDefSpecGroup() +
                ")\\s+((?:(?:-|" + ValidationType.INT.getPattern() + ")\\s+)+)");
        elitesPattern = Pattern.compile("(?:" + commonEntitiesAccess.getEliteGroup() +
                ")\\s+((?:" + ValidationType.INT.getPattern() + "\\s+)+)");
        horsesPattern = Pattern.compile("War Horses\\s+((?:" + ValidationType.INT.getPattern() + "\\s+)+)");
        thievesPattern = Pattern.compile("Thieves\\s+((?:" + ValidationType.INT.getPattern() + "\\s+)+)");
        landPattern = Pattern.compile("Captured Land" +
                "\\s+((?:(?:-|" + ValidationType.INT.getPattern() + ")\\s+)+)");

        identifierPattern = Pattern.compile("(?:(?:" + provincePattern.pattern() +
                                            ".*?)?we have \\d generals available to lead our armies)");
        accuracyPattern = Pattern.compile("we have\\s*(" + ValidationType.INT.getPattern() + ")%\\s*confidence in the information retrieved");

    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    @Override
    public Pattern getIdentifierPattern() {
        return identifierPattern;
    }

    @Override
    public SoM parse(final String savedBy, final String text) throws Exception {
        SoM som = new SoM();
        Province province;

        Matcher matcher = provincePattern.matcher(text);
        if (matcher.find()) {
            ProvinceDAO provinceDao = provinceDAOProvider.get();
            String name = matcher.group(1).trim();
            province = provinceDao.getOrCreateProvince(name, matcher.group(2));
            province.setName(name);
            if (province.getSom() != null) som = province.getSom();
            else som.setProvince(province);
        } else {
            BotUser user = botUserDAOProvider.get().getUser(savedBy);
            province = provinceDAOProvider.get().getProvinceForUser(user);
            if (province == null)
                throw new ParseException("Self som contains no province name, and user " + user.getMainNick() + " has no registered province", 0);
            if (province.getSom() != null) som = province.getSom();
            else som.setProvince(province);
        }

        matcher = accuracyPattern.matcher(text);
        if (matcher.find()) {
            som.setAccuracy(NumberUtil.parseInt(matcher.group(1)));
        }

        matcher = netDefPattern.matcher(text);
        if (matcher.find()) {
            int netDef = NumberUtil.parseInt(matcher.group(1));
            som.setNetDefense(netDef);
        } else throw new ParseException("SoM to be parsed does not contain net def", 0);

        matcher = netOffPattern.matcher(text);
        if (matcher.find()) {
            int netOff = NumberUtil.parseInt(matcher.group(1));
            som.setNetOffense(netOff);
        } else throw new ParseException("SoM to be parsed does not contain net off", 0);

        matcher = omePattern.matcher(text);
        if (matcher.find()) {
            double ome = NumberUtil.parseDouble(matcher.group(1));
            som.setOffensiveME(ome);
        } else throw new ParseException("SoM to be parsed does not contain ome", 0);

        matcher = dmePattern.matcher(text);
        if (matcher.find()) {
            double dme = NumberUtil.parseDouble(matcher.group(1));
            som.setDefensiveME(dme);
        } else throw new ParseException("SoM to be parsed does not contain dme", 0);

        Set<Army> allArmies = new HashSet<>();
        Army armyHome = SoMArmyUtil.getOrCreateHomeArmy(som, som.getProvince());
        allArmies.add(armyHome);

        List<Army> armiesOut = new ArrayList<>();

        Army armyTraining = SoMArmyUtil.getOrCreateTrainingArmy(som, som.getProvince());
        allArmies.add(armyTraining);

        matcher = armyReturnPattern.matcher(text);
        int armyNo = 2;
        while (matcher.find()) {
            double time = NumberUtil.parseDouble(matcher.group(1));
            long returnTime = System.currentTimeMillis() + DateUtil.hoursToMillis(time);
            Army army = SoMArmyUtil.getOrCreateOutArmy(som, armyNo, som.getProvince());
            army.setReturningDate(new Date(returnTime));
            armiesOut.add(army);
            ++armyNo;
        }

        matcher = generalsPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            armyHome.setGenerals(NumberUtil.parseInt(split[0]));
            if (split.length > 1) {
                for (int i = 1; i < split.length && i <= armiesOut.size(); ++i) {
                    armiesOut.get(i - 1).setGenerals(NumberUtil.parseInt(split[i]));
                }
            }
        }

        matcher = soldiersPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            armyHome.setSoldiers(NumberUtil.parseInt(split[0]));
            if (split.length > 1) {
                for (int i = 1; i < split.length && i <= armiesOut.size(); ++i) {
                    armiesOut.get(i - 1).setSoldiers(NumberUtil.parseInt(split[i]));
                }
            }
        }

        matcher = offSpecsPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            armyHome.setOffSpecs(NumberUtil.parseInt(split[0]));
            if (split.length > 1) {
                for (int i = 1; i < split.length && i <= armiesOut.size(); ++i) {
                    armiesOut.get(i - 1).setOffSpecs(NumberUtil.parseInt(split[i]));
                }
            }

            if (matcher.find()) {
                split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
                int inTraining = 0;
                for (String aSplit : split) {
                    inTraining += NumberUtil.parseInt(aSplit);
                }
                armyTraining.setOffSpecs(inTraining);
            } else {
                armyTraining.setOffSpecs(0);
            }
        }

        matcher = defSpecsPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            armyHome.setDefSpecs(NumberUtil.parseInt(split[0]));

            if (matcher.find()) {
                split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
                int inTraining = 0;
                for (String aSplit : split) {
                    inTraining += NumberUtil.parseInt(aSplit);
                }
                armyTraining.setDefSpecs(inTraining);
            } else {
                armyTraining.setDefSpecs(0);
            }
        }

        matcher = elitesPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            armyHome.setElites(NumberUtil.parseInt(split[0]));
            if (split.length > 1) {
                for (int i = 1; i < split.length && i <= armiesOut.size(); ++i) {
                    armiesOut.get(i - 1).setElites(NumberUtil.parseInt(split[i]));
                }
            }

            if (matcher.find()) {
                split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
                int inTraining = 0;
                for (String aSplit : split) {
                    inTraining += NumberUtil.parseInt(aSplit);
                }
                armyTraining.setElites(inTraining);
            } else {
                armyTraining.setElites(0);
            }
        }

        matcher = horsesPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            armyHome.setWarHorses(NumberUtil.parseInt(split[0]));
            if (split.length > 1) {
                for (int i = 1; i < split.length && i <= armiesOut.size(); ++i) {
                    armiesOut.get(i - 1).setWarHorses(NumberUtil.parseInt(split[i]));
                }
            }
        }

        matcher = landPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            if (split.length > 1) {
                for (int i = 1; i < split.length && i <= armiesOut.size(); ++i) {
                    armiesOut.get(i - 1).setLandGained(NumberUtil.parseInt(split[i]));
                }
            }
        }

        matcher = thievesPattern.matcher(text);
        if (matcher.find()) {
            String[] split = RegexUtil.WHITESPACES_PATTERN.split(matcher.group(1));
            int inTraining = 0;
            for (String aSplit : split) {
                inTraining += NumberUtil.parseInt(aSplit);
            }
            armyTraining.setThieves(inTraining);
        } else {
            armyTraining.setThieves(0);
        }

        allArmies.addAll(armiesOut);

        for (Army army : allArmies) {
            army.setModOffense(calculateModOffense(province, army, som.getOffensiveME()));
            army.setModDefense(calculateModDefense(province, army, som.getDefensiveME()));
        }

        som.setSavedBy(savedBy);
        som.setLastUpdated(new Date());
        som.getProvince().setLastUpdated(som.getLastUpdated(), true);

        for (Iterator<Army> iter = som.getArmies().iterator(); iter.hasNext(); ) {
            Army entry = iter.next();
            if (!allArmies.contains(entry)) iter.remove();
        }

        som.setExportLine(null);

        armyDAOProvider.get().save(allArmies);
        som.setArmiesOutWhenPosted(som.getArmiesOut().size());

        return som;
    }

    @Override
    public String getIntelTypeHandled() {
        return SoM.class.getSimpleName();
    }

    private int calculateModOffense(final Province province, final Army army, final double ome) {
        Race race = province.getRace();
        Personality persona = province.getPersonality();

        double modOffense = 0;
        if (race != null && persona != null) {
            modOffense = race.getSoldierStrength() * army.getSoldiers();
            modOffense += (race.getOffSpecStrength() + persona.getOffSpecStrength()) * army.getOffSpecs();
            modOffense += race.getEliteOffStrength() * army.getElites();
        }

        if (ome != 0) { modOffense =  modOffense * ome * .01; }

        return (int) modOffense;
    }

    private int calculateModDefense(final Province province, final Army army, final double dme) {
        Race race = province.getRace();
        Personality persona = province.getPersonality();

        double modDefense = 0;
        if (race != null) {
            modDefense = race.getSoldierStrength() * army.getSoldiers();
            modDefense += (race.getDefSpecStrength() + persona.getDefSpecStrength()) * army.getDefSpecs();
            modDefense += race.getEliteDefStrength() * army.getElites();
        }

        if (dme != 0) { modDefense = modDefense * dme * .01; }

        return (int) modDefense;
    }
}
