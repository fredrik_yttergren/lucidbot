/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package parsers;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import api.tools.text.StringUtil;
import api.tools.time.DateUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.CommonEntitiesAccess;
import database.daos.DragonDAO;
import database.daos.ProvinceDAO;
import database.models.*;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.parsing.UtopiaValidationType;
import tools.time.UtopiaTime;
import tools.time.UtopiaTimeFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Singleton
@Log4j
class StateCouncilParser implements IntelParser<StateCouncil> {
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;

    private Pattern identifierPattern;
    private Pattern unemployedPeasantsPattern;
    private Pattern unfilledJobsHonorPattern;
    private Pattern employmentPattern;
    private Pattern totalPopIncomePattern;
    private Pattern maxPopWagesPattern;
    private Pattern netIncomePattern;
    private Pattern peasantsBornPattern;
    private Pattern draftCostPattern;
    private Pattern foodGrownPattern;
    private Pattern foodNeededPattern;
    private Pattern foodDecayedPattern;
    private Pattern netFoodPattern;
    private Pattern runesProducedPattern;
    private Pattern runesDecayedPattern;
    private Pattern netRunesPattern;

    private static final String EMPTY_STRING = "";

    @Inject
    StateCouncilParser(final Provider<ProvinceDAO> provinceDAOProvider, final EventBus eventBus,
                       final Provider<BotUserDAO> botUserDAOProvider) {
        this.provinceDAOProvider = provinceDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;

        compilePatterns();
        eventBus.register(this);
    }

    private void compilePatterns() {
        identifierPattern = Pattern.compile("I track some important information about the health of our province. I hope you will find this information useful");
        unemployedPeasantsPattern = Pattern.compile("Army\\s*(" + ValidationType.INT.getPattern() + ")\\s*Unemployed Peasants\\s*(" +
                ValidationType.INT.getPattern() + ')');
        unfilledJobsHonorPattern = Pattern.compile("Thieves\\s*(" + ValidationType.INT.getPattern() + ")\\s*Unfilled Jobs\\s*(" +
                ValidationType.INT.getPattern() + ")\\s*Current Honor\\s*(" + ValidationType.INT.getPattern() + ")");
        employmentPattern = Pattern.compile("Wizards\\s*(" + ValidationType.INT.getPattern() + ")\\s*Employment\\s*(" +
                ValidationType.INT.getPattern() + ')');
        totalPopIncomePattern = Pattern.compile("Total\\s*(" + ValidationType.INT.getPattern() + ")\\s*Daily Income\\s*(" +
                ValidationType.INT.getPattern() + ')');
        maxPopWagesPattern = Pattern.compile("Max Population\\s*(" + ValidationType.INT.getPattern() + ")\\s*Daily Wages\\s*(" +
                ValidationType.INT.getPattern() + ')');
        draftCostPattern = Pattern.compile("Military Wages\\s*(" + ValidationType.SIGNED_INT.getPattern() + ")gc");
        netIncomePattern = Pattern.compile("Net Change\\s*(" + ValidationType.SIGNED_INT.getPattern() + ")gc");
        peasantsBornPattern = Pattern.compile("Peasants\\s*(" + ValidationType.SIGNED_INT.getPattern() + ") peasants");
        foodGrownPattern = Pattern.compile("Food Grown\\s*(" + ValidationType.INT.getPattern() + ") bushels");
        foodNeededPattern = Pattern.compile("Food Needed\\s*(" + ValidationType.INT.getPattern() + ") bushels");
        foodDecayedPattern = Pattern.compile("Food Decayed\\s*(" + ValidationType.INT.getPattern() + ") bushels");
        netFoodPattern = Pattern.compile("Net Change\\s*(" + ValidationType.SIGNED_INT.getPattern() + ") bushels");

        runesProducedPattern = Pattern.compile("Runes Produced\\s*(" + ValidationType.INT.getPattern() + ") runes");
        runesDecayedPattern = Pattern.compile("Runes Decayed\\s*(" + ValidationType.INT.getPattern() + ") runes");
        netRunesPattern = Pattern.compile("Net Change\\s*(" + ValidationType.SIGNED_INT.getPattern() + ") runes");

    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    @Override
    public Pattern getIdentifierPattern() {
        return identifierPattern;
    }

    @Override
    public StateCouncil parse(final String savedBy, final String text) throws Exception {
        StateCouncil sc = new StateCouncil();

        Province province;
        BotUser user = botUserDAOProvider.get().getUser(savedBy);
        province = provinceDAOProvider.get().getProvinceForUser(user);
        if (province == null)
            throw new ParseException("State Council contains no province name, and user " + user.getMainNick() + " has no registered province", 0);
        if (province.getStateCouncil() != null) sc = province.getStateCouncil();
        else sc.setProvince(province);

        Matcher matcher = unemployedPeasantsPattern.matcher(text);
        if (matcher.find()) {
            sc.setUnemployedPeasants(NumberUtil.parseInt(matcher.group(2)));
        }
        matcher = unfilledJobsHonorPattern.matcher(text);
        if (matcher.find()) {
            sc.setUnfilledJobs(NumberUtil.parseInt(matcher.group(2)));
            sc.setHonor(NumberUtil.parseInt(matcher.group(3)));
        }
        matcher = employmentPattern.matcher(text);
        if (matcher.find()) {
            sc.setEmployment(NumberUtil.parseInt(matcher.group(2)));
        }
        matcher = totalPopIncomePattern.matcher(text);
        if (matcher.find()) {
            sc.setTotalPopulation(NumberUtil.parseInt(matcher.group(1)));
            sc.setIncome(NumberUtil.parseInt(matcher.group(2)));
        }
        matcher = maxPopWagesPattern.matcher(text);
        if (matcher.find()) {
            sc.setMaxPopulation(NumberUtil.parseInt(matcher.group(1)));
            sc.setWages(NumberUtil.parseInt(matcher.group(2)));
        }
        matcher = draftCostPattern.matcher(text);
        if (matcher.find()) {
            sc.setDraftCost(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = netIncomePattern.matcher(text);
        if (matcher.find()) {
            sc.setNetIncome(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = peasantsBornPattern.matcher(text);
        if (matcher.find()) {
            sc.setPeasantsBorn(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = foodGrownPattern.matcher(text);
        if (matcher.find()) {
            sc.setFoodGrown(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = foodNeededPattern.matcher(text);
        if (matcher.find()) {
            sc.setFoodNeeded(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = foodDecayedPattern.matcher(text);
        if (matcher.find()) {
            sc.setFoodDecayed(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = netFoodPattern.matcher(text);
        if (matcher.find()) {
            sc.setNetFood(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = runesProducedPattern.matcher(text);
        if (matcher.find()) {
            sc.setRunesProduced(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = runesDecayedPattern.matcher(text);
        if (matcher.find()) {
            sc.setRunesDecayed(NumberUtil.parseInt(matcher.group(1)));
        }
        matcher = netRunesPattern.matcher(text);
        if (matcher.find()) {
            sc.setNetRunes(NumberUtil.parseInt(matcher.group(1)));
        }

        sc.setSavedBy(savedBy);
        sc.setLastUpdated(new Date());

        return sc;
    }

    @Override
    public String getIntelTypeHandled() {
        return StateCouncil.class.getSimpleName();
    }
}
