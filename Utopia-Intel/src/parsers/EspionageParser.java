package parsers;


import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.irc.ValidationType;
import database.CommonEntitiesAccess;
import database.daos.HonorTitleDAO;
import database.daos.KingdomDAO;
import database.daos.ProvinceDAO;
import database.daos.RaceDAO;
import database.models.*;
import events.CacheReloadEvent;
import intel.IntelParser;
import tools.calculators.TPACalculator;
import tools.calculators.WPACalculator;
import tools.parsing.UtopiaValidationType;

import java.text.ParseException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Pattern;

import api.irc.ValidationType;
import api.tools.numbers.NumberUtil;
import api.tools.text.StringUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.CommonEntitiesAccess;
import database.daos.HonorTitleDAO;
import database.daos.KingdomDAO;
import database.daos.ProvinceDAO;
import database.daos.RaceDAO;
import database.models.Kingdom;
import events.CacheReloadEvent;
import intel.IntelParser;
import lombok.extern.log4j.Log4j;
import tools.calculators.TPACalculator;
import tools.calculators.WPACalculator;
import tools.parsing.UtopiaValidationType;
import tools.time.UtopiaTime;
import tools.time.UtopiaTimeFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Singleton
@Log4j
class EspionageParser {
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;
    private final CommonEntitiesAccess commonEntitiesAccess;
    private final UtopiaTimeFactory utopiaTimeFactory;

    private final ConcurrentMap<Pattern, SpellType> spellPatternsMap = new ConcurrentHashMap<>();

    private Pattern provinceLinePattern;

    @Inject
    EspionageParser(final CommonEntitiesAccess commonEntitiesAccess, final Provider<ProvinceDAO> provinceDAOProvider,
                    final Provider<BotUserDAO> botUserDAOProvider,
                    final EventBus eventBus, final UtopiaTimeFactory utopiaTimeFactory) {
        this.provinceDAOProvider = provinceDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;
        this.commonEntitiesAccess = commonEntitiesAccess;
        this.utopiaTimeFactory = utopiaTimeFactory;

        for (SpellType spellType : commonEntitiesAccess.getAllSpellTypes()) {
            if (spellType.getCastRegex() != null) spellPatternsMap.put(Pattern.compile(spellType.getName() +
                    "\\s*\\((" + ValidationType.INT.getPattern() + ')'), spellType);
        }

        compilePatterns();
    }

    private void compilePatterns() {
        provinceLinePattern = Pattern.compile("\\d+\\s+([^(])\\s+\\(" + commonEntitiesAccess.getRaceGroup() + "\\)" + "(.+)");
    }

    @Subscribe
    public void onCacheReload(final CacheReloadEvent event) {
        compilePatterns();
    }

    public void parse(final String savedBy, final String text) throws ParseException {
        Matcher matcher = provinceLinePattern.matcher(text);
        BotUserDAO botUserDAO = botUserDAOProvider.get();
        BotUser user = botUserDAO.getUser(savedBy);
        while (matcher.find()) {
            String provName = matcher.group(0).trim();
            Province province = provinceDAOProvider.get().getProvince(provName);
            for (Map.Entry<Pattern, SpellType> entry : spellPatternsMap.entrySet()) {
                matcher = entry.getKey().matcher(matcher.group(1));
                if (matcher.find()) {
                    SpellType spellType = entry.getValue();
                    UtopiaTime currentTime = utopiaTimeFactory.newUtopiaTime(System.currentTimeMillis());
                    UtopiaTime expires = currentTime.increment(NumberUtil.parseInt(matcher.group(1)) + 1);

                    province.addThroneSpell(new DurationSpell(user, province, new Date(expires.getTime()), spellType, 1, new Date()));
                }
            }
        }
    }
}
