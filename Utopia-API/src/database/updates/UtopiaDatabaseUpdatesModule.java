/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package database.updates;

import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;

import api.database.updates.DatabaseUpdater;
//This is Importing all of the h2 and mysql database update classes
import database.updates.h2.*;
import database.updates.mysql.*;


public class UtopiaDatabaseUpdatesModule extends AbstractModule {

  @Override
  protected void configure() {
    Multibinder<DatabaseUpdater> binder = Multibinder.newSetBinder(binder(), DatabaseUpdater.class);
    binder.addBinding().to(H2UpdateV1ToV2.class);
    binder.addBinding().to(MySQLUpdateV1ToV2.class);
    binder.addBinding().to(H2UpdateV3ToV4.class);
    binder.addBinding().to(MySQLUpdateV3ToV4.class);
    binder.addBinding().to(H2UpdateV4ToV5.class);
    binder.addBinding().to(MySQLUpdateV4ToV5.class);
    binder.addBinding().to(H2UpdateV5ToV6.class);
    binder.addBinding().to(MySQLUpdateV5ToV6.class);
    binder.addBinding().to(H2UpdateV6ToV7.class);
    binder.addBinding().to(MySQLUpdateV6ToV7.class);
    binder.addBinding().to(H2UpdateV7ToV8.class);
    binder.addBinding().to(MySQLUpdateV7ToV8.class);
    binder.addBinding().to(H2UpdateV8ToV9.class);
    binder.addBinding().to(MySQLUpdateV8ToV9.class);
    binder.addBinding().to(H2UpdateV9ToV10.class);
    binder.addBinding().to(MySQLUpdateV9ToV10.class);
    binder.addBinding().to(H2UpdateV10ToV11.class);
    binder.addBinding().to(MySQLUpdateV10ToV11.class);
    binder.addBinding().to(H2UpdateV11ToV12.class);
    binder.addBinding().to(MySQLUpdateV11ToV12.class);
    binder.addBinding().to(H2UpdateV12ToV13.class);
    binder.addBinding().to(MySQLUpdateV12ToV13.class);
    binder.addBinding().to(H2UpdateV13ToV14.class);
    binder.addBinding().to(MySQLUpdateV13ToV14.class);
    binder.addBinding().to(H2UpdateV14ToV15.class);
    binder.addBinding().to(MySQLUpdateV14ToV15.class);
    binder.addBinding().to(H2UpdateV15ToV16.class);
    binder.addBinding().to(MySQLUpdateV15ToV16.class);
    binder.addBinding().to(H2UpdateV16ToV17.class);
    binder.addBinding().to(MySQLUpdateV16ToV17.class);
    binder.addBinding().to(H2UpdateV17ToV18.class);
    binder.addBinding().to(MySQLUpdateV17ToV18.class);
    binder.addBinding().to(H2UpdateV18ToV19.class);
    binder.addBinding().to(MySQLUpdateV18ToV19.class);
    binder.addBinding().to(H2UpdateV19ToV20.class);
    binder.addBinding().to(MySQLUpdateV19ToV20.class);
    binder.addBinding().to(H2UpdateV20ToV21.class);
    binder.addBinding().to(MySQLUpdateV20ToV21.class);
    binder.addBinding().to(H2UpdateV21ToV22.class);
    binder.addBinding().to(MySQLUpdateV21ToV22.class);
    binder.addBinding().to(H2UpdateV22ToV23.class);
    binder.addBinding().to(MySQLUpdateV22ToV23.class);
    binder.addBinding().to(H2UpdateV23ToV24.class);
    binder.addBinding().to(MySQLUpdateV23ToV24.class);
    binder.addBinding().to(H2UpdateV24ToV25.class);
    binder.addBinding().to(MySQLUpdateV24ToV25.class);
    binder.addBinding().to(H2UpdateV25ToV26.class);
    binder.addBinding().to(MySQLUpdateV25ToV26.class);
    binder.addBinding().to(H2UpdateV26ToV27.class);
    binder.addBinding().to(MySQLUpdateV26ToV27.class);
    binder.addBinding().to(H2UpdateV27ToV28.class);
    binder.addBinding().to(MySQLUpdateV27ToV28.class);
    binder.addBinding().to(H2UpdateV28ToV29.class);
    binder.addBinding().to(MySQLUpdateV28ToV29.class);
    binder.addBinding().to(H2UpdateV29ToV30.class);
    binder.addBinding().to(MySQLUpdateV29ToV30.class);
    binder.addBinding().to(H2UpdateV30ToV31.class);
    binder.addBinding().to(MySQLUpdateV30ToV31.class);
    binder.addBinding().to(H2UpdateV31ToV32.class);
    binder.addBinding().to(MySQLUpdateV31ToV32.class);
    binder.addBinding().to(H2UpdateV32ToV33.class);
    binder.addBinding().to(MySQLUpdateV32ToV33.class);
    binder.addBinding().to(H2UpdateV33ToV34.class);
    binder.addBinding().to(MySQLUpdateV33ToV34.class);
    binder.addBinding().to(H2UpdateV34ToV35.class);
    binder.addBinding().to(MySQLUpdateV34ToV35.class);
    binder.addBinding().to(H2UpdateV35ToV36.class);
    binder.addBinding().to(MySQLUpdateV35ToV36.class);
    binder.addBinding().to(H2UpdateV36ToV37.class);
    binder.addBinding().to(MySQLUpdateV36ToV37.class);
    binder.addBinding().to(H2UpdateV37ToV38.class);
    binder.addBinding().to(MySQLUpdateV37ToV38.class);
    binder.addBinding().to(H2UpdateV38ToV39.class);
    binder.addBinding().to(MySQLUpdateV38ToV39.class);
    binder.addBinding().to(H2UpdateV39ToV40.class);
    binder.addBinding().to(MySQLUpdateV39ToV40.class);
    binder.addBinding().to(H2UpdateV40ToV41.class);
    binder.addBinding().to(MySQLUpdateV40ToV41.class);
    binder.addBinding().to(H2UpdateV41ToV42.class);
    binder.addBinding().to(MySQLUpdateV41ToV42.class);
    binder.addBinding().to(H2UpdateV42ToV43.class);
    binder.addBinding().to(MySQLUpdateV42ToV43.class);
    binder.addBinding().to(H2UpdateV43ToV44.class);
    binder.addBinding().to(MySQLUpdateV43ToV44.class);
    binder.addBinding().to(H2UpdateV44ToV45.class);
    binder.addBinding().to(MySQLUpdateV44ToV45.class);
    binder.addBinding().to(H2UpdateV45ToV46.class);
    binder.addBinding().to(MySQLUpdateV45ToV46.class);
    binder.addBinding().to(H2UpdateV46ToV47.class);
    binder.addBinding().to(MySQLUpdateV46ToV47.class);
    binder.addBinding().to(H2UpdateV47ToV48.class);
    binder.addBinding().to(MySQLUpdateV47ToV48.class);
    binder.addBinding().to(H2UpdateV48ToV49.class);
    binder.addBinding().to(MySQLUpdateV48ToV49.class);
    binder.addBinding().to(H2UpdateV49ToV50.class);
    binder.addBinding().to(MySQLUpdateV49ToV50.class);
    binder.addBinding().to(H2UpdateV50ToV51.class);
    binder.addBinding().to(MySQLUpdateV50ToV51.class);
    binder.addBinding().to(H2UpdateV51ToV52.class);
    binder.addBinding().to(MySQLUpdateV51ToV52.class);
    binder.addBinding().to(H2UpdateV52ToV53.class);
    binder.addBinding().to(MySQLUpdateV52ToV53.class);
    binder.addBinding().to(H2UpdateV53ToV54.class);
    binder.addBinding().to(MySQLUpdateV53ToV54.class);
    binder.addBinding().to(H2UpdateV54ToV55.class);
    binder.addBinding().to(MySQLUpdateV54ToV55.class);
    binder.addBinding().to(H2UpdateV55ToV56.class);
    binder.addBinding().to(MySQLUpdateV55ToV56.class);
    binder.addBinding().to(H2UpdateV56ToV57.class);
    binder.addBinding().to(MySQLUpdateV56ToV57.class);
    binder.addBinding().to(H2UpdateV57ToV58.class);
    binder.addBinding().to(MySQLUpdateV57ToV58.class);
    binder.addBinding().to(H2UpdateV58ToV59.class);
    binder.addBinding().to(MySQLUpdateV58ToV59.class);
    binder.addBinding().to(H2UpdateV59ToV60.class);
    binder.addBinding().to(MySQLUpdateV59ToV60.class);
    binder.addBinding().to(H2UpdateV60ToV61.class);
    binder.addBinding().to(MySQLUpdateV60ToV61.class);
    binder.addBinding().to(H2UpdateV61ToV62.class);
    binder.addBinding().to(MySQLUpdateV61ToV62.class);
    binder.addBinding().to(H2UpdateV62ToV63.class);
    binder.addBinding().to(MySQLUpdateV62ToV63.class);
    binder.addBinding().to(H2UpdateV63ToV64.class);
    binder.addBinding().to(MySQLUpdateV63ToV64.class);
  }
}
