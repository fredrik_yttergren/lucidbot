package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV62ToV63 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 63;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE personality SET off_spec_strength = 0, def_spec_strength = 0 WHERE name = 'War Hero'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Faery WPA', 'WPA', 'BOTH', true, 0.2)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery WPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Heretic WPA', 'WPA', 'BOTH', true, 0.25)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Heretic TPA', 'TPA', 'BOTH', true, 0.25)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'")
        );
    }
}
