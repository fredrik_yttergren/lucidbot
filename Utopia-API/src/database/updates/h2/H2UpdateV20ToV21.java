package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV20ToV21 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 21;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN thieves_accurate BIT"),
                new SimpleUpdateAction("UPDATE province SET thieves_accurate = False"),
                new SimpleUpdateAction("ALTER TABLE province MODIFY COLUMN thieves_accurate BIT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN wizards_accurate BIT"),
                new SimpleUpdateAction("UPDATE province SET wizards_accurate = False"),
                new SimpleUpdateAction("ALTER TABLE province MODIFY COLUMN wizards_accurate BIT NOT NULL")
        );
    }
}