package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV35ToV36 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 36;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_networth = 8.75 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 9, elite_def_strength = 6 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .4 WHERE name = 'Elf WPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 5, elite_def_strength = 9, off_spec_strength = 8, elite_networth = 11 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .5 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 6, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Halfling'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Halfling Pop', 'POP', 'BOTH', true, .10)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling Pop'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Human WPA', 'WPA', 'BOTH', false, .10)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Human' AND bonus.name = 'Human WPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 10, elite_def_strength = 3 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 9.5 WHERE name = 'Undead'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .2 WHERE name = 'Heretic WPA'")
        );
    }
}
