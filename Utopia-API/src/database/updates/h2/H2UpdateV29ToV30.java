package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV29ToV30 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 30;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE duration_op ADD COLUMN amount INT"),
                new SimpleUpdateAction("UPDATE duration_op SET amount = 1"),
                new SimpleUpdateAction("ALTER TABLE duration_op MODIFY COLUMN amount INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE duration_spell ADD COLUMN amount INT"),
                new SimpleUpdateAction("UPDATE duration_spell SET amount = 1"),
                new SimpleUpdateAction("ALTER TABLE duration_spell MODIFY COLUMN amount INT NOT NULL")
        );
    }
}
