package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV36ToV37 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 37;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('Our people are increasingly drawn to the sciences for (?<result>[\\d,]+) days!', 'Increases scientist spawn rate by 100%', 'Revelation', '', 're', 'SELF_SPELLOP')")
        );
    }
}
