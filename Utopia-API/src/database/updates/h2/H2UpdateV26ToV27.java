package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV26ToV27 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 27;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_networth = 6 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.2 where name = 'Orc Gains'"),
                new SimpleUpdateAction("DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality INNER JOIN bonus ON personality_bonus.personality_id = bonus.id WHERE " +
                        "personality.name = 'Warrior' AND bonus.type = 'OME_IN_WAR' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Warrior OME In War'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Human OME In War', 'OME_IN_WAR', 'OFFENSIVELY', true, .2)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Human' AND bonus.name = 'Human OME In War'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Human Gains', 'GAIN', 'OFFENSIVELY', false, .1)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Human' AND bonus.name = 'Human Gains'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Halfling Pop', 'POP', 'BOTH', true, .1)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling Pop'")
        );
    }
}