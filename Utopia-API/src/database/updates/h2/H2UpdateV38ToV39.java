package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV38ToV39 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 39;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_def_strength = 5 WHERE name = 'Dwarf'")
        );
    }
}
