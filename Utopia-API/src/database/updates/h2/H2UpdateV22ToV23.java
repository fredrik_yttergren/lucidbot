package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV22ToV23 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 23;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.25 WHERE name = 'Faery'")
        );
    }
}