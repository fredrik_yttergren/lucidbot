package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV58ToV59 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 59;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("CREATE TABLE military (id BIGINT, spec_credits INT, wage_rate INT, draft_target INT, " +
                        "draft_rate DOUBLE, last_updated DATETIME, saved_by VARCHAR(200)," +
                        "province_id BIGINT, PRIMARY KEY (id))"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN id BIGINT NOT NULL AUTO_INCREMENT"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN spec_credits INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN wage_rate INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN draft_target INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN draft_rate DOUBLE NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN last_updated DATETIME NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN saved_by VARCHAR(200) NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE military MODIFY COLUMN province_id BIGINT NOT NULL")
                );
    }
}
