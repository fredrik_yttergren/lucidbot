package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import database.updates.mysql.ApiMySQLDatabaseUpdater;


public class H2UpdateV28ToV29 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 29;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("DROP TABLE IF EXISTS nub"),
                new SimpleUpdateAction("CREATE TABLE nub (id BIGINT, nub_name VARCHAR(50), reason VARCHAR(5000), " +
                        "setter VARCHAR(50), set_time DATETIME, PRIMARY KEY (id))"),
                new SimpleUpdateAction("ALTER TABLE nub MODIFY COLUMN id BIGINT NOT NULL AUTO_INCREMENT"),
                new SimpleUpdateAction("ALTER TABLE nub MODIFY COLUMN nub_name VARCHAR(50) NOT NULL"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 3, elite_networth = 8.75, soldier_strength = 2 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 7, elite_networth = 10, soldier_strength = 2 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 8, off_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 6, elite_networth = 8.5, soldier_strength = 2 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 9, elite_networth = 11, soldier_strength = 2 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 8, elite_off_strength = 6, elite_def_strength = 6, elite_networth = 8.5, soldier_strength = 3 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 4, elite_networth = 9.25, soldier_strength = 2 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 6, elite_off_strength = 12, elite_def_strength = 2, elite_networth = 10.5, soldier_strength = 2 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 6, off_spec_strength = 6, elite_off_strength = 11, elite_def_strength = 3, elite_networth = 10, soldier_strength = 2 WHERE name = 'Undead'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Elf' AND bonus.type = 'WPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Elf WPA'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Human' AND bonus.type = 'OME_IN_WAR' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Human OME In War'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Human' AND bonus.type = 'GAIN' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Human Gains'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .12 WHERE name = 'Halfling Pop'"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('The gluttony of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) has increased for (?<result>[\\d,]+) days.', 'Increases food consumption by 25%', 'Gluttony', '', 'glut', 'FADING_SPELLOP_WITH_PROVINCE')"),
                new SimpleUpdateAction("MERGE INTO personality (alias, cons, dragon_immune, intel_accuracy, name, pros) KEY(name) VALUES('Skeptic', '', false, 'NEVER', 'Heretic', '')")
        );
    }
}
