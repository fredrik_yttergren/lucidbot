package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV27ToV28 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 28;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("DROP TABLE IF EXISTS nub"),
                new SimpleUpdateAction("CREATE TABLE nub (id BIGINT, nub_name VARCHAR(50), reason VARCHAR(5000), " +
                        "setter VARCHAR(50), set_time DATETIME, PRIMARY KEY (id))"),
                new SimpleUpdateAction("ALTER TABLE nub MODIFY COLUMN id BIGINT NOT NULL AUTO_INCREMENT"),
                new SimpleUpdateAction("ALTER TABLE nub MODIFY COLUMN nub_name VARCHAR(50) NOT NULL")
        );
    }
}