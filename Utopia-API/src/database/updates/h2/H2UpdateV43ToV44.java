package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV43ToV44 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 44;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE kingdom DROP COLUMN kingdom_ritual"),
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN kingdom_ritual_id BIGINT DEFAULT NULL"),
                new SimpleUpdateAction("ALTER TABLE news_item ADD COLUMN snatch_news BIT NOT NULL DEFAULT FALSE"),
                new SimpleUpdateAction("CREATE TABLE kingdom_ritual (id BIGINT NOT NULL AUTO_INCREMENT, name VARCHAR(100), PRIMARY KEY (id))"),
                new SimpleUpdateAction("CREATE TABLE kingdom_ritual_action (id BIGINT NOT NULL AUTO_INCREMENT, contribution INT, updated DATETIME, kingdom_ritual_project_id BIGINT, " +
                        "bot_user_id BIGINT, PRIMARY KEY (id))"),
                new SimpleUpdateAction("CREATE TABLE kingdom_ritual_bonus (kingdom_ritual_id BIGINT NOT NULL AUTO_INCREMENT, bonus_id BIGINT, PRIMARY KEY (kingdom_ritual_id, bonus_id))"),
                new SimpleUpdateAction("CREATE TABLE kingdom_ritual_project (id BIGINT NOT NULL AUTO_INCREMENT, created DATETIME, original_status INT, status INT, " +
                        "type VARCHAR(100), updated DATETIME, PRIMARY KEY (id))"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Barrier')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Affluent')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Onslaught')"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Kingdom Ritual Gains', 'GAIN', 'DEFENSIVELY', false, .1)"),
                new SimpleUpdateAction(
                        "MERGE INTO kingdom_ritual_bonus (kingdom_ritual_id, bonus_id) SELECT kingdom_ritual.id,bonus.id FROM kingdom_ritual INNER JOIN bonus WHERE kingdom_ritual.name = 'Barrier' AND bonus.name = 'Kingdom Ritual Gains'"),
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'Our realm is now under a sphere of protection for (?<result>[\\\\d,]+)' WHERE name = 'Minor Protection'"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name) VALUES('Our army has been inspired by our Paladin to train even harder. We expect maintenance costs to be reduced for (?<result>[\\\\d,]+) days!', 'Reduces maintenance costs by 15% and reduces training by 25%', 'Paladins Inspiration', '', 'pi', 'SELF_SPELLOP')"),
                new SimpleUpdateAction("UPDATE spell_type SET spell_character = 'SELF_SPELLOP', cast_regex = 'Your mages fill your province with holy light, reducing shadows for thieves to hide in for (?<result>[\\\\d,]+) days' WHERE name = 'Illuminate Shadows'"),
                new SimpleUpdateAction("UPDATE spell_type SET spell_character = 'SELF_SPELLOP', cast_regex = 'You imbue your province with a holy shield, protecting against foul sorcery for (?<result>[\\\\d,]+) days' WHERE name = 'Divine Shield'")

        );
    }
}
