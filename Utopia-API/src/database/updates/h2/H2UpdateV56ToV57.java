package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV56ToV57 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 57;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Fix Heretic
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Heretic' AND bonus.type = 'WPA' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Heretic WPA'"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Heretic' AND bonus.type = 'TPA' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Heretic TPA'"),
                //Remove Sci Bonuses, not needed anymore since we are given the number of days of training per category
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Cleric' AND bonus.type = 'HOUSING_SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Mystic' AND bonus.type = 'CHANNELING_SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Sage' AND bonus.type = 'SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Rogue' AND bonus.type = 'CRIME_SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Warrior' AND bonus.type = 'MILITARY_SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Heretic' AND bonus.type = 'CHANNELING_SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Cleric' AND bonus.type = 'HOUSING_SCI_EFF' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Undead' AND bonus.type = 'SCI_EFF' AND race_bonus.race_id = race.id)")
                );
    }
}
