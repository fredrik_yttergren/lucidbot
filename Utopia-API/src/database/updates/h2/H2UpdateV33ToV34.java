package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV33ToV34 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 34;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('Blizzards will beset the works of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) for for (?<result>[\\d,]+) days.', 'Reduces the building effectiveness of a province by 10%', 'Blizzard', '', 'bliz', 'FADING_SPELLOP_WITH_PROVINCE')")
                );
    }
}
