package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV24ToV25 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 25;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Lord Pop Bonus', 'POP', 'BOTH', true, .01)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Baron Pop Bonus', 'POP', 'BOTH', true, .02)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Viscount Pop Bonus', 'POP', 'BOTH', true, .03)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Count Pop Bonus', 'POP', 'BOTH', true, .05)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Marquis Pop Bonus', 'POP', 'BOTH', true, .07)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Duke Pop Bonus', 'POP', 'BOTH', true, .09)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Prince Pop Bonus', 'POP', 'BOTH', true, .11)"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Lord' AND bonus.name = 'Lord Pop Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Baron' AND bonus.name = 'Baron Pop Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Viscount' AND bonus.name = 'Viscount Pop Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Count' AND bonus.name = 'Count Pop Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Marquis' AND bonus.name = 'Marquis Pop Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Duke' AND bonus.name = 'Duke Pop Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Prince' AND bonus.name = 'Prince Pop Bonus'")
        );
    }
}