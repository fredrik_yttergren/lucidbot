package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV54ToV55 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 55;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 2, elite_networth = 8.5 WHERE name = 'Avian'"),
                //Bocan
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 8, elite_networth = 9.5 WHERE name = 'Bocan'"),
                //Dark Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 8, def_spec_strength = 6, elite_off_strength = 4, elite_def_strength = 8, elite_networth = 10.5 WHERE name = 'Dark Elf'"),
                //Dryad
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 10, elite_def_strength = 1, elite_networth = 9, war_horse_strength = 3 WHERE name = 'Dryad'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 9 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 8, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 10 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 11 WHERE name = 'Faery'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9.5 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.5 WHERE name = 'Halfling TPA'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 10.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 WHERE name = 'Human Income'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 9 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.15 WHERE name = 'Orc Gains'"),
                //Heretic
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.4 WHERE name = 'Heretic Channeling Sci'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.4 WHERE name = 'Heretic Crime Sci'"),
                //Mystic
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 1 WHERE name = 'Mystic Sci'"),
                //Rogue
                new SimpleUpdateAction("DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                        "personality.name = 'Rogue' AND bonus.type = 'BUILDING_EFFECT' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Double TDs'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('TD Effect', 'BUILDING_EFFECT', 'BOTH', true, .5)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Rogue' AND bonus.name = 'TD Effect'"),
                //Sage
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .35 WHERE name = 'Sage Sci'"),
                //Warrior
                new SimpleUpdateAction("DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                        "personality.name = 'Warrior' AND bonus.type = 'OME_IN_WAR' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .2 WHERE name = 'Warrior Sci'"),
                //Bugfix
                new SimpleUpdateAction("ALTER TABLE kingdom MODIFY COLUMN kingdom_ritual_strength INT DEFAULT 0")
        );
    }
}
