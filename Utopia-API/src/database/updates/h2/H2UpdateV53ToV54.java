package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV53ToV54 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 54;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                                "personality.name = 'Heretic' AND bonus.type = 'WPA' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Heretic WPA'"),
                new SimpleUpdateAction("ALTER TABLE province DROP COLUMN kingdom_ritual_id"),
                new SimpleUpdateAction("ALTER TABLE kingdom DROP COLUMN kingdomRitualStrength"),
                new SimpleUpdateAction("ALTER TABLE kingdom ADD COLUMN kingdom_ritual_ticks INT")
        );
    }
}
