package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV48ToV49 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 49;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_def_strength = 6, elite_networth = 7, elite_sendout_percentage = 5 WHERE name = 'Bocan'")
        );
    }
}
