package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV19ToV20 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 20;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Human OME In War', 'OME_IN_WAR', 'OFFENSIVELY', true, .15)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Human' AND bonus.name = 'Human OME In War'"),
                new SimpleUpdateAction("ALTER TABLE sot ADD COLUMN war BIT"),
                new SimpleUpdateAction("UPDATE sot SET war = FALSE"),
                new SimpleUpdateAction("ALTER TABLE sot MODIFY COLUMN war BIT NOT NULL")
        );
    }
}