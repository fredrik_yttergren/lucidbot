package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV40ToV41 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 41;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE attack ADD COLUMN conversions INT"),
                new SimpleUpdateAction("UPDATE attack SET conversions = 0"),
                new SimpleUpdateAction("ALTER TABLE attack MODIFY COLUMN conversions INT NOT NULL")
                );
    }
}
