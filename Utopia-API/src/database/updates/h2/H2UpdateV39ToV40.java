package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV39ToV40 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 40;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 11, elite_def_strength = 5, elite_networth = 11 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 13, elite_def_strength = 2, elite_networth = 11.5 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 9, elite_off_strength = 15, elite_def_strength = 1, elite_networth = 11.5 WHERE name = 'Undead'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 11, elite_def_strength = 4, elite_networth = 10.5 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 8, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 2, off_spec_strength = 8, elite_off_strength = 6, elite_def_strength = 8, elite_networth = 9 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 10, elite_networth = 11 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 7, elite_networth = 9.5 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .35 WHERE name = 'Elf WPA'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .15 WHERE name = 'Halfling Pop'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .2 WHERE name = 'Heretic WPA'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .5 WHERE name = 'Double TDs'"),
                new SimpleUpdateAction("UPDATE science_type SET name = 'Production', result_factor = .03 WHERE id = 4"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .5 WHERE id = 1"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .25 WHERE id = 2"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .15 WHERE id = 3"),
                new SimpleUpdateAction("UPDATE science_type SET name = 'Production', result_factor = .3 WHERE id = 4"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .15 WHERE id = 5"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .125 WHERE id = 6"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .15 WHERE id = 7"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Faery Pop', 'POP', 'BOTH', false, .05)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery Pop'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Orc TPA', 'TPA', 'BOTH', false, .10)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Orc' AND bonus.name = 'Orc TPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Rogue' AND bonus.name = 'Double TDs'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Avian Gain', 'GAIN', 'BOTH', false, .10)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Avian' AND bonus.name = 'Avian Gain'"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'War Hero' AND bonus.name = 'War Hero Honor'"),
                new SimpleUpdateAction(
                        "DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE "
                                +
                                "race.name = 'Halfling' AND bonus.type = 'BUILDING_EFFECT' AND race_bonus.race_id = race.id)")
                );
    }
}
