package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV45ToV46 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 46;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Lord Income Bonus', 'INCOME', 'BOTH', true, .02)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Baron Income Bonus', 'INCOME', 'BOTH', true, .04)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Viscount Income Bonus', 'INCOME', 'BOTH', true, .06)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Count Income Bonus', 'INCOME', 'BOTH', true, .1)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Marquis Income Bonus', 'INCOME', 'BOTH', true, .14)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Duke Income Bonus', 'INCOME', 'BOTH', true, .18)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Prince Income Bonus', 'INCOME', 'BOTH', true, .22)"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Lord' AND bonus.name = 'Lord Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Baron' AND bonus.name = 'Baron Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Viscount' AND bonus.name = 'Viscount Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Count' AND bonus.name = 'Count Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Marquis' AND bonus.name = 'Marquis Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Duke' AND bonus.name = 'Duke Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title INNER JOIN bonus WHERE honor_title.name = 'Prince' AND bonus.name = 'Prince Income Bonus'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Human Income', 'INCOME', 'BOTH', true, .3)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Human' AND bonus.name = 'Human Income'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Faery Income', 'INCOME', 'BOTH', false, .1)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery Income'"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '0.02*#amount#' WHERE result_text = 'Produces ? wizards'"),
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'Our realm is now under a sphere of protection for (?<result>[\\\\d,]+)' WHERE name = 'Minor Protection'")
        );
    }
}
