package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV31ToV32 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 32;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'A magic vortex overcomes the province of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\))' WHERE name = 'Mystic Vortex'"),
                new SimpleUpdateAction("UPDATE spell_type SET spell_character = 'INSTANT_SPELLOP_WITH_PROVINCE' WHERE name = 'Mystic Vortex'"),
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'Our mages have illuminated the lands of our enemies and exposed the thieves that walk through their lands.' WHERE name = 'Expose Thieves'"),
                new SimpleUpdateAction("UPDATE spell_type SET spell_character = 'INSTANT_SPELLOP_WITHOUT_PROVINCE' WHERE name = 'Expose Thieves'"),
                new SimpleUpdateAction("ALTER TABLE instant_op ADD COLUMN last_updated DATETIME"),
                new SimpleUpdateAction("UPDATE instant_op SET last_updated = '2016-06-12 00:00:00'"),
                new SimpleUpdateAction("ALTER TABLE instant_op MODIFY COLUMN last_updated DATETIME NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE instant_spell ADD COLUMN last_updated DATETIME"),
                new SimpleUpdateAction("UPDATE instant_spell SET last_updated = '2016-06-12 00:00:00'"),
                new SimpleUpdateAction("ALTER TABLE instant_spell MODIFY COLUMN last_updated DATETIME NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE duration_op ADD COLUMN last_updated DATETIME"),
                new SimpleUpdateAction("UPDATE duration_op SET last_updated = '2016-06-12 00:00:00'"),
                new SimpleUpdateAction("ALTER TABLE duration_op MODIFY COLUMN last_updated DATETIME NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE duration_spell ADD COLUMN last_updated DATETIME"),
                new SimpleUpdateAction("UPDATE duration_spell SET last_updated = '2016-06-12 00:00:00'"),
                new SimpleUpdateAction("ALTER TABLE duration_spell MODIFY COLUMN last_updated DATETIME NOT NULL")
        );
    }
}
