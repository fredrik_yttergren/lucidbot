package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV30ToV31 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 31;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("CREATE TABLE records (id BIGINT, record_name VARCHAR(100), user_name VARCHAR(100), " +
                        "record_value INT, all_time_record BOOLEAN, PRIMARY KEY (id))"),
                new SimpleUpdateAction("ALTER TABLE records MODIFY COLUMN id BIGINT NOT NULL AUTO_INCREMENT"),
                new SimpleUpdateAction("ALTER TABLE records MODIFY COLUMN record_name VARCHAR(100) NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE records MODIFY COLUMN user_name VARCHAR(100) NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE records MODIFY COLUMN record_value INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE records MODIFY COLUMN all_time_record BOOLEAN NOT NULL"),
                new SimpleUpdateAction("UPDATE spell_type SET short_name = null WHERE name = 'Gluttony'")
        );
    }
}
