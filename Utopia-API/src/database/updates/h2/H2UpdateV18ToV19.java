package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV18ToV19 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 19;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Double TDs', 'BUILDING_EFFECT', 'BOTH', true, 1.0)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Rogue' AND bonus.name = 'Double TDs'"),
                new SimpleUpdateAction(
                        "UPDATE science_type SET angel_name = 'Gains in Combat' WHERE name = 'Military'")
        );
    }
}