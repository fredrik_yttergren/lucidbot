package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV42ToV43 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 43;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 1, soldier_networth = .5"),
                new SimpleUpdateAction("ALTER TABLE kingdom ADD COLUMN kingdom_ritual VARCHAR(100)"),
                new SimpleUpdateAction("ALTER TABLE race ADD COLUMN war_horse_strength INT NOT NULL DEFAULT 2"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 8, elite_def_strength = 9 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 9, elite_def_strength = 7, elite_networth = 10 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .1 WHERE name = 'Halfling Pop'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .4 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 7 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 8, elite_off_strength = 11, elite_def_strength = 3 WHERE name = 'Undead'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 12, elite_def_strength = 1 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .5 WHERE name = 'Double TDs'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Warrior OME In War', 'OME_IN_WAR', 'OFFENSIVELY', true, .1)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Warrior' AND bonus.name = 'Warrior OME In War'"),
                new SimpleUpdateAction("MERGE INTO personality (alias, cons, dragon_immune, intel_accuracy, name, pros) KEY(alias, name) VALUES('Chivalrous', '', false, 'NEVER', 'Paladin', '')"),
                new SimpleUpdateAction(
                        "MERGE INTO race (cons, def_spec_name, def_spec_strength, dragon_immune, elite_def_strength, elite_name, elite_networth, elite_off_strength, elite_sendout_percentage, intel_accuracy, name," +
                                "off_spec_name, off_spec_strength, pros, short_name, soldier_networth, soldier_strength, war_horse_strength) KEY(name, short_name) VALUES(null, 'Nymphs', 6, 0, 2, 'Will O'' The Wisps', 12.5, 14, 100, 'NEVER', 'Dryad'," +
                                "'Huldras', 6, null, 'DR', .5, 1, 4)"),
                new SimpleUpdateAction(
                "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('Your magic will smite attackers of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) for (?<result>[\\d,]+) days', 'Increases casualties of attackers', 'Wrathful Smite', '', 'ws', 'FADING_SPELLOP_WITH_PROVINCE')"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('You have imbued (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) scientists with mental energy, making them work harder for the next (?<result>[\\d,]+) days', 'Increases science effectiveness by 10%', 'Scientific Insights', '', 'si', 'FADING_SPELLOP_WITH_PROVINCE')"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('You fill (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) with holy light, reducing shadows for thieves to hide in for (?<result>[\\d,]+) days', 'Reduces thievery damage by 20%', 'Illuminate Shadows', '', 'is', 'FADING_SPELLOP_WITH_PROVINCE')"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('You imbue (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)), protecting them from witchcraft for (?<result>[\\d,]+) days', 'Increases science effectiveness by 10%', 'Divine Shield', '', 'ds', 'FADING_SPELLOP_WITH_PROVINCE')"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('Holy magic interferes with the natural casting ability of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) for (?<result>[\\d,]+) days', 'Increases target rune cost by 25%', 'Magic Ward', '', 'mw', 'FADING_SPELLOP_WITH_PROVINCE')"),
                new SimpleUpdateAction(
                        "MERGE INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) KEY(name, short_name) VALUES('Your mages have ruined (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) faith in the military for (?<result>[\\d,]+) days', 'Stops draft on the target province', 'Barrier of Integrity', '', 'bi', 'FADING_SPELLOP_WITH_PROVINCE')")
        );
    }
}
