package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV57ToV58 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 58;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 9.0 WHERE name = 'Avian'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 4, elite_networth = 9.0 WHERE name = 'Avian'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 9.5 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9.5 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.07 WHERE name = 'Halfling Pop'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 9.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.1 WHERE name = 'Human WPA'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 9 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.20 WHERE name = 'Orc Gains'"),

                //Add new science categories
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Bookkeeping', '', 'Bookkeeping', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Artisan', '', 'Artisan', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Strategy', '', 'Strategy', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Seige', '', 'Seige', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Tactics', '', 'Tactics', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Valor', '', 'Valor', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Heroism', '', 'Heroism', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Shielding', '', 'Shielding', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Cunning', '', 'Cunning', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Invocation', '', 'Invocation', 0.5)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Economy ', '', 'Economy', 0)"),
                new SimpleUpdateAction("INSERT INTO science_type (angel_name, description, name, result_factor) VALUES('Arcane Arts ', '', 'Arcane Arts', 0)"),

                //Bugfix
                new SimpleUpdateAction("ALTER TABLE kingdom MODIFY COLUMN kingdom_ritual_strength INT DEFAULT 0 NULL"),

                //Labs fix
                new SimpleUpdateAction("UPDATE building_formula SET cap = '95' WHERE result_text = 'Increases science effects by ?%'")
                );
    }
}
