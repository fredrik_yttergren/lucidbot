package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV50ToV51 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 51;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_name = 'Will O The Wisps' WHERE name = 'Dryad'"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Havoc')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Resilient')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Godspeed')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Expedient')")
        );
    }
}
