package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV34ToV35 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 35;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Halfing Pop'"),
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 3, soldier_networth = 3 WHERE name = 'Halfling'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Elf WPA', 'WPA', 'BOTH', true, .4)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Heretic WPA', 'WPA', 'BOTH', true, .25)"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .3 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Elf' AND bonus.name = 'Elf WPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling TPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 1 WHERE name = 'Alchemy'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .5 WHERE name = 'Tools'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .3 WHERE name = 'Housing'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 3 WHERE name = 'Food'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .3 WHERE name = 'Military'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 3 WHERE name = 'Crime'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 3 WHERE name = 'Channeling'")
        );
    }
}
