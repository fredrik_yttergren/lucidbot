package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV60ToV61 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 61;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE province MODIFY COLUMN mana INT NOT NULL DEFAULT 100"),
                new SimpleUpdateAction("ALTER TABLE province MODIFY COLUMN stealth INT NOT NULL DEFAULT 100")
        );
    }
}
