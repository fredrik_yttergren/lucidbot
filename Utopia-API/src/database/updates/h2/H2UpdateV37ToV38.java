package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV37ToV38 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 38;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 5 WHERE name = 'Food'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 2.5 WHERE name = 'Crime'")
        );
    }
}
