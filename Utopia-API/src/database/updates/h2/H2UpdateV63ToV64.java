package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV63ToV64 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 64;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 8.5 WHERE name = 'Avian'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 10.5 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 3, elite_networth = 8.5 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.25 WHERE name = 'Faery WPA'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 6, elite_networth = 8 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.25 WHERE name = 'Halfling TPA'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 7, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9.5 WHERE name = 'Human'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 3, elite_networth = 9 WHERE name = 'Orc'"),
                //Undead
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5, def_spec_strength = 5, elite_off_strength = 10, elite_def_strength = 2, elite_networth = 10 WHERE name = 'Undead'")
        );
    }
}
