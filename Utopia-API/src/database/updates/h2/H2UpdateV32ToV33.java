package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;


public class H2UpdateV32ToV33 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 33;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 7, off_spec_strength = 7, elite_off_strength = 8, elite_def_strength = 6, elite_networth = 8 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET elite_sendout_percentage = 5, elite_off_strength = 6, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE race SET elite_sendout_percentage = 100, soldier_strength = 2, off_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 4, elite_networth = 8 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 9 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET soldier_networth = 2"),
                new SimpleUpdateAction("DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality INNER JOIN bonus ON personality_bonus.personality_id = bonus.id WHERE " +
                        "personality.name = 'Rogue' AND bonus.name = 'Double TDs' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Halfling' AND bonus.name = 'Double TDs'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Halfling' AND bonus.type = 'TPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Halfling' AND bonus.type = 'POP' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("UPDATE building SET name = 'Laboratories', short_name = 'labs', syntax = 'percent be' WHERE name = 'Libraries'"),
                new SimpleUpdateAction("UPDATE building SET name = 'Universities', short_name = 'unis', syntax = 'percent be' WHERE name = 'Schools'")
        );
    }
}
