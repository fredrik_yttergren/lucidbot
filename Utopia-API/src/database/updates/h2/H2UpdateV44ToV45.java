package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV44ToV45 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 45;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE kingdom ADD COLUMN kingdom_ritual_strength INT NOT NULL DEFAULT 0"),
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN last_seen DATETIME"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .5 WHERE name = 'Alchemy'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .33 WHERE name = 'Tools'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .2 WHERE name = 'Housing'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 2 WHERE name = 'Production'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .2 WHERE name = 'Military'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 1.65 WHERE name = 'Crime'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 2 WHERE name = 'Channeling'"),
                new SimpleUpdateAction("UPDATE spell_type SET name = 'Meteor Showers' WHERE name = 'Meteor Shower'"),
                new SimpleUpdateAction("UPDATE spell_type SET name = 'Love and Peace' WHERE name = 'Love & Peace'"),
                new SimpleUpdateAction("UPDATE spell_type SET name = 'Builders Boon' WHERE name = 'Builder''s Boon'"),
                new SimpleUpdateAction("UPDATE spell_type SET name = 'Fools Gold' WHERE name = 'Fool''s Gold'"),
                new SimpleUpdateAction("UPDATE spell_type SET name = 'Scientific Insight' WHERE name = 'Scientific Insights'"),
                new SimpleUpdateAction("UPDATE records SET record_name = 'Meteor Showers' WHERE record_name = 'Meteor Shower'"),
                new SimpleUpdateAction("UPDATE records SET record_name = 'Love and Peace' WHERE record_name = 'Love & Peace'"),
                new SimpleUpdateAction("UPDATE records SET record_name = 'Builders Boon' WHERE record_name = 'Builder''s Boon'"),
                new SimpleUpdateAction("UPDATE records SET record_name = 'Fools Gold' WHERE record_name = 'Fool''s Gold'"),
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'You have made the scientists of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) forget (?<result>[\\d,]+) days of learning' where name = 'Amnesia'"),
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'A magic vortex overcomes the province of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)), negating (?<result>\\d+) active spells? \\((?<spells>.+)\\)\\.' where name = 'Mystic Vortex'")
        );
    }
}
