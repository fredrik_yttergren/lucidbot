package database.updates.h2;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;

public class H2UpdateV14ToV15 extends ApiH2DatabaseUpdater {

  @Override
  public int updatesToVersion() {
    return 15;
  }

  @Override
  public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
    return Lists.newArrayList(
        new SimpleUpdateAction(
            "DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE "
            +
            "race.name = 'Avian' AND bonus.type = 'GAIN' AND race_bonus.race_id = race.id)"),
        new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Avian Gains'"),
        new SimpleUpdateAction(
            "DELETE FROM sciencetype_bonus WHERE EXISTS(SELECT * FROM science_type INNER JOIN bonus ON sciencetype_bonus.bonus_id = bonus.id WHERE "
            +
            "science_type.name = 'Military' AND bonus.type = 'GAIN' AND sciencetype_bonus.sciencetype_id = science_type.id)"),
        new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Gains Sci'"),
        new SimpleUpdateAction(
            "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES(" +
            "'Halfling Gains', 'GAIN', 'OFFENSIVELY', false, 0.1)"),
        new SimpleUpdateAction(
            "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling Gains'"),
        new SimpleUpdateAction(
            "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES(" +
            "'War Hero Honor', 'HONOR', 'BOTH', true, 1)"),
        new SimpleUpdateAction(
            "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality INNER JOIN bonus WHERE personality.name = 'War Hero' AND bonus.name = 'War Hero Honor'"),
        new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.2 WHERE name = 'Orc Gains'"),
        new SimpleUpdateAction(
            "UPDATE race SET elite_off_strength = 5, elite_def_strength = 5, off_spec_strength = 4, elite_networth = 6.5, soldier_strength = 1, soldier_networth = 1.5 WHERE name = 'Halfling'"),
        new SimpleUpdateAction("UPDATE race SET elite_networth = 7, elite_sendout_percentage = 70 WHERE name = 'Human'"),
        new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5, elite_networth = 6.5 WHERE name = 'Undead'"),
        new SimpleUpdateAction(
            "UPDATE science_type SET angel_name = 'Military Efficiency', result_factor = 0.65 WHERE name = 'Military'")
    );
  }
}
