package database.updates.h2;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;


public class H2UpdateV55ToV56 extends ApiH2DatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 56;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Create ability for personas to have off/def spec strength boosts
                new SimpleUpdateAction("ALTER TABLE personality ADD COLUMN off_spec_strength INTEGER DEFAULT 0 NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE personality ADD COLUMN def_spec_strength INTEGER DEFAULT 0 NOT NULL"),

                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 2, elite_networth = 8.5 WHERE name = 'Avian'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 9.5 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 7, elite_def_strength = 3, elite_networth = 9 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Faery' AND bonus.type = 'Pop' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Faery Pop'"),

                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.05 WHERE name = 'Halfling Pop'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 9.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.15 WHERE name = 'Human WPA'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 9 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.20 WHERE name = 'Orc Gains'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Orc' AND bonus.type = 'TPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Orc TPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Orc TPA', 'TPA', 'BOTH', false, .15)"),
                new SimpleUpdateAction(
                        "MERGE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race INNER JOIN bonus WHERE race.name = 'Orc' AND bonus.name = 'Orc TPA'"),
                //Heretic
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Heretic TPA', 'TPA', 'BOTH', true, .3)"),
                new SimpleUpdateAction(
                        "MERGE INTO bonus (name, type, applicability, is_increasing, bonus_value) KEY(name) VALUES('Heretic WPA', 'WPA', 'BOTH', true, .3)"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic TPA'"),
                new SimpleUpdateAction(
                        "MERGE INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'"),
                //Mystic
                //Rogue
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .75 WHERE name = 'TD Effect'"),
                //Sage
                //Warrior
                //War Hero
                new SimpleUpdateAction("UPDATE personality SET off_spec_strength = 1 WHERE name = 'War Hero'")
        );
    }
}
