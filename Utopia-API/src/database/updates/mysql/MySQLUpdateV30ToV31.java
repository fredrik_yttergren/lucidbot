package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV30ToV31 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 31;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("CREATE TABLE records (id BIGINT NOT NULL AUTO_INCREMENT, record_name VARCHAR(100) NOT NULL, " +
                        "user_name VARCHAR(100) NOT NULL, record_value INT NOT NULL, all_time_record BOOLEAN NOT NULL, PRIMARY KEY (id))"),
                new SimpleUpdateAction("UPDATE spell_type SET short_name = null WHERE name = 'Gluttony'")
        );
    }
}