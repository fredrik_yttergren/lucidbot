package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV51ToV52 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 52;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE building_formula SET result_text = 'Decreased enemy thief damage: ?%' WHERE result_text = 'Decreased enemy thief damage: ?$'"),
                new SimpleUpdateAction("ALTER TABLE som ADD COLUMN offensive_me DOUBLE"),
                new SimpleUpdateAction("ALTER TABLE som ADD COLUMN defensive_me DOUBLE"),
                new SimpleUpdateAction("UPDATE op_type SET name = 'Incite Riots' WHERE name = 'Riots'"),
                new SimpleUpdateAction("UPDATE op_type SET name = 'Rob the Granaries' WHERE name = 'Rob Granaries'"),
                new SimpleUpdateAction("UPDATE op_type SET name = 'Rob the Towers' WHERE name = 'Rob Towers'"),
                new SimpleUpdateAction("UPDATE op_type SET name = 'Rob the Vaults' WHERE name = 'Rob Vaults'"),
                new SimpleUpdateAction("UPDATE op_type SET name = 'Steal War Horses' WHERE name = 'Steal Horses'"),
                new SimpleUpdateAction("UPDATE spell_type SET name = 'Drought' WHERE name = 'Droughts'"),
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'A magic vortex overcomes the province of (?<target>[^(]+\\\\(\\\\d{1,2}:\\\\d{1,2}\\\\)), (negating (?<result>\\\\d+) active spells? \\\\((?<spells>.+)\\\\))?' where name = 'Mystic Vortex'")
                );
    }
}