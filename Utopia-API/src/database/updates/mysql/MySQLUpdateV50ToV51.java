package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV50ToV51 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 51;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_name = 'Will O The Wisps' WHERE name = 'Dryad'"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Havoc')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Resilient')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Godspeed')"),
                new SimpleUpdateAction("INSERT INTO kingdom_ritual (name) VALUES('Expedient')")
                );
    }
}