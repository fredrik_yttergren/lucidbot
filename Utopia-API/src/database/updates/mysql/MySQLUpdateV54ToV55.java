package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV54ToV55 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 55;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 2, elite_networth = 8.5 WHERE name = 'Avian'"),
                //Bocan
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 8, elite_networth = 9.5 WHERE name = 'Bocan'"),
                //Dark Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 8, def_spec_strength = 6, elite_off_strength = 4, elite_def_strength = 8, elite_networth = 10.5 WHERE name = 'Dark Elf'"),
                //Dryad
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 10, elite_def_strength = 1, elite_networth = 9, war_horse_strength = 3 WHERE name = 'Dryad'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 9 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 8, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 10 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 11 WHERE name = 'Faery'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9.5 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.5 WHERE name = 'Halfling TPA'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 10.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.30 WHERE name = 'Human Income'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 9 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.15 WHERE name = 'Orc Gains'"),
                //Heretic
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.4 WHERE name = 'Heretic Channeling Sci'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.4 WHERE name = 'Heretic Crime Sci'"),
                //Mystic
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 1 WHERE name = 'Mystic Sci'"),
                //Rogue
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Rogue' AND (bonus.type = 'BUILDING_EFFECT')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Double TDs'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('TD Effect', 'BUILDING_EFFECT', 'BOTH', true, .5)"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Rogue' AND bonus.name = 'TD Effect'"),
                //Sage
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .35 WHERE name = 'Sage Sci'"),
                //Warrior
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Warrior' AND (bonus.type = 'OME_IN_WAR')"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .2 WHERE name = 'Warrior Sci'"),
                //Bugfix
                new SimpleUpdateAction("ALTER TABLE kingdom MODIFY COLUMN kingdom_ritual_strength INT DEFAULT 0")

                );
    }
}