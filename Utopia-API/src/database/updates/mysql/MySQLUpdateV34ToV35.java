package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV34ToV35 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 35;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Halfing Pop'"),
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 3, soldier_networth = 3 WHERE name = 'Halfling'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic WPA', 'WPA', 'BOTH', true, .25) ON DUPLICATE KEY UPDATE bonus_value = .25"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Elf WPA', 'WPA', 'BOTH', true, .4) ON DUPLICATE KEY UPDATE bonus_value = .4"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .3 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Elf' AND bonus.name = 'Elf WPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling TPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 1 WHERE name = 'Alchemy'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .5 WHERE name = 'Tools'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .3 WHERE name = 'Housing'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 3 WHERE name = 'Food'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = .3 WHERE name = 'Military'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 3 WHERE name = 'Crime'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 3 WHERE name = 'Channeling'")

        );
    }
}