package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV62ToV63 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 63;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE personality SET off_spec_strength = 0, def_spec_strength = 0 WHERE name = 'War Hero'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Faery WPA', 'WPA', 'BOTH', true, .2)"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery WPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic WPA', 'WPA', 'BOTH', true, .25)"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic TPA', 'TPA', 'BOTH', true, .25)"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic TPA'")
                );
    }
}