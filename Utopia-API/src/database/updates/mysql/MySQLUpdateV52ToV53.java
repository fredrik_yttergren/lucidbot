package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV52ToV53 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 53;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 9 WHERE name = 'Avian'"),
                //Bocan
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 4, elite_def_strength = 7, elite_networth = 7 WHERE name = 'Bocan'"),
                //Dark Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 8, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9 WHERE name = 'Dark Elf'"),
                //Dryad
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 12, elite_def_strength = 2, elite_networth = 11 WHERE name = 'Dryad'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 7, elite_def_strength = 6, elite_networth = 8.5 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 4, elite_def_strength = 9, elite_networth = 10.5 WHERE name = 'Faery'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 4, elite_networth = 10 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.1 WHERE name = 'Human WPA'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 10, elite_def_strength = 1, elite_networth = 9.5 WHERE name = 'Orc'")

                );
    }
}