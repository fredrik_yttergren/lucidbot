package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV48ToV49 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 49;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'Our realm is now under a sphere of protection for (?<result>[\\\\d,]+)' WHERE name = 'Minor Protection'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_def_strength = 6, elite_networth = 7, elite_sendout_percentage = 5 WHERE name = 'Bocan'"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '2*#percent#*#be#/100*(1-#percent#/100)' WHERE result_text = 'Increases DME by ?%'"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '2.5*#percent#*#be#/100*(1-#percent#/100)' WHERE result_text = 'Protects against learns by ?%'"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '4*#percent#*#be#/100*(1-#percent#/100)' WHERE result_text = 'Increases new scientists by ?%'"),
                new SimpleUpdateAction("UPDATE personality SET alias = 'Chivalrous' WHERE name = 'Paladin'"),
                new SimpleUpdateAction("INSERT INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) VALUES('Your mages infest the guilds of (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)). Their ritual lifespan has been shortened!', 'Destroys 2% of a ritual', 'Abolish Ritual', '', 'ar', 'INSTANT_SPELLOP_WITH_PROVINCE')")
        );
    }
}