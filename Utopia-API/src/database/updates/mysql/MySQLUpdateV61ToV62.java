package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV61ToV62 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 62;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE province ADD estimated_thieves INT NOT NULL DEFAULT 0"),
                new SimpleUpdateAction("ALTER TABLE province ADD estimated_wizards INT NOT NULL DEFAULT 0"),
                new SimpleUpdateAction("ALTER TABLE province ADD estimated_books INT NOT NULL DEFAULT 0"),
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 7, elite_def_strength = 3, elite_networth = 8.0 WHERE name = 'Avian'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Avian' AND bonus.type = 'GAIN' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Avian Gain'"),

                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 6, elite_def_strength = 6, elite_networth = 8.5 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 4, elite_networth = 9.5 WHERE name = 'Elf'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Elf' AND bonus.type = 'WPA' AND race_bonus.race_id = race.id)"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 4, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Faery Pop', 'POP', 'BOTH', false, .05)"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery Pop'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 5, elite_networth = 6.5 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.5 WHERE name = 'Halfling TPA'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 7, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 10 WHERE name = 'Human'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 8.75 WHERE name = 'Orc'"),
                //Undead
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5, def_spec_strength = 5, elite_off_strength = 1, elite_def_strength = 3, elite_networth = 10.5 WHERE name = 'Undead'"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                "race.name = 'Orc' AND bonus.type = 'GAIN' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM race_bonus WHERE EXISTS(SELECT * FROM race,bonus WHERE race_bonus.bonus_id = bonus.id AND " +
                        "race.name = 'Orc' AND bonus.type = 'TPA' AND race_bonus.race_id = race.id)"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Orc Gains'"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Orc TPA'")
                );
    }
}