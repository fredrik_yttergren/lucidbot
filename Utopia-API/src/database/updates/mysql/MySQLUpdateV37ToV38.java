package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV37ToV38 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 38;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 5 WHERE name = 'Food'"),
                new SimpleUpdateAction("UPDATE science_type SET result_factor = 2.5 WHERE name = 'Crime'")
        );
    }
}