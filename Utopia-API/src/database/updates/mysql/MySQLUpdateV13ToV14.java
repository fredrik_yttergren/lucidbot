package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV13ToV14 extends ApiMySQLDatabaseUpdater {
    @Override
    public int updatesToVersion() {
      return 14;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE forum_post MODIFY bot_user_id bigint(20) default null")
        );
    }
}
