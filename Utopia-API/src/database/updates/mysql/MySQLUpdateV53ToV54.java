package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV53ToV54 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 54;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Heretic' AND (bonus.type = 'WPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Heretic WPA'"),
                new SimpleUpdateAction("ALTER TABLE province DROP COLUMN kingdom_ritual_id"),
                new SimpleUpdateAction("ALTER TABLE kingdom DROP COLUMN kingdomRitualStrength"),
                new SimpleUpdateAction("ALTER TABLE kingdom ADD COLUMN kingdom_ritual_ticks INT")
        );
    }
}