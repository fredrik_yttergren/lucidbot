package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV56ToV57 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 57;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Fix Heretic
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Heretic' AND (bonus.type = 'WPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Heretic WPA'"),
                //Remove Sci Bonuses, not needed anymore since we are given the number of days of training per category
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Heretic' AND (bonus.type = 'TPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Heretic TPA'"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Cleric' AND (bonus.type = 'HOUSING_SCI_EFF')"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Mystic' AND (bonus.type = 'CHANNELING_SCI_EFF')"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Sage' AND (bonus.type = 'SCI_EFF')"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Rogue' AND (bonus.type = 'CRIME_SCI_EFF')"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Warrior' AND (bonus.type = 'MILITARY_SCI_EFF')"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Heretic' AND (bonus.type = 'CHANNELING_SCI_EFF')"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Heretic' AND (bonus.type = 'CRIME_SCI_EFF')"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Undead' AND (bonus.type = 'SCI_EFF')")
                //new SimpleUpdateAction("DELETE FROM bonus WHERE name like '%Sci'")

                );
    }
}