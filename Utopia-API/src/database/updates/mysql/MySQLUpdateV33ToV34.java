package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV33ToV34 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 34;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) VALUES('Blizzards will beset the works of (?<target>[^(]+\\\\(\\\\d{1,2}:\\\\d{1,2}\\\\)) for for (?<result>[\\\\d,]+) days.', 'Reduces the building effectiveness of a province by 10%', 'Blizzard', '', 'bliz', 'FADING_SPELLOP_WITH_PROVINCE')")
                );
    }
}