package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV26ToV27 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 27;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_networth = 6 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.2 where name = 'Orc Gains'"),
                new SimpleUpdateAction("DELETE personality_bonus FROM personality_bonus INNER JOIN personality ON personality_bonus.personality_id = personality.id INNER JOIN bonus ON personality_bonus.bonus_id = bonus.id WHERE " +
                        "personality.name = 'Warrior' AND (bonus.type = 'OME_IN_WAR')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Warrior OME In War'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Human OME In War', 'OME_IN_WAR', 'OFFENSIVELY', true, .2) ON DUPLICATE KEY UPDATE bonus_value = .2"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Human' AND bonus.name = 'Human OME In War'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Human Gains', 'GAIN', 'OFFENSIVELY', false, .1) ON DUPLICATE KEY UPDATE bonus_value = .1"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Human' AND bonus.name = 'Human Gains'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Halfling Pop', 'POP', 'BOTH', true, .1) ON DUPLICATE KEY UPDATE bonus_value = .1"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling Pop'")
        );
    }
}