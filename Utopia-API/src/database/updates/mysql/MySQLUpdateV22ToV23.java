package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV22ToV23 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 23;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.25 WHERE name = 'Faery'")
        );
    }
}