package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV45ToV46 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 46;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Lord Income Bonus', 'INCOME', 'BOTH', true, .02) ON DUPLICATE KEY UPDATE bonus_value = .02"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Baron Income Bonus', 'INCOME', 'BOTH', true, .04) ON DUPLICATE KEY UPDATE bonus_value = .04"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Viscount Income Bonus', 'INCOME', 'BOTH', true, .06) ON DUPLICATE KEY UPDATE bonus_value = .06"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Count Income Bonus', 'INCOME', 'BOTH', true, .1) ON DUPLICATE KEY UPDATE bonus_value = .1"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Marquis Income Bonus', 'INCOME', 'BOTH', true, .14) ON DUPLICATE KEY UPDATE bonus_value = .14"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Duke Income Bonus', 'INCOME', 'BOTH', true, .18) ON DUPLICATE KEY UPDATE bonus_value = .18"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Prince Income Bonus', 'INCOME', 'BOTH', true, .22) ON DUPLICATE KEY UPDATE bonus_value = .22"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Lord' AND bonus.name = 'Lord Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Baron' AND bonus.name = 'Baron Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Viscount' AND bonus.name = 'Viscount Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Count' AND bonus.name = 'Count Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Marquis' AND bonus.name = 'Marquis Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Duke' AND bonus.name = 'Duke Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO honortitle_bonus (title_id, bonus_id) SELECT honor_title.id,bonus.id FROM honor_title,bonus WHERE honor_title.name = 'Prince' AND bonus.name = 'Prince Income Bonus'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Human Income', 'INCOME', 'BOTH', true, .3) ON DUPLICATE KEY UPDATE bonus_value = .3"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Human' AND bonus.name = 'Human Income'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Faery Income', 'INCOME', 'BOTH', false, .1) ON DUPLICATE KEY UPDATE bonus_value = .1"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Faery' AND bonus.name = 'Faery Income'"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '0.02*#amount#' WHERE result_text = 'Produces ? wizards'")
        );
    }
}