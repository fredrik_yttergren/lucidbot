package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV47ToV48 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 48;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Cleric Sci', 'HOUSING_SCI_EFF', 'BOTH', true, .15) ON DUPLICATE KEY UPDATE bonus_value = .15"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Cleric' AND bonus.name = 'Cleric Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic Channeling Sci', 'CHANNELING_SCI_EFF', 'BOTH', true, .10) ON DUPLICATE KEY UPDATE bonus_value = .10"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic Channeling Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic Crime Sci', 'CRIME_SCI_EFF', 'BOTH', true, .10) ON DUPLICATE KEY UPDATE bonus_value = .10"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic Crime Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Mystic Sci', 'CHANNELING_SCI_EFF', 'BOTH', true, .75) ON DUPLICATE KEY UPDATE bonus_value = .75"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Mystic' AND bonus.name = 'Mystic Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Rogue Sci', 'CRIME_SCI_EFF', 'BOTH', true, .5) ON DUPLICATE KEY UPDATE bonus_value = .5"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Rogue' AND bonus.name = 'Rogue Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Sage Sci', 'SCI_EFF', 'BOTH', true, .3) ON DUPLICATE KEY UPDATE bonus_value = .3"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Sage' AND bonus.name = 'Sage Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Warrior Sci', 'MILITARY_SCI_EFF', 'BOTH', true, .15) ON DUPLICATE KEY UPDATE bonus_value = .15"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Warrior' AND bonus.name = 'Warrior Sci'"),

                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Undead Sci', 'SCI_EFF', 'BOTH', false, .15) ON DUPLICATE KEY UPDATE bonus_value = .15"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Undead' AND bonus.name = 'Undead Sci'")
                );
    }
}