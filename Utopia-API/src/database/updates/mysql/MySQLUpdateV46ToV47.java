package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV46ToV47 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 47;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //HU
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .15 WHERE name = 'Human WPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 8, elite_def_strength = 3, elite_networth = 10 WHERE name = 'Human'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Human Wages', 'WAGES', 'BOTH', false, .25) ON DUPLICATE KEY UPDATE bonus_value = .25"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Human' AND bonus.name = 'Human Wages'"),
                //Avian
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 9 where name = 'Avian'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .05 WHERE name = 'Avian Gain'"),
                //DE
                new SimpleUpdateAction(
                        "INSERT INTO race (cons, def_spec_name, def_spec_strength, dragon_immune, elite_def_strength, elite_name, elite_networth, elite_off_strength, elite_sendout_percentage, intel_accuracy, name," +
                                "off_spec_name, off_spec_strength, pros, short_name, soldier_networth, soldier_strength, war_horse_strength) VALUES(null, 'Druids', 5, 0, 7, 'Drows', 10, 4, 10, 'NEVER', 'Dark Elf'," +
                                "'Night Rangers', 7, null, 'DE', .5, 1, 2)"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Dark Elf WPA', 'WPA', 'BOTH', true, .25) ON DUPLICATE KEY UPDATE bonus_value = .25"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Dark Elf' AND bonus.name = 'Dark Elf WPA'"),
                //DW
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 7, elite_def_strength = 4 WHERE name = 'Dwarf'"),
                //FAE
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Faery' AND (bonus.type = 'POP')"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 2, elite_def_strength = 8, elite_networth = 10.5 WHERE name = 'Faery'"),
                //ORC
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .3 WHERE name = 'Orc Gains'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .15 WHERE name = 'Orc TPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 10, elite_def_strength = 1, elite_networth = 9.5 WHERE name = 'Orc'"),
                //Undead
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 8, elite_off_strength = 11, elite_def_strength = 2, elite_networth = 10.5 WHERE name = 'Undead'"),
                //Bocan
                new SimpleUpdateAction(
                        "INSERT INTO race (cons, def_spec_name, def_spec_strength, dragon_immune, elite_def_strength, elite_name, elite_networth, elite_off_strength, elite_sendout_percentage, intel_accuracy, name," +
                                "off_spec_name, off_spec_strength, pros, short_name, soldier_networth, soldier_strength, war_horse_strength) VALUES(null, 'Imps', 6, 0, 4, 'Tricksters', 6, 4, 7, 'NEVER', 'Bocan'," +
                                "'Marauders', 6, null, 'BO', .5, 1, 2)"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Bocan Honor', 'HONOR', 'BOTH', true, .5) ON DUPLICATE KEY UPDATE bonus_value = .5"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Bocan' AND bonus.name = 'Bocan Honor'"),
                //Dryad
                new SimpleUpdateAction("UPDATE race SET war_horse_Strength = 3, elite_off_strength = 12, elite_def_strength = 2 where name = 'Dryad'"),
                //Random
                new SimpleUpdateAction(
                "DELETE FROM personality_bonus WHERE EXISTS(SELECT * FROM personality,bonus WHERE personality_bonus.bonus_id = bonus.id AND " +
                        "personality.name = 'Warrior' AND bonus.type = 'OME_IN_WAR' AND personality_bonus.personality_id = personality.id)"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '2.5*#percent#*#be#/100*(1-#percent#/100)' WHERE result_text = 'Protects against learns by ?%'"),
                new SimpleUpdateAction("UPDATE building_formula SET cap = '50' WHERE result_text = 'Protects against learns by ?%'")
                );
    }
}