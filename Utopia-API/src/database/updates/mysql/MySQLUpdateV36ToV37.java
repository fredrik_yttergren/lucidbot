package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV36ToV37 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 37;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO spell_type (cast_regex, effects, name, news_regex, short_name, spell_character) VALUES('Our people are increasingly drawn to the sciences for (?<result>[\\\\d,]+) days.', 'Increases scientist spawn rate by 100%', 'Revelation', '', 're', 'SELF_SPELLOP')")
        );
    }
}