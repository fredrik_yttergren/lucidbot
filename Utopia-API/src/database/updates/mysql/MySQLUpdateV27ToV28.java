package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV27ToV28 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 28;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("DROP TABLE IF EXISTS nub"),
                new SimpleUpdateAction("CREATE TABLE nub (id BIGINT NOT NULL AUTO_INCREMENT, nub_name VARCHAR(50), " +
                        "reason VARCHAR(5000), setter VARCHAR(50), set_time DATETIME, PRIMARY KEY (id))")
        );
    }
}