package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV59ToV60 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 60;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE spell_type SET cast_regex = 'Your mages infest the guilds of (?<target>[^(]+\\\\(\\\\d{1,2}:\\\\d{1,2}\\\\))\\\\. Their ritual is now (?<destroyed>\\\\d+)% destroyed! This province can (?<targetsRemaining>.*)!' where name = 'Abolish Ritual'"),
                new SimpleUpdateAction("ALTER TABLE province ADD abolish_rituals_left INT NOT NULL DEFAULT 10"),
                new SimpleUpdateAction("ALTER TABLE kingdom ADD kingdom_ritual_destroyed INT NULL"),
                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 1, elite_networth = 9.0 WHERE name = 'Avian'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 4, elite_networth = 9.0 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 8, elite_def_strength = 2, elite_networth = 9.5 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.25 WHERE name = 'Elf WPA'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9.5 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.25 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.05 WHERE name = 'Halfling Pop'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 9.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Human' AND (bonus.type = 'WPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Human WPA'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 1, elite_networth = 9 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.20 WHERE name = 'Orc Gains'")
        );
    }
}