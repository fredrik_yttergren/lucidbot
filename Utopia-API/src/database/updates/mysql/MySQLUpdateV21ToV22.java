package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV21ToV22 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 22;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Elf WPA', 'WPA', 'BOTH', true, 0.4) ON DUPLICATE KEY UPDATE bonus_value = 0.4"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Halfling TPA', 'TPA', 'BOTH', true, 0.5) ON DUPLICATE KEY UPDATE bonus_value = 0.5"),
                new SimpleUpdateAction("REPLACE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Elf' AND bonus.name = 'Elf WPA'"),
                new SimpleUpdateAction("REPLACE INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Halfling' AND bonus.name = 'Halfling TPA'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Faery' AND (bonus.type = 'TPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Faery TPA'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Faery' AND (bonus.type = 'WPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Faery WPA'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 5, elite_networth = 5.25 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 4, elite_networth = 5.5 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.5 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.25 WHERE name = 'Elf'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 5.75 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE race SET elite_networth = 6.0 WHERE name = 'Orc'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Warrior OME In War', 'OME_IN_WAR', 'OFFENSIVELY', true, .2) ON DUPLICATE KEY UPDATE bonus_value = .2"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Warrior' AND bonus.name = 'Warrior OME In War'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Human' AND (bonus.type = 'OME_IN_WAR')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Human OME In War'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Halfling' AND (bonus.type = 'GAIN')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Halfling Gains'")
                );
    }
}