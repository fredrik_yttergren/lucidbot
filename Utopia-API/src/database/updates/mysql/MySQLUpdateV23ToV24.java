package database.updates.mysql;

import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV23ToV24 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 24;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE province ADD COLUMN max_population INT NOT NULL")
        );
    }
}