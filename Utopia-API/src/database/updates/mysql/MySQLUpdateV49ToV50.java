package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV49ToV50 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 50;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 9, elite_def_strength = 2, elite_networth = 10 WHERE name = 'Avian'"),
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 8, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9 WHERE name = 'Dark Elf'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Dark Elf' AND (bonus.type = 'WPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Dark Elf WPA'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 8, elite_def_strength = 5, elite_networth = 10 WHERE name = 'Dwarf'"),
                new SimpleUpdateAction("UPDATE race SET def_spec_strength = 7, elite_off_strength = 7, elite_def_strength = 6, elite_networth = 8.5 WHERE name = 'Elf'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Elf WPA', 'WPA', 'BOTH', true, .35) ON DUPLICATE KEY UPDATE bonus_value = .35"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_def_strength = 9, elite_networth = 10.5 WHERE name = 'Faery'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 9, elite_def_strength = 4, elite_networth = 10 WHERE name = 'Human'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Human' AND (bonus.type = 'WAGES')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Human Wages'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 10, elite_def_strength = 1, elite_networth = 9.5 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE race SET elite_off_strength = 4, elite_def_strength = 7, elite_networth = 7 WHERE name = 'Bocan'"),
                new SimpleUpdateAction("UPDATE race SET war_horse_strength = 4, elite_off_strength = 12, elite_def_strength = 2, elite_networth = 10.5 WHERE name = 'Dryad'"),

                new SimpleUpdateAction("INSERT INTO personality (alias, cons, dragon_immune, intel_accuracy, name, pros) VALUES('Undying', '', false, 'NEVER', 'Undead', '')"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 WHERE name = 'Heretic Channeling Sci'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 WHERE name = 'Heretic Crime Sci'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.75 WHERE name = 'Rogue Crime Sci'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.35 WHERE name = 'Sage Sci'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Undead' AND (bonus.type = 'SCI EFF')"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '2.5*#percent#*#be#/100*(1-#percent#/100)', result_text = 'Decreased enemy thief damage: ?$' WHERE result_text = 'Chance of repelling individual thieves: ?%'"),
                new SimpleUpdateAction("UPDATE building_formula SET formula = '1.6*#percent#*#be#/100*(1-#percent#/100)', result_text = 'Catches thieves ?% of the time' WHERE result_text = 'Cactches thieves ?% of the time'")
                );
    }
}