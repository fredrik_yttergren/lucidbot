package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV29ToV30 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 30;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE duration_op ADD COLUMN amount INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE duration_spell ADD COLUMN amount INT NOT NULL")
        );
    }
}