package database.updates.mysql;

import api.tools.common.CleanupUtil;
import com.google.common.collect.Lists;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import lombok.extern.log4j.Log4j;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Log4j
public class MySQLUpdateV38ToV39 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 39;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("UPDATE race SET elite_def_strength = 5 WHERE name = 'Dwarf'")
        );
    }
}