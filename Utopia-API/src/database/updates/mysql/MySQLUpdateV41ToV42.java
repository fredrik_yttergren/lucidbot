package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV41ToV42 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 42;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("CREATE TABLE state_council (id BIGINT, max_population INT, total_population INT, unemployed_peasants INT, " +
                        "unfilled_jobs INT, employment INT, income INT, wages INT, draft_cost INT, honor INT, net_income INT, peasants_born INT, food_grown INT, food_needed INT," +
                        "food_decayed INT, net_food INT, runes_produced INT, runes_decayed INT, net_runes INT, last_updated DATETIME, saved_by VARCHAR(200)," +
                        "province_id BIGINT, PRIMARY KEY (id))"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN id BIGINT NOT NULL AUTO_INCREMENT"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN max_population INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN total_population INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN unemployed_peasants INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN unfilled_jobs INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN employment INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN income INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN wages INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN honor INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN draft_cost INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN net_income INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN peasants_born INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN food_grown INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN food_needed INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN food_decayed INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN net_food INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN runes_produced INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN runes_decayed INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN net_runes INT NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN last_updated DATETIME NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN saved_by VARCHAR(200) NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE state_council MODIFY COLUMN province_id BIGINT NOT NULL")
        );
    }
}