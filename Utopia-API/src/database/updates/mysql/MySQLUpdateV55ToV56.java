package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV55ToV56 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 56;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                //Create ability for personas to have off/def spec strength boosts
                new SimpleUpdateAction("ALTER TABLE personality ADD COLUMN off_spec_strength INTEGER DEFAULT 0 NOT NULL"),
                new SimpleUpdateAction("ALTER TABLE personality ADD COLUMN def_spec_strength INTEGER DEFAULT 0 NOT NULL"),

                //Avian
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 2, elite_networth = 8.5 WHERE name = 'Avian'"),
                //Dwarf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 7, elite_def_strength = 5, elite_networth = 9.5 WHERE name = 'Dwarf'"),
                //Elf
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 7, elite_off_strength = 7, elite_def_strength = 3, elite_networth = 9 WHERE name = 'Elf'"),
                //Faery
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 3, elite_def_strength = 8, elite_networth = 10 WHERE name = 'Faery'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Faery' AND (bonus.type = 'Pop')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Faery Pop'"),
                //Halfling
                new SimpleUpdateAction("UPDATE race SET soldier_strength = 2, soldier_networth = 1, off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 5, elite_def_strength = 7, elite_networth = 9 WHERE name = 'Halfling'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.3 WHERE name = 'Halfling TPA'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.05 WHERE name = 'Halfling Pop'"),
                //Human
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 8, elite_def_strength = 3, elite_networth = 9.5 WHERE name = 'Human'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.15 WHERE name = 'Human WPA'"),
                //Orc
                new SimpleUpdateAction("UPDATE race SET off_spec_strength = 6, def_spec_strength = 6, elite_off_strength = 9, elite_def_strength = 2, elite_networth = 9 WHERE name = 'Orc'"),
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = 0.20 WHERE name = 'Orc Gains'"),
                new SimpleUpdateAction("DELETE race_bonus FROM race_bonus INNER JOIN race ON race_bonus.race_id = race.id INNER JOIN bonus ON race_bonus.bonus_id = bonus.id WHERE " +
                        "race.name = 'Orc' AND (bonus.type = 'TPA')"),
                new SimpleUpdateAction("DELETE FROM bonus WHERE name = 'Orc TPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Orc TPA', 'TPA', 'BOTH', false, .15)"),
                new SimpleUpdateAction(
                        "INSERT INTO race_bonus (race_id, bonus_id) SELECT race.id,bonus.id FROM race,bonus WHERE race.name = 'Orc' AND bonus.name = 'Orc TPA'"),

                //Heretic
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic TPA', 'TPA', 'BOTH', true, .3) ON DUPLICATE KEY UPDATE bonus_value = .3"),
                new SimpleUpdateAction(
                        "INSERT INTO bonus (name, type, applicability, is_increasing, bonus_value) VALUES('Heretic WPA', 'WPA', 'BOTH', true, .3) ON DUPLICATE KEY UPDATE bonus_value = .3"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic TPA'"),
                new SimpleUpdateAction(
                        "INSERT INTO personality_bonus (personality_id, bonus_id) SELECT personality.id,bonus.id FROM personality,bonus WHERE personality.name = 'Heretic' AND bonus.name = 'Heretic WPA'"),
                //Mystic
                //Rogue
                new SimpleUpdateAction("UPDATE bonus SET bonus_value = .75 WHERE name = 'TD Effect'"),
                //Sage
                //Warrior
                //War Hero
                new SimpleUpdateAction("UPDATE personality SET off_spec_strength = 1 WHERE name = 'War Hero'")


                );
    }
}