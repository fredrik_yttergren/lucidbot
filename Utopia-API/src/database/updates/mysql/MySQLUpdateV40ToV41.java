package database.updates.mysql;

import api.database.updates.DatabaseUpdateAction;
import api.database.updates.SimpleUpdateAction;
import com.google.common.collect.Lists;
import lombok.extern.log4j.Log4j;

@Log4j
public class MySQLUpdateV40ToV41 extends ApiMySQLDatabaseUpdater {

    @Override
    public int updatesToVersion() {
        return 41;
    }

    @Override
    public Iterable<? extends DatabaseUpdateAction> getUpdateActions() {
        return Lists.newArrayList(
                new SimpleUpdateAction("ALTER TABLE attack ADD COLUMN conversions INT NOT NULL")
        );
    }
}