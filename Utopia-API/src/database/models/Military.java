package database.models;

import api.common.HasNumericId;
import api.filters.FilterEnabled;
import api.filters.SortEnabled;
import events.MilitarySavedEvent;
import filtering.filters.*;
import intel.Intel;
import intel.ProvinceIntel;
import intel.ProvinceResourceProvider;
import intel.ProvinceResourceType;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.lang.Integer;
import java.util.Date;

@Entity
@Table(name = "military")
@NoArgsConstructor
@EqualsAndHashCode(of = "province")
@Getter
@Setter
public class Military implements ProvinceIntel, Comparable<Military>, HasNumericId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "province_id", nullable = false, unique = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Province province;

    @Column(name = "spec_credits", nullable = false)
    private int specCredits;

    @Column(name = "wage_rate", nullable = false)
    private int wageRate;

    @Column(name = "draft_target", nullable = false)
    private int draftTarget;

    @Column(name = "draft_rate", nullable = false)
    private double draftRate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", nullable = false)
    private Date lastUpdated;

    @Column(name = "saved_by", nullable = false, length = 200)
    private String savedBy;

    private String exportLine;

    public Military(final Province province, final int specCredits, final int wageRate, final int draftTarget, final double draftRate,
               final Date lastUpdated) {
        this.province = province;
        this.specCredits = specCredits;
        this.wageRate = wageRate;
        this.draftTarget = draftTarget;
        this.draftRate = draftRate;
        this.lastUpdated = new Date(lastUpdated.getTime());
    }

    @ProvinceResourceProvider(ProvinceResourceType.SPEC_CREDITS)
    public int getSpecCredits() {
        return specCredits;
    }

    @ProvinceResourceProvider(ProvinceResourceType.WAGERATE)
    public int getWageRate() {
        return wageRate;
    }

    @ProvinceResourceProvider(ProvinceResourceType.DRAFT_TARGET)
    public int getDraftTarget() {
        return draftTarget;
    }

    @ProvinceResourceProvider(ProvinceResourceType.DRAFT_RATE)
    public double getDraftRate() {
        return draftRate;
    }

    @Override
    public int compareTo(final Military o) {
        if (getId() != null && getId().equals(o.getId())) return 0;
        return getProvince().compareTo(o.getProvince());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(400);
        sb.append(getIntelTypeName());
        sb.append("{province=").append(getProvince().getName());
        sb.append(", specCredits=").append(getSpecCredits());
        sb.append(", wageRate=").append(getWageRate());
        sb.append(", draftTarget=").append(getDraftTarget());
        sb.append(", draftRate=").append(getDraftRate());
        sb.append(", savedBy='").append(getSavedBy()).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public Object newSavedEvent() {
        return new MilitarySavedEvent(id);
    }

    @Override
    public String getDescription() {
        return getIntelTypeName() + " of " + getProvince().getName();
    }

    @Override
    public String getIntelTypeName() {
        return getClass().getSimpleName();
    }

    @Override
    public Class<? extends Intel> getIntelType() {
        return getClass();
    }

    @Override
    public String getKingdomLocation() {
        return province.getKingdom().getLocation();
    }

    @Override
    public String getExportLine() {
        return null;
    }

    @Override
    public boolean isUnsaved() {
        return id == null;
    }
}
