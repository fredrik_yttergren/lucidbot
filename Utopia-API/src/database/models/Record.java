package database.models;

import api.common.HasName;
import api.common.HasNumericId;
import lombok.*;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "records")
@NoArgsConstructor
@EqualsAndHashCode(of = {"recordName", "allTimeRecord"})
@Getter
@Setter
public class Record implements HasNumericId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    @Column(name = "record_name", nullable = false, length = 100)
    private String recordName;

    @Column(name = "user_name", nullable = false, length = 100)
    private String userName;

    @Column(name = "record_value", nullable = false)
    private int recordValue;

    @Column(name = "all_time_record", nullable = false)
    private Boolean allTimeRecord;

    public Record(final String recordName, final String userName, final int recordValue, final boolean allTimeRecord) {
        this.recordName = recordName;
        this.userName = userName;
        this.recordValue = recordValue;
        this.allTimeRecord = allTimeRecord;
    }
}
