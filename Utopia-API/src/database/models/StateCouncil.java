/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package database.models;

import api.common.HasNumericId;
import api.filters.FilterEnabled;
import api.filters.SortEnabled;
import events.SoTSavedEvent;
import events.StateCouncilSavedEvent;
import filtering.filters.*;
import intel.Intel;
import intel.ProvinceIntel;
import intel.ProvinceResourceProvider;
import intel.ProvinceResourceType;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.lang.Integer;
import java.util.Date;

@Entity
@Table(name = "state_council")
@NoArgsConstructor
@EqualsAndHashCode(of = "province")
@Getter
@Setter
public class StateCouncil implements ProvinceIntel, Comparable<StateCouncil>, HasNumericId {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    @Setter(AccessLevel.NONE)
    private Long id;

    @OneToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name = "province_id", nullable = false, unique = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Province province;

    @Column(name = "max_population", nullable = false)
    private int maxPopulation;

    @Column(name = "total_population", nullable = false)
    private int totalPopulation;

    @Column(name = "unemployed_peasants", nullable = false)
    private int unemployedPeasants;

    @Column(name = "unfilled_jobs", nullable = false)
    private int unfilledJobs;

    @Column(name = "employment", nullable = false)
    private int employment;

    @Column(name = "income", nullable = false)
    private int income;

    @Column(name = "wages", nullable = false)
    private int wages;

    @Column(name = "draft_cost", nullable = false)
    private int draftCost;

    @Column(name = "honor", nullable = false)
    private int honor;

    @Column(name = "net_income", nullable = false)
    private int netIncome;

    @Column(name = "peasants_born", nullable = false)
    private int peasantsBorn;

    @Column(name = "food_grown", nullable = false)
    private int foodGrown;

    @Column(name = "food_needed", nullable = false)
    private int foodNeeded;

    @Column(name = "food_decayed", nullable = false)
    private int foodDecayed;

    @Column(name = "net_food", nullable = false)
    private int netFood;

    @Column(name = "runes_produced", nullable = false)
    private int runesProduced;

    @Column(name = "runes_decayed", nullable = false)
    private int runesDecayed;

    @Column(name = "net_runes", nullable = false)
    private int netRunes;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_updated", nullable = false)
    private Date lastUpdated;

    @Column(name = "saved_by", nullable = false, length = 200)
    private String savedBy;

    private String exportLine;

    public StateCouncil(final Province province, final int maxPopulation, final int totalPopulation, final int unemployedPeasants, final int unfilledJobs, final int employment, final int income,
               final int wages, final int draftCost, final int honor, final int netIncome, final int peasantsBorn, final int foodGrown, final int foodNeeded, final int foodDecayed,
               final int netFood, final int runesProduced, final int runesDecayed, final int netRunes, final Date lastUpdated) {
        this.province = province;
        this.maxPopulation = maxPopulation;
        this.totalPopulation = totalPopulation;
        this.unemployedPeasants = unemployedPeasants;
        this.unfilledJobs = unfilledJobs;
        this.employment = employment;
        this.income = income;
        this.wages = wages;
        this.draftCost = draftCost;
        this.honor = honor;
        this.netIncome = netIncome;
        this.peasantsBorn = peasantsBorn;
        this.foodGrown = foodGrown;
        this.foodNeeded = foodNeeded;
        this.foodDecayed = foodDecayed;
        this.netFood = netFood;
        this.runesProduced = runesProduced;
        this.runesDecayed = runesDecayed;
        this.netRunes = netRunes;
        this.exportLine = null;
        this.lastUpdated = new Date(lastUpdated.getTime());
    }

    @ProvinceResourceProvider(ProvinceResourceType.INCOME)
    public int getIncome() {
        return income;
    }

    @ProvinceResourceProvider(ProvinceResourceType.NET_INCOME)
    public int getNetIncome() {
        return netIncome;
    }

    public int getWages() {
        return wages;
    }

    @ProvinceResourceProvider(ProvinceResourceType.HONOR)
    public int getHonor() {
        return honor;
    }

    @ProvinceResourceProvider(ProvinceResourceType.RUNES_PRODUCED)
    public int getRunesProduced() {
        return runesProduced;
    }

    @ProvinceResourceProvider(ProvinceResourceType.NET_RUNES)
    public int getNetRunes() {
        return netRunes;
    }

    @ProvinceResourceProvider(ProvinceResourceType.NET_FOOD)
    public int getNetFood() {
        return netFood;
    }

    @ProvinceResourceProvider(ProvinceResourceType.PEASANTS_BORN)
    public int getPeasantsBorn() {
        return peasantsBorn;
    }

    public int getMaxPopulation() {
        return maxPopulation;
    }

    public int getTotalPopulation() {
        return totalPopulation;
    }

    @Override
    public int compareTo(final StateCouncil o) {
        if (getId() != null && getId().equals(o.getId())) return 0;
        return getProvince().compareTo(o.getProvince());
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(600);
        sb.append(getIntelTypeName());
        sb.append("{province=").append(getProvince().getName());
        sb.append(", maxPopulation=").append(getMaxPopulation());
        sb.append(", totalPopulation=").append(getTotalPopulation());
        sb.append(", unemployedPeasants=").append(getUnemployedPeasants());
        sb.append(", unfilledJobs=").append(getUnfilledJobs());
        sb.append(", employment=").append(getEmployment());
        sb.append(", income=").append(getIncome());
        sb.append(", wages=").append(getWages());
        sb.append(", draftCost=").append(getDraftCost());
        sb.append(", honor=").append(getHonor());
        sb.append(", netIncome=").append(getNetIncome());
        sb.append(", foodGrown=").append(getFoodGrown());
        sb.append(", foodNeeded=").append(getFoodNeeded());
        sb.append(", foodDecayed=").append(getFoodDecayed());
        sb.append(", netFood=").append(getNetFood());
        sb.append(", runesProduced=").append(getRunesProduced());
        sb.append(", runesDecayed=").append(getRunesDecayed());
        sb.append(", netRunes=").append(getNetRunes());
        sb.append(", savedBy='").append(getSavedBy()).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public Object newSavedEvent() {
        return new StateCouncilSavedEvent(id);
    }

    @Override
    public String getDescription() {
        return getIntelTypeName() + " of " + getProvince().getName();
    }

    @Override
    public String getIntelTypeName() {
        return getClass().getSimpleName();
    }

    @Override
    public Class<? extends Intel> getIntelType() {
        return getClass();
    }

    @Override
    public String getKingdomLocation() {
        return province.getKingdom().getLocation();
    }

    @Override
    public String getExportLine() {
        return null;
    }

    @Override
    public boolean isUnsaved() {
        return id == null;
    }
}
