package database.daos;

/**
 * Created by Kyle on 5/21/2017.
 */


import api.database.AbstractDAO;
import api.database.DBException;
import api.database.transactions.Transactional;
import api.tools.text.StringUtil;
import database.models.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import api.database.AbstractDAO;
import api.database.DBException;
import api.database.transactions.Transactional;
import api.tools.text.StringUtil;
import com.google.inject.Provider;
import database.models.Bonus;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@ParametersAreNonnullByDefault
public class KingdomRitualDAO extends AbstractDAO<KingdomRitual> {
    @Inject
    public KingdomRitualDAO (final Provider<Session> sessionProvider) {
        super(KingdomRitual.class, sessionProvider);
    }

    @Transactional
    public Collection<KingdomRitual> getAllKingdomRituals() {
        return find();
    }

    @Transactional
    public KingdomRitual getKingdomRitual(final String name) {
        return get(Restrictions.eq("name", name).ignoreCase());
    }

    @Transactional
    public KingdomRitual getKingdomRitual(final long id) {
        return get(Restrictions.idEq(id));
    }

    @Transactional
    public String getKingdomRitualGroup() {
        List<String> names = new ArrayList<>();
        for (KingdomRitual kingdomRitual : find()) {
            names.add(kingdomRitual.getName());
        }
        return StringUtil.merge(names, '|');
    }

    @Transactional
    @Override
    public KingdomRitual save(final KingdomRitual kingdomRitual) {
        try {
            for (Bonus bonus : kingdomRitual.getBonuses()) {
                getSession().saveOrUpdate(bonus);
            }
            return super.save(kingdomRitual);
        } catch (HibernateException e) {
            throw new DBException(e);
        }
    }

    @Transactional
    @Override
    public void delete(final KingdomRitual kingdomRitual) {
        kingdomRitual.clear();
        super.delete(kingdomRitual);
    }
}
