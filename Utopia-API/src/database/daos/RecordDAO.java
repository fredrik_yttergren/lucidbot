package database.daos;

import api.database.AbstractDAO;
import api.database.transactions.Transactional;
import com.google.inject.Provider;
import api.database.models.Nub;
import database.models.Record;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

@ParametersAreNonnullByDefault
public class RecordDAO extends AbstractDAO<Record> {
    @Inject
    public RecordDAO(final Provider<Session> sessionProvider) {
        super(Record.class, sessionProvider);
    }

    @Transactional
    @Nullable
    public Record getRecordById(final long id) {
        return get(Restrictions.idEq(id));
    }

    @Transactional
    @Nullable
    public Record getRecord(final String recordName, final Boolean allTimeRecord) {
        return get(Restrictions.and(Restrictions.eq("recordName", recordName), Restrictions.eq("allTimeRecord", allTimeRecord)));
    }

    @Transactional
    @Nullable
    public List<Record> getCurrentAgeRecords() {
        return find(Restrictions.eq("allTimeRecord", false));
    }

    @Transactional
    @Nullable
    public List<Record> getAllTimeRecords() {
        return find(Restrictions.eq("allTimeRecord", true));
    }

    @Transactional
    @Nullable
    public List<Record> getRecordsForUser(final String userName) {
        return find(Restrictions.eq("userName", userName));
    }

    @Transactional
    @Nullable
    public List<Record> getRecordsOfType(final String recordName) {
        return find(Restrictions.eq("recordName", recordName));
    }

    @Transactional
    @Nullable
    public List<Record> getRecordsForUserOfType(final String userName, final String recordName) {
        return find(Restrictions.and(Restrictions.eq("userName", userName), Restrictions.eq("recordName", recordName)));
    }

    @Transactional
    @Nullable
    public List<Record> getAllRecords() {
        return find();
    }

    @Transactional
    public Record createRecord(final String recordName, final String userName, final int recordValue, final boolean allTimeRecord) {
        Record record = new Record(recordName, userName, recordValue, allTimeRecord);
        super.save(record);
        return record;
    }
}
