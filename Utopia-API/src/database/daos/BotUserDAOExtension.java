package database.daos;

import api.database.models.BotUser;
import api.database.transactions.Transactional;
import org.hibernate.Session;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

@ParametersAreNonnullByDefault
public class BotUserDAOExtension {
    private final Session session;
    private final ProvinceDAO provinceDAO;

    @Inject
    public BotUserDAOExtension(final Session session, final ProvinceDAO provinceDAO) {
        this.session = session;
        this.provinceDAO = provinceDAO;
    }

    @Transactional
    public void delete(final BotUser user) {
        provinceDAO.removeProvinceForUser(user);
        session.createSQLQuery("delete from alarm where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from attendance_status where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from dragon_action where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from duration_op where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from duration_spell where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from instant_op where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from instant_spell where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from notification where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from poll_vote where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from user_activities where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from check_in where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from user_spellop_target where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from nub where nub_name = :name").setParameter("name", user.getMainNick()).executeUpdate();

        session.createSQLQuery("delete from private_message where recipient_user_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from botuser_binding where botuser_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from target_hitter where hitter_id = :id").setParameter("id", user.getId()).executeUpdate();
        session.createSQLQuery("delete from wait where bot_user_id = :id or wait_for_user_id = :otherId")
               .setParameter("id", user.getId())
               .setParameter("otherId", user.getId())
               .executeUpdate();

        session.createSQLQuery("update forum_post set bot_user_id = null where bot_user_id = :id").setParameter("id", user.getId()).executeUpdate();

        session.delete(user);
    }

}
