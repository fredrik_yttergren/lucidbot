/**
 * Created by Kyle on 4/9/2017.
 */
package events;

import tools.time.UtopiaTime;

import lombok.AllArgsConstructor;
import lombok.Getter;
import tools.time.UtopiaTime;

/**
 * The game is going to tick in 5 minutes
 */
@AllArgsConstructor
@Getter
public class PreTickEvent {
    private final UtopiaTime utoDate;
}