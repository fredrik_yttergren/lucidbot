package events;

import database.models.DragonProjectType;
import database.models.NewsItem;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Date;

import javax.annotation.Nullable;

@AllArgsConstructor
@Getter
public class DragonNewsEvent {
    private final Date date;
    private final DragonProjectType type;
    private final int status;
    private final String newsType;
}
