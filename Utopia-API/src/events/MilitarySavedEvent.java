package events;

/**
 * Created by Kyle on 4/2/2017.
 */
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class MilitarySavedEvent {
    public long id;
}
