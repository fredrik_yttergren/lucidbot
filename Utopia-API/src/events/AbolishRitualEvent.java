package events;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Kyle on 7/4/2017.
 */

@AllArgsConstructor
@Getter
public class AbolishRitualEvent {
    public String provName;
    public int destroyed;
    public int abolishRitualsLeft;
}
