package events;

import api.runtime.IRCContext;
import lombok.AllArgsConstructor;
import lombok.Getter;
import javax.annotation.Nullable;

/**
 * Created by Kyle on 4/23/2016.
 */
@AllArgsConstructor
@Getter
public class NewRecordEvent {
    @Nullable
    private final IRCContext context;
    private final long recordId;
}
