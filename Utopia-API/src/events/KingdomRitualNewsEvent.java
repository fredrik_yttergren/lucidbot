package events;

import database.models.KingdomRitualProjectType;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class KingdomRitualNewsEvent {
    private final Date date;
    private final KingdomRitualProjectType type;
    private final int status;
    private final String newsType;
}
