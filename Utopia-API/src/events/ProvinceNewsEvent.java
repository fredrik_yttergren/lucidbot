package events;

import api.database.models.BotUser;
import api.runtime.IRCContext;
import database.models.Province;
import lombok.AllArgsConstructor;
import lombok.Getter;
import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Kyle on 8/12/2018.
 */
@AllArgsConstructor
@Getter
public class ProvinceNewsEvent {
    private final BotUser user;
    private final Province province;
    private final List<String> news;
}
