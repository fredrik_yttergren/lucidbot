package events;

import database.models.Province;
import database.models.Province;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Kyle on 7/4/2017.
 */

@AllArgsConstructor
@Getter
public class MysticVortexEvent {
    public String provName;
    public String spells;
    public int result;
}
