package tools;

import database.models.AttackType;
import lombok.*;

/**
 * Created by Kyle on 4/1/2017.
 */
@Getter
@Setter
public class CalculatedAttack {
    private final AttackType type;
    private final int soldiers;
    private final int offSpecs;
    private final int elites;
    private final int warHorses;
    private final int modOffense;

    public CalculatedAttack(final AttackType type, final int soldiers,
                            final int offSpecs, final int elites, final int warHorses, final int modOffense) {
        this.type = type;
        this.soldiers = soldiers;
        this.offSpecs = offSpecs;
        this.elites = elites;
        this.warHorses = warHorses;
        this.modOffense = modOffense;
    }
}
