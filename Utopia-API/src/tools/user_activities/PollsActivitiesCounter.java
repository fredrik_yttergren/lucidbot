package tools.user_activities;

import api.database.models.BotUser;
import database.daos.PollDAO;
import tools.BindingsManager;

import javax.inject.Inject;
import java.util.Date;

public class PollsActivitiesCounter implements RecentActivitiesCounter {
    private final PollDAO pollDAO;
    private final BindingsManager bindingsManager;

    @Inject
    public PollsActivitiesCounter(final PollDAO pollDAO, final BindingsManager bindingsManager) {
        this.pollDAO = pollDAO;
        this.bindingsManager = bindingsManager;
    }

    @Override
    public int countNewActivities(final Date lastCheck, final BotUser user) {
        return pollDAO.countPollsForUserAddedAfter(lastCheck, user, bindingsManager);
    }
}
