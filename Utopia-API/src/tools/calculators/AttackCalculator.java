package tools.calculators;

import database.models.*;
import tools.CalculatedAttack;

import javax.inject.Inject;
import java.lang.Math;

/**
 * Created by Kyle on 4/1/2017.
 */
public class AttackCalculator {

    public CalculatedAttack calcAttack(final AttackType attackType, final Province targetProvince, final Province province, final boolean offSpecsPriority,
                                       final boolean elitesPriority) {

        Race race = province.getRace();
        final int estimatedDefense = targetProvince.getEstimatedCurrentDefense();
        //final Army armyHome = province.getSom().getArmyHome();

        double remainingDefense = estimatedDefense * 1.05;

        final int offSpecStrength = race.getOffSpecStrength();
        final int eliteOffStrength = race.getEliteOffStrength();
        //TODO: Convert war horses to a system-wide value
        final int warHorseStrength = race.getWarHorseStrength();

        int offSpecsToSend = 0;
        int elitesToSend = 0;

        if (offSpecsPriority) {
        }
        if (elitesPriority) {
        }

        //offSpecsToSend += Math.max((estimatedDefense / 2)

        while (remainingDefense > estimatedDefense / 2) {
            remainingDefense -= offSpecStrength;
            offSpecsToSend += 1;
        }

        while (remainingDefense > 0) {
            remainingDefense -= eliteOffStrength;
            elitesToSend += 1;
        }

        //Optimize for horses
        int warHorsesToSend = (int) Math.min((elitesToSend + offSpecsToSend) * .9, province.getWarHorses());
        remainingDefense -= (warHorsesToSend * warHorseStrength);

        while (remainingDefense < 0) {
            remainingDefense += offSpecStrength;
            remainingDefense += eliteOffStrength;
            offSpecsToSend -= 1;
            elitesToSend -= 1;
        }

        final int modOffense = (int) ((offSpecsToSend * offSpecStrength) + (elitesToSend * eliteOffStrength) +
                (warHorsesToSend * warHorseStrength) * province.getOffensiveME());

        return new CalculatedAttack(attackType, 0, elitesToSend, offSpecsToSend, warHorsesToSend, modOffense);
    }
}
