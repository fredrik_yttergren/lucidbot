/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package tools.calculators;

import database.models.*;

import javax.inject.Inject;
import java.util.Date;

import lombok.extern.log4j.Log4j;

/**
 * A class that returns thieves.  If thief values are known, it will return that.
 * Otherwise, it will estimate thief quantities based off of SoT/Survey/SoS as available.  If Survey/SoS are not available it will estimate those values.
 */
@Log4j
public class TPACalculator {
    private final NetworthCalculator networthCalculator;
    private final PopulationCalculator populationCalculator;

    @Inject
    public TPACalculator(final NetworthCalculator networthCalculator, final PopulationCalculator populationCalculator) {
        this.networthCalculator = networthCalculator;
        this.populationCalculator = populationCalculator;

    }

    public int calcThieves(final Province province, final SoT sot, final SoM som) {
        return populationCalculator.calcPopulation(province, sot, som, true);
    }

    public int calcThieves(final Province province, final SoT sot, final SoS sos, final Survey survey) {
        return networthCalculator.calcNetworth(province, sot, sos, survey, true);
    }

    public void updateThieves(final Province province) {
        if (!province.getThievesAccurate() && province.getSot() != null) {
            int thieves = calcThieves(province, province.getSot(), province.getSom());
            setThieves(province, thieves);
        }
    }

    public void updateThieves(final Province province, final SoT sot) {
        if (!province.getThievesAccurate() && sot != null) {
            int thieves = calcThieves(province, sot, province.getSos(), province.getSurvey());
            setThieves(province, thieves);
        }
    }

    public void updateThieves(final Province province, final Survey survey) {
        if (!province.getThievesAccurate() && province.getSot() != null) {
            int thieves = calcThieves(province, province.getSot(), province.getSos(), survey);
            setThieves(province, thieves);
        }
    }

    public void updateThieves(final Province province, final SoS sos) {
        if (!province.getThievesAccurate() && province.getSot() != null) {
            int thieves = calcThieves(province, province.getSot(), sos, province.getSurvey());
            setThieves(province, thieves);
        }
    }

    public void setThieves(final Province province, final int thieves) {
        province.setThieves(thieves);
        province.setThievesAccurate(false);
    }
}