package tools.calculators;

import database.models.*;
import javax.inject.Inject;
import java.util.Date;

import lombok.extern.log4j.Log4j;

@Log4j
public class PopulationCalculator {
    private final NetworthCalculator networthCalculator;

    @Inject
    public PopulationCalculator(final NetworthCalculator networthCalculator) {
        this.networthCalculator = networthCalculator;
    }

    public int calcPopulation(final Province province, final SoT sot, final SoM som, final boolean personaAdjustment) {
        if (province.getMaxPopulation() == 0) return 0;
        if (sot == null) return 0;
        Personality persona = province.getPersonality();

        int pop = province.getMaxPopulation();
        pop -= sot.getSoldiers();
        pop -= sot.getElites();
        pop -= sot.getOffSpecs();
        pop -= sot.getDefSpecs();
        pop -= sot.getPeasants();

        if (som != null) {
            pop -= som.getTroopsInTraining();
        }

        if (personaAdjustment) { pop = networthCalculator.personaAdjustment(pop, persona); }

        return Math.max(0, pop);
    }
}
