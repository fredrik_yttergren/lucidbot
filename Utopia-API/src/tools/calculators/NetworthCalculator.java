package tools.calculators;

import database.CommonEntitiesAccess;
import database.models.*;
import tools.time.UtopiaTimeFactory;
import javax.inject.Inject;
import java.util.Date;

import lombok.extern.log4j.Log4j;

@Log4j
public class NetworthCalculator {
    private final UtopiaTimeFactory utopiaTimeFactory;

    private final static double SCIENTIST_NW = 2.6;
    private final static double BOOK_NW = .015;

    @Inject
    public NetworthCalculator(final UtopiaTimeFactory utopiaTimeFactory) {
        this.utopiaTimeFactory = utopiaTimeFactory;
    }

    public int calcNetworth(final Province province, final SoT sot, final SoS sos, final Survey survey,
                            final boolean personaAdjustment) {
        Race race = province.getRace();
        Personality persona = province.getPersonality();

        int nw = province.getNetworth();
        nw -= sot.getSoldiers() * race.getSoldierNetworth();
        nw -= sot.getPeasants() * .25;
        nw -= sot.getDefSpecs() * (race.getDefSpecStrength() + persona.getDefSpecStrength());
        nw -= sot.getOffSpecs() * (race.getOffSpecStrength() + persona.getOffSpecStrength()) * 0.8;
        nw -= sot.getElites() * race.getEliteNetworth();
        nw -= sot.getWarHorses() * race.getWarHorseStrength() * .6;
        nw -= sot.getMoney() / 1000.0;

        int utoYear = (utopiaTimeFactory.newUtopiaTime(System.currentTimeMillis()).getYear());

        if (sos != null) {
            nw -= (int) ((sos.getTotalBooks() + sos.getTotalUnallocatedBooks()) * BOOK_NW);
        } else {
            //Super complicated algo to estimate books over time.  Redo this with something more sane.
            nw -=  (int) ((Math.log(((double) utoYear + 1) * utoYear + 1) * utoYear) + 1) * ((utoYear * 30000) + 100000) * BOOK_NW;
        }

        nw -= survey == null ? province.getLand() * 60 : (survey.getTotalBuilt() - survey.getBuildingAmount("barren")) * 60
                + survey.getTotalInProgress() * 50 + survey.getBuildingAmount("barren") * 40;

        if (personaAdjustment) { nw = personaAdjustment(nw, persona); }

        return Math.max(0, nw / 4);
    }

    public int personaAdjustment(final int value, final Personality persona) {
        //TODO: Convert these to a bonus or some other bot operator-adjustable system
        if (persona != null) {
            if (persona.getName().equals("Rogue")) {
                return (int) (value * .66);
            } else if (persona.getName().equals("Mystic") || persona.getName().equals("Heretic")) {
                return (int) (value * .33);
            } else {
                return (int) (value * .5);
            }
        } return (int) (value * .5);
    }

}
