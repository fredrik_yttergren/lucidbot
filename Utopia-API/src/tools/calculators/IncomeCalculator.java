package tools.calculators;

import api.tools.numbers.NumberUtil;
import database.CommonEntitiesAccess;
import database.models.*;
import tools.GameMechanicCalculator;

import javax.inject.Inject;
import java.util.List;
import lombok.extern.log4j.Log4j;

/**
 * Created by Kyle on 6/24/2017.
 *
 * This class calculates a provinces income based upon known values
 */
@Log4j
public class IncomeCalculator {
    private final GameMechanicCalculator gameMechanicCalculator;
    private final CommonEntitiesAccess commonEntitiesAccess;


    @Inject
    public IncomeCalculator(final GameMechanicCalculator gameMechanicCalculator, final CommonEntitiesAccess commonEntitiesAccess) {
        this.gameMechanicCalculator = gameMechanicCalculator;
        this.commonEntitiesAccess = commonEntitiesAccess;
    }

    public int calcIncome(final Province province) {
        if (province.getStateCouncil() != null) return province.getStateCouncil().getIncome();
        SoT sot = province.getSot();
        Survey survey = province.getSurvey();
        SoS sos = province.getSos();

        //Can't calc without a sot
        if (sot == null) return 0;

        double plaguePenalty = sot.isPlagued() ? .85 : 1;
        double bankBonus = 1;
        double buildingEfficiency = sot.getBuildingEfficiency();
        double income = sot.getPeasants() * 2.75;

        //If we have a survey, factor in bank income
        if (survey != null) {
            try {
                Building building = commonEntitiesAccess.getBuilding("banks");
                for (BuildingFormula buildingFormula : building.getFormulas()) {
                    if (buildingFormula.getResultText().contains("Increases income")) {
                        bankBonus = 1 + (gameMechanicCalculator.performRawBuildingCalc(buildingFormula,
                                survey.getBuildingPercentage("banks"), (double) survey.getBuildingAmount("banks"),
                                buildingEfficiency) * .01);
                    }
                    if (buildingFormula.getResultText().contains("Produces ? gcs")) {
                        income += gameMechanicCalculator.performRawBuildingCalc(buildingFormula,
                                survey.getBuildingPercentage("banks"), (double) survey.getBuildingAmount("banks"),
                                buildingEfficiency);
                    }
                }
            } catch (Exception e) {
                log.error("Something bad happened when calculating bank bonus " + e);
            }
        }

        double sciBonus = sos != null ? (1 + sos.getScienceEffectByType("alchemy") * .01) : 1;

        income = province.getHonorTitle() != null ? province.getHonorTitle().getBonus(BonusType.INCOME, BonusApplicability.BOTH).applyTo(income) : income;
        income = province.getRace() != null ? province.getRace().getBonus(BonusType.INCOME, BonusApplicability.BOTH).applyTo(income) : income;
        income = province.getPersonality() != null ? province.getPersonality().getBonus(BonusType.INCOME, BonusApplicability.BOTH).applyTo(income) : income;
        income = province.getKingdom().getDragon() != null ? province.getKingdom().getDragon().getBonus(BonusType.INCOME, BonusApplicability.BOTH).applyTo(income) : income;

        return (int) (income * plaguePenalty * bankBonus * sciBonus);


    }

}
