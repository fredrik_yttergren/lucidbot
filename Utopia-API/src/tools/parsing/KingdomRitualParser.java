/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package tools.parsing;

import api.database.models.AccessLevel;
import api.events.bot.NonCommandEvent;
import api.irc.ValidationType;
import api.runtime.IRCContext;
import api.runtime.ThreadingManager;
import api.tools.numbers.NumberUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import database.models.DragonProjectType;
import database.models.KingdomRitual;
import database.models.KingdomRitualAction;
import database.models.KingdomRitualProjectType;
import events.DragonActionEvent;
import events.DragonProjectUpdateEvent;
import events.KingdomRitualActionEvent;
import events.KingdomRitualProjectUpdateEvent;
import lombok.extern.log4j.Log4j;
import spi.events.EventListener;

import javax.annotation.Nullable;
import javax.inject.Inject;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Log4j
public class KingdomRitualParser implements EventListener {
    private static final Pattern CASTING = Pattern
            .compile("We are now closer to completing our ritual project!");
    private static final Pattern CURRENT_CASTS_LEFT = Pattern
            .compile("(" + ValidationType.INT.getPattern() + ") casts have been performed out of (" + ValidationType.INT.getPattern() + ") to fully complete a ritual");

    private final ThreadingManager threadingManager;
    private final EventBus eventBus;

    @Inject
    public KingdomRitualParser(final ThreadingManager threadingManager, final EventBus eventBus) {
        this.threadingManager = threadingManager;
        this.eventBus = eventBus;
    }

    @Subscribe
    public void onNonCommandEvent(final NonCommandEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                IRCContext context = event.getContext();
                if (!AccessLevel.USER.allows(context.getUser(), context.getChannel())) return;

                parse(context.getInput(), context, context.getUser().getMainNick());
            }
        });
    }

    public void parse(final String userMainNick, final String text) {
        parse(text, null, userMainNick);
    }

    private void parse(final String text, @Nullable final IRCContext context, final String userMainNick) {
        Matcher matcher = CASTING.matcher(text);
        if (matcher.find()) {
            eventBus.post(new KingdomRitualActionEvent(context, userMainNick, KingdomRitualProjectType.CASTING, 1));
        }

        matcher = CURRENT_CASTS_LEFT.matcher(text);
        if (matcher.find()) {
            int left = NumberUtil.parseInt(matcher.group(2)) - NumberUtil.parseInt(matcher.group(1));
            eventBus.post(new KingdomRitualProjectUpdateEvent(context, userMainNick, KingdomRitualProjectType.CASTING, left));
        }
    }
}
