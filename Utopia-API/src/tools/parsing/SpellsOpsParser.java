/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package tools.parsing;

import api.database.models.AccessLevel;
import api.database.models.BotUser;
import api.events.bot.NonCommandEvent;
import api.runtime.IRCContext;
import api.runtime.ThreadingManager;
import api.tools.numbers.NumberUtil;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import database.CommonEntitiesAccess;
import database.models.*;
import events.AbolishRitualEvent;
import events.MysticVortexEvent;
import events.OpPastedEvent;
import events.SpellPastedEvent;
import spi.events.EventListener;
import tools.target_locator.CharacterDrivenTargetLocatorFactory;
import tools.target_locator.TargetLocator;
import tools.target_locator.TargetLocatorFactory;

import javax.inject.Inject;
import javax.inject.Provider;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.log4j.Log4j;

@Log4j
public class SpellsOpsParser implements EventListener {
    private final Provider<CharacterDrivenTargetLocatorFactory> defaultTargetLocatorFactory;
    private final ThreadingManager threadingManager;
    private final EventBus eventBus;

    private final ConcurrentMap<Pattern, SpellType> spellPatternsMap = new ConcurrentHashMap<>();
    private final ConcurrentMap<Pattern, OpType> opPatternsMap = new ConcurrentHashMap<>();

    @Inject
    public SpellsOpsParser(final ThreadingManager threadingManager,
                           final CommonEntitiesAccess commonEntitiesAccess,
                           final Provider<CharacterDrivenTargetLocatorFactory> defaultTargetLocatorFactory,
                           final EventBus eventBus) {
        this.threadingManager = threadingManager;
        this.defaultTargetLocatorFactory = defaultTargetLocatorFactory;
        this.eventBus = eventBus;

        for (SpellType spellType : commonEntitiesAccess.getAllSpellTypes()) {
            if (spellType.getCastRegex() != null) spellPatternsMap.put(Pattern.compile(spellType.getCastRegex()), spellType);
            //Below is for catching paladin casting self spells upon others.
            spellPatternsMap.put(Pattern.compile("We have cast " + spellType.getName() + " upon (?<target>[^(]+\\(\\d{1,2}:\\d{1,2}\\)) for (?<result>[\\d,]+)"), spellType);
        }

        for (OpType opType : commonEntitiesAccess.getAllOpTypes()) {
            if (opType.getOpRegex() != null) opPatternsMap.put(Pattern.compile(opType.getOpRegex()), opType);
        }
    }

    @Subscribe
    public void onNonCommandEvent(final NonCommandEvent event) {
        IRCContext context = event.getContext();
        if (context.getBotUser() == null || !AccessLevel.USER.allows(context.getUser(), context.getChannel())) return;

        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    parse(event.getContext().getInput(), defaultTargetLocatorFactory.get(), event.getContext().getBotUser(), true, event.getContext());
                } catch (Exception e) {
                    log.info(e);
                }
            }
        });
    }

    /**
     * Parses the specified text for spells and ops and returns as soon as it finds a match.
     *
     * @param botUser the user that pasted the spell/op
     * @param text    the text to parse
     * @return 0 if no spells or ops were parsed, 1 otherwise
     */
    public int parseSingle(final BotUser botUser, final String text, final TargetLocatorFactory targetLocatorFactory) {
        return parse(text, targetLocatorFactory, botUser, true, null);
    }

    /**
     * Parses the specified text for spells and ops. Keeps going to find as many matches as possible and returns how many were found.
     *
     * @param botUser the user that pasted the spell/op
     * @param text    the text to parse
     * @return the amount of matches found
     */
    public int parseMultiple(final BotUser botUser, final String text, final TargetLocatorFactory targetLocatorFactory) {
        return parse(text, targetLocatorFactory, botUser, false, null);
    }

    private int parse(final String text,
                      final TargetLocatorFactory targetLocatorFactory,
                      final BotUser botUser,
                      final boolean quitAfterFirstMatch,
                      final IRCContext context) {
        int matchCounter = parseSpells(text, targetLocatorFactory, botUser, quitAfterFirstMatch, context);
        if (quitAfterFirstMatch && matchCounter != 0) return matchCounter;

        matchCounter += parseOps(text, targetLocatorFactory, botUser, quitAfterFirstMatch, context);
        return matchCounter;
    }

    private int parseSpells(final String text,
                            final TargetLocatorFactory targetLocatorFactory,
                            final BotUser botUser,
                            final boolean quitAfterFirstMatch,
                            final IRCContext context) {
        Matcher matcher;
        int matchCounter = 0;

        Map<Province, Map<SpellType, Map<SpellOpDamageType, ActionSpecs>>> parsedSpells = new HashMap<>();
        for (Map.Entry<Pattern, SpellType> entry : spellPatternsMap.entrySet()) {
            matcher = entry.getKey().matcher(text);
            while (matcher.find()) {
                SpellType type = entry.getValue();
                Province target;
                //Wedging this in here because the bot's structure really resists casting self spells upon others.
                if (entry.getKey().toString().contains("We have cast")) {
                    target = locateTarget(targetLocatorFactory, botUser, matcher, SpellOpCharacter.FADING_SPELLOP_WITH_PROVINCE);
                } else target = locateTarget(targetLocatorFactory, botUser, matcher, type.getSpellCharacter());

                //Wedging this in here because making mystic vortex play nice with the rest of the bot would be a lot of work and its the only spell of this type.
                if (entry.getKey().toString().contains("A magic vortex overcomes the province of")) {
                    if (matcher.groupCount() == 4) {
                        eventBus.post(new MysticVortexEvent(target.getName(), matcher.group("spells"), NumberUtil.parseInt(matcher.group("result"))));
                    }
                }

                //Really need to rewrite spellops parsing, because more and more of these special spells keep coming up. (This one is for ritual destruction)
                if (entry.getKey().toString().contains("Your mages infest the guilds of")) {
                    eventBus.post(new AbolishRitualEvent(target.getName(), NumberUtil.parseInt(matcher.group("destroyed")), parseAbolishRitualTargetsRemaining(matcher.group("abolishRitualsLeft"))));
                }

                if (target != null) {
                    Map<SpellType, Map<SpellOpDamageType, ActionSpecs>> spellsForTarget = addResults(matcher, parsedSpells, type, type.getSpellCharacter(), target);
                    if (quitAfterFirstMatch) {
                        for (Map.Entry<SpellOpDamageType, ActionSpecs> damageEntry : spellsForTarget.get(type).entrySet()) {
                            ActionSpecs actionSpecs = damageEntry.getValue();
                            eventBus.post(new SpellPastedEvent(target.getId(), type.getId(), actionSpecs.amount, actionSpecs.damage, actionSpecs.largestAction, damageEntry.getKey(), botUser, context));
                            return 1;
                        }
                    }
                    ++matchCounter;
                }
            }
        }
        postSpellEvents(botUser, context, parsedSpells);
        return matchCounter;
    }

    private void postSpellEvents(final BotUser botUser, final IRCContext context, final Map<Province, Map<SpellType, Map<SpellOpDamageType, ActionSpecs>>> parsedSpells) {
        for (Map.Entry<Province, Map<SpellType, Map<SpellOpDamageType, ActionSpecs>>> entry : parsedSpells.entrySet()) {
            for (Map.Entry<SpellType, Map<SpellOpDamageType, ActionSpecs>> specsEntry : entry.getValue().entrySet()) {
                for (Map.Entry<SpellOpDamageType, ActionSpecs> damageEntry : specsEntry.getValue().entrySet()) {
                    eventBus.post(new SpellPastedEvent(entry.getKey().getId(), specsEntry.getKey().getId(), damageEntry.getValue().amount,
                            damageEntry.getValue().damage, damageEntry.getValue().largestAction, damageEntry.getKey(), botUser, context));
                }
            }
        }
    }


    private int parseOps(final String text,
                         final TargetLocatorFactory targetLocatorFactory,
                         final BotUser botUser,
                         final boolean quitAfterFirstMatch,
                         final IRCContext context) {
        Matcher matcher;
        int matchCounter = 0;

        Map<Province, Map<OpType, Map<SpellOpDamageType, ActionSpecs>>> parsedOps = new HashMap<>();
        for (Map.Entry<Pattern, OpType> entry : opPatternsMap.entrySet()) {
            matcher = entry.getKey().matcher(text);
            while (matcher.find()) {
                OpType type = entry.getValue();
                Province target = locateTarget(targetLocatorFactory, botUser, matcher, type.getOpCharacter());
                if (target != null) {
                    Map<OpType, Map<SpellOpDamageType, ActionSpecs>> opsForTarget = addResults(matcher, parsedOps, type, type.getOpCharacter(), target);

                    if (quitAfterFirstMatch) {
                        for (Map.Entry<SpellOpDamageType, ActionSpecs> damageEntry : opsForTarget.get(type).entrySet()) {
                            ActionSpecs actionSpecs = damageEntry.getValue();
                            eventBus.post(new OpPastedEvent(target.getId(), type.getId(), actionSpecs.amount, actionSpecs.damage, actionSpecs.largestAction, damageEntry.getKey(), botUser, context));
                            return 1;
                        }
                    }
                    ++matchCounter;
                }
            }
        }
        postOpEvents(botUser, context, parsedOps);

        return matchCounter;
    }

    private void postOpEvents(final BotUser botUser, final IRCContext context, final Map<Province, Map<OpType, Map<SpellOpDamageType, ActionSpecs>>> parsedOps) {
        for (Map.Entry<Province, Map<OpType, Map<SpellOpDamageType, ActionSpecs>>> entry : parsedOps.entrySet()) {
            for (Map.Entry<OpType, Map<SpellOpDamageType, ActionSpecs>> specsEntry : entry.getValue().entrySet()) {
                for (Map.Entry<SpellOpDamageType, ActionSpecs> damageEntry : specsEntry.getValue().entrySet()) {
                    eventBus.post(new OpPastedEvent(entry.getKey().getId(), specsEntry.getKey().getId(), damageEntry.getValue().amount,
                            damageEntry.getValue().damage, damageEntry.getValue().largestAction, damageEntry.getKey(), botUser, context));
                }
            }
        }
    }

    private static Province locateTarget(final TargetLocatorFactory targetLocatorFactory,
                                         final BotUser botUser,
                                         final Matcher matcher,
                                         final SpellOpCharacter spellOpCharacter) {
        TargetLocator targetLocator = targetLocatorFactory.createLocator(spellOpCharacter);
        return targetLocator.locateTarget(botUser, matcher);
    }

    private static <T> Map<T, Map<SpellOpDamageType, ActionSpecs>> addResults(final Matcher matcher,
                                                                              final Map<Province, Map<T, Map<SpellOpDamageType, ActionSpecs>>> parsed,
                                                                              final T type,
                                                                              final SpellOpCharacter spellOpCharacter,
                                                                              final Province target) {
        if (!parsed.containsKey(target)) parsed.put(target, new HashMap<T, Map<SpellOpDamageType, ActionSpecs>>());

        Map<T, Map<SpellOpDamageType, ActionSpecs>> spellsForTarget = parsed.get(target);
        SpellOpDamageType damageType = SpellOpDamageType.NONE;
        if (matcher.group(0).contains("We have converted")) {
            damageType = SpellOpDamageType.getByName(matcher.group("damagetype")) != null ? SpellOpDamageType.getByName(matcher.group("damagetype")) : SpellOpDamageType.NONE;
        }

        if (spellsForTarget.containsKey(type) && spellOpCharacter.isInstant()) {
            if (!spellsForTarget.get(type).containsKey(damageType)) {
                spellsForTarget.get(type).put(damageType, new ActionSpecs(getResult(matcher)));
            } else { spellsForTarget.get(type).get(damageType).add(getResult(matcher)); }
        } else {
            Map<SpellOpDamageType, ActionSpecs> damageSpec = new HashMap<>();
            damageSpec.put(damageType, new ActionSpecs(getResult(matcher)));
            spellsForTarget.put(type, damageSpec);
        }
        return spellsForTarget;
    }

    private int parseAbolishRitualTargetsRemaining(final String value) {
        if (value.contains("no longer")) return 0;
        else if (value.contains("once")) return 1;
        else {
            Pattern numberPattern = Pattern.compile("\\d+");
            Matcher matcher = numberPattern.matcher(value);
            if (matcher.find())
                return NumberUtil.parseInt(matcher.group(0));
            throw new IllegalArgumentException("Could not parse number of AR's remaining on target");
        }
    }

    private static class ActionSpecs {
        private int amount;
        private int damage;
        private int largestAction;

        private ActionSpecs(final int damage) {
            this.amount = 1;
            this.damage = damage;
            this.largestAction = damage;
        }

        private void add(final int damage) {
            ++amount;
            this.damage += damage;
            if (damage > this.largestAction) { this.largestAction = damage; }
        }
    }

    private static Integer getResult(final Matcher matcher) {
        try {
            return NumberUtil.parseInt(matcher.group("result"));
        } catch (IllegalArgumentException e) {
            return 0;
        }
    }
}
