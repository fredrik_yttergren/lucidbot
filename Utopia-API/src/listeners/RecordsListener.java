package listeners;

import database.daos.*;
import database.models.*;
import events.*;
import spi.events.EventListener;
import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.irc.communication.IRCAccess;
import api.runtime.ThreadingManager;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import lombok.extern.log4j.Log4j;
import org.hibernate.HibernateException;
import spi.events.EventListener;

import javax.inject.Inject;
import java.util.Date;
import java.util.Set;

import tools.communication.NotificationDeliverer;
import tools.time.UtopiaTimeFactory;

import static api.database.transactions.Transactions.inTransaction;
import static api.tools.text.StringUtil.prettifyEnumName;

/**
 * Performs record listening, registration, and announcement
 */


@Log4j
class RecordsListener implements EventListener {
    private final Provider<BotUserDAO> botUserDAOProvider;
    private final Provider<ProvinceDAO> provinceDAOProvider;
    private final Provider<SpellDAO> spellDAOProvider;
    private final Provider<OpDAO> opDAOProvider;
    private final Provider<IntelDAO> intelDAOProvider;
    private final Provider<RecordDAO> recordDAOProvider;
    private final Provider<AttackDAO> attackDAOProvider;
    private final ThreadingManager threadingManager;
    private final IRCAccess ircAccess;
    private final EventBus eventBus;

    @Inject
    RecordsListener(final Provider<BotUserDAO> botUserDAOProvider,
                         final Provider<ProvinceDAO> provinceDAOProvider,
                         final Provider<SpellDAO> spellDAOProvider,
                         final Provider<OpDAO> opDAOProvider,
                         final Provider<IntelDAO> intelDAOProvider,
                         final EventBus eventBus,
                         final ThreadingManager threadingManager,
                         final IRCAccess ircAccess,
                         final Provider<RecordDAO> recordDAOProvider,
                         final Provider<AttackDAO> attackDAOProvider) {
        this.botUserDAOProvider = botUserDAOProvider;
        this.provinceDAOProvider = provinceDAOProvider;
        this.spellDAOProvider = spellDAOProvider;
        this.opDAOProvider = opDAOProvider;
        this.intelDAOProvider = intelDAOProvider;
        this.threadingManager = threadingManager;
        this.ircAccess = ircAccess;
        this.recordDAOProvider = recordDAOProvider;
        this.eventBus = eventBus;
        this.attackDAOProvider = attackDAOProvider;
    }

    @Subscribe
    public void onOpAdded(final OpPastedEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                String recordName = opDAOProvider.get().getOpType(event.getOpTypeId()).getName();
                String userName = event.getUser().getMainNick();
                Integer result = event.getLargestAction();
                checkRecord(recordName, userName, result);
            }
        });
    }

    @Subscribe
    public void onSpellAdded(final SpellPastedEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                SpellType spellType = spellDAOProvider.get().getSpellType(event.getSpellTypeId());
                if (spellType.getSpellCharacter().equals(SpellOpCharacter.SELF_SPELLOP) || spellType.getSpellCharacter().equals(SpellOpCharacter.SELF_SPELLOP)
                        || spellType.getSpellCharacter().equals(SpellOpCharacter.INSTANT_SELF_SPELLOP) || spellType.getSpellCharacter().equals(SpellOpCharacter.OTHER)) { return; }
                String recordName = spellType.getName();
                String userName = event.getUser().getMainNick();
                Integer result = event.getLargestAction();
                checkRecord(recordName, userName, result);
            }
        });
    }
    @Subscribe
    public void onAttackAdded(final AttackInfoPastedEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                String recordName = event.getAttackType().getName();
                String userName = event.getAttackerMainNick();
                Integer result = event.getGain();

                if (event.getConversions() != null) {
                    checkRecord("Conversions", userName, event.getConversions());
                }
                checkRecord(recordName, userName, result);
            }
        });
    }

    public void checkRecord(final String recordName, final String userName, final int result) {
        RecordDAO recordDAO = recordDAOProvider.get();
        Record currentAgeRecord = recordDAO.getRecord(recordName, false);
        Record allTimeRecord = recordDAO.getRecord(recordName, true);

        if (result == 0) {
            return;
        }
        if (currentAgeRecord != null) {
            if (result > currentAgeRecord.getRecordValue()) {
                currentAgeRecord.setUserName(userName);
                currentAgeRecord.setRecordValue(result);
                recordDAO.save(currentAgeRecord);
                eventBus.post(new NewRecordEvent(null, currentAgeRecord.getId()));
            }
        } else {
            Record record = recordDAO.createRecord(recordName, userName, result, false);
            eventBus.post(new NewRecordEvent(null, record.getId()));
        }

        if (allTimeRecord != null) {
            if (result > allTimeRecord.getRecordValue()) {
                allTimeRecord.setUserName(userName);
                allTimeRecord.setRecordValue(result);
                recordDAO.save(allTimeRecord);
                eventBus.post(new NewRecordEvent(null, allTimeRecord.getId()));
            }
        } else {
            Record record = recordDAO.createRecord(recordName, userName, result, true);
            eventBus.post(new NewRecordEvent(null, record.getId()));
        }
    }
}