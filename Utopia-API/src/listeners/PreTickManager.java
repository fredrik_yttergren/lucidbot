package listeners;

/**
 * Created by Kyle on 4/9/2017.
 */

import api.events.bot.StartupEvent;
import api.timers.Timer;
import api.timers.TimerManager;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import events.PreTickEvent;
import spi.events.EventListener;
import spi.timers.TimerRunOutHandler;
import tools.time.UtopiaTime;
import tools.time.UtopiaTimeFactory;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

/**
 * A class that manages pre-tick events.  Useful for firing events 5 minutes before tick.
 */
public class PreTickManager implements TimerRunOutHandler, EventListener {
    private final UtopiaTimeFactory utopiaTimeFactory;
    private final EventBus eventBus;
    private final TimerManager timerManager;

    private long lastPreTickEvent;
    private long lastPreTickId;

    @Inject
    public PreTickManager(final UtopiaTimeFactory utopiaTimeFactory, final EventBus eventBus, final TimerManager timerManager) {
        this.utopiaTimeFactory = utopiaTimeFactory;
        this.eventBus = eventBus;
        this.timerManager = timerManager;
    }

    @Subscribe
    public void onStartup(final StartupEvent startupEvent) {
        scheduleNextPreTick();
    }

    private UtopiaTime scheduleNextPreTick() {
        UtopiaTime utopiaTime = utopiaTimeFactory.newUtopiaTime(System.currentTimeMillis());
        UtopiaTime nextTick = utopiaTime.increment(1);

        if (utopiaTime.getTime() == lastPreTickEvent) { //System time has been set back, so we're a bit early
            utopiaTime = nextTick;
            nextTick = nextTick.increment(1);
        }
        lastPreTickEvent = utopiaTime.getTime();

        timerManager.schedule(new Timer(PreTickEvent.class, lastPreTickId++, this), nextTick.getTime() - System.currentTimeMillis() - 300000,
                TimeUnit.MILLISECONDS);

        return utopiaTime;
    }

    @Override
    public void register(final long itemId) {
        UtopiaTime thisTick = scheduleNextPreTick();
        eventBus.post(new PreTickEvent(thisTick));
    }
}
