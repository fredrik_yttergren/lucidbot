package listeners;


import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.runtime.ThreadingManager;
import database.CommonEntitiesAccess;

import database.daos.ProvinceDAO;
import database.models.Province;
import database.models.SpellType;
import events.MysticVortexEvent;
import spi.events.EventListener;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;

import com.google.inject.Provider;
import lombok.extern.log4j.Log4j;
import com.google.common.eventbus.Subscribe;

import static api.database.transactions.Transactions.inTransaction;

/**
 * Created by Kyle on 7/4/2017.
 */

@Log4j
class MysticVortexListener implements EventListener {
    private final CommonEntitiesAccess commonEntitiesAccess;
    private final ConcurrentMap<Pattern, SpellType> spellPatternsMap = new ConcurrentHashMap<>();
    private final ThreadingManager threadingManager;
    private final Provider<ProvinceDAO> provinceDAOProvider;

    @Inject
    MysticVortexListener(final CommonEntitiesAccess commonEntitiesAccess, final ThreadingManager threadingManager,
                         final Provider<ProvinceDAO> provinceDAOProvider) {
        this.commonEntitiesAccess = commonEntitiesAccess;
        this.threadingManager = threadingManager;
        this.provinceDAOProvider = provinceDAOProvider;

        for (SpellType spellType : commonEntitiesAccess.getAllSpellTypes()) {
            if (spellType.getCastRegex() != null) spellPatternsMap.put(Pattern.compile(spellType.getName()), spellType);
        }
    }

    @Subscribe
    public void onMysticVortexEvent(final MysticVortexEvent event) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventBus) {
                        registerMysticVortex(event.getProvName(), event.getSpells(), event.getResult());
                    }
                });
            }
        };
        threadingManager.execute(runnable);
    }

    private String registerMysticVortex(final String provName, final String spells, final int result) {
        ProvinceDAO provinceDAO = provinceDAOProvider.get();
        Province prov = provinceDAO.getProvince(provName);
        for (Map.Entry<Pattern, SpellType> entry : spellPatternsMap.entrySet()) {
            Matcher matcher = entry.getKey().matcher(spells);
            if (matcher.find()) {
                prov.removeDurationSpell(entry.getValue());
            }
        }
        return "Registered Mystic Vortex removing " + String.valueOf(result) + " spells";
    }

}

