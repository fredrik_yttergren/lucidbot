package listeners;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.runtime.ThreadingManager;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.NotificationDAO;
import database.models.Notification;
import database.models.NotificationType;
import events.SelfIntelPostedEvent;
import lombok.extern.log4j.Log4j;
import spi.events.EventListener;
import tools.communication.NotificationDeliverer;

import javax.inject.Inject;

import static api.database.transactions.Transactions.inTransaction;

@Log4j
class SelfIntelListener implements EventListener {
    private final Provider<BotUserDAO> userDAOProvider;
    private final Provider<NotificationDAO> notificationDAOProvider;
    private final Provider<NotificationDeliverer> delivererProvider;
    private final ThreadingManager threadingManager;

    @Inject
    SelfIntelListener(final Provider<BotUserDAO> userDAOProvider,
                      final Provider<NotificationDAO> notificationDAOProvider,
                      final Provider<NotificationDeliverer> delivererProvider,
                      final ThreadingManager threadingManager) {
        this.userDAOProvider = userDAOProvider;
        this.notificationDAOProvider = notificationDAOProvider;
        this.delivererProvider = delivererProvider;
        this.threadingManager = threadingManager;
    }

    @Subscribe
    public void onSelfIntelPosted(final SelfIntelPostedEvent event) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventPoster) {
                        try {
                            BotUser user = userDAOProvider.get().getUser(event.getUserId());
                            if (user != null) {
                                NotificationDeliverer notificationDeliverer = delivererProvider.get();
                                for (Notification notification : notificationDAOProvider.get().getNotifications(user, NotificationType.SELF_INTEL_POSTED)) {
                                    notification.getMethod().deliver(notificationDeliverer, notification.getUser(), "Self intel posted",
                                                                     "The self " + event.getIntelType() + " you posted was received successfully by the bot");
                                }
                            }
                        } catch (Exception e) {
                            log.error("", e);
                        }
                    }
                });
            }
        };
        threadingManager.execute(runnable);
    }
}
