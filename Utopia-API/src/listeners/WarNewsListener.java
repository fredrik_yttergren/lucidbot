/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package listeners;

import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.runtime.ThreadingManager;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.EventDAO;
import database.daos.NotificationDAO;
import database.models.Event;
import database.models.Notification;
import database.models.NotificationType;
import events.EventAddedEvent;
import events.WarNewsEvent;
import lombok.extern.log4j.Log4j;
import spi.events.EventListener;
import tools.BindingsManager;
import tools.communication.NotificationDeliverer;
import api.settings.PropertiesCollection;

import javax.inject.Inject;

import java.util.Date;

import static api.database.transactions.Transactions.inTransaction;

import static api.settings.PropertiesConfig.AUTO_EOWCF_EVENT;
import static api.settings.PropertiesConfig.AUTO_EOWCF_DURATION;

@Log4j
class WarNewsListener implements EventListener {
    private final BindingsManager bindingsManager;
    private final Provider<NotificationDAO> notificationDAOProvider;
    private final Provider<NotificationDeliverer> delivererProvider;
    private final ThreadingManager threadingManager;
    private final EventDAO eventDAO;
    private final PropertiesCollection properties;

    @Inject
    WarNewsListener(final Provider<NotificationDAO> notificationDAOProvider,
                    final BindingsManager bindingsManager,
                    final Provider<NotificationDeliverer> delivererProvider,
                    final ThreadingManager threadingManager,
                    final EventDAO eventDAO,
                    final PropertiesCollection properties) {
        this.notificationDAOProvider = notificationDAOProvider;
        this.bindingsManager = bindingsManager;
        this.delivererProvider = delivererProvider;
        this.threadingManager = threadingManager;
        this.eventDAO = eventDAO;
        this.properties = properties;
    }


    @Subscribe
    public void onWarNewsEvent(final WarNewsEvent event) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventPoster) {
                        try {
                            NotificationDeliverer notificationDeliverer = delivererProvider.get();
                            if (event.getNewsType().matches("(Incoming|Outgoing) war declared")) {
                                for (Notification notification : notificationDAOProvider.get().getNotifications(NotificationType.WAR_DECLARED)) {
                                    notification.getMethod()
                                            .deliver(notificationDeliverer, notification.getUser(), event.getNewsType(),
                                                    event.getNewsType());
                                }
                            }
                            if (event.getNewsType().matches("(Incoming|Outgoing) withdrawal")) {
                                if (properties.getBoolean(AUTO_EOWCF_EVENT)) {
                                    int duration = properties.getInteger(AUTO_EOWCF_DURATION) + 1;
                                    Event withdrawEvent = new Event(Event.EventType.EVENT, "End of EoWCF", new Date(event.getDate().getTime() + 3600000 * duration));
                                    eventDAO.save(withdrawEvent);
                                    delayedEventPoster.enqueue(new EventAddedEvent(withdrawEvent.getId(), null));
                                }
                                for (Notification notification : notificationDAOProvider.get().getNotifications(NotificationType.WAR_ENDED)) {
                                    notification.getMethod()
                                            .deliver(notificationDeliverer, notification.getUser(), event.getNewsType(),
                                                    event.getNewsType());
                                }
                            }
                        } catch (Exception e) {
                            log.error("", e);
                        }
                    }
                });
            }
        };
        threadingManager.execute(runnable);
    }
}