/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package listeners;

import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.irc.communication.IRCAccess;
import api.runtime.ThreadingManager;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.KingdomRitualProjectDAO;
import database.daos.NotificationDAO;
import database.models.*;
import events.KingdomRitualActionEvent;
import events.KingdomRitualActionRegisteredEvent;
import events.KingdomRitualNewsEvent;
import events.KingdomRitualProjectUpdateEvent;
import lombok.extern.log4j.Log4j;
import org.hibernate.HibernateException;
import spi.events.EventListener;
import tools.communication.NotificationDeliverer;

import javax.inject.Inject;
import java.util.Date;
import java.util.Set;

import static api.database.transactions.Transactions.inTransaction;
import static api.tools.text.StringUtil.prettifyEnumName;

/**
 * Manages kingdom ritual related information
 */
@Log4j
class NewKingdomRitualActionsListener implements EventListener {
    private final Provider<KingdomRitualProjectDAO> kingdomRitualProjectDAOProvider;
    private final Provider<BotUserDAO> botUserDAOProvider;
    private final Provider<NotificationDAO> notificationDAOProvider;
    private final Provider<NotificationDeliverer> delivererProvider;
    private final IRCAccess ircAccess;
    private final ThreadingManager threadingManager;
    private final EventBus eventBus;

    @Inject
    NewKingdomRitualActionsListener(final Provider<KingdomRitualProjectDAO> kingdomRitualProjectDAOProvider,
                                    final Provider<BotUserDAO> botUserDAOProvider,
                                    final Provider<NotificationDAO> notificationDAOProvider,
                                    final Provider<NotificationDeliverer> delivererProvider,
                                    final IRCAccess ircAccess,
                                    final ThreadingManager threadingManager,
                                    final EventBus eventBus) {
        this.kingdomRitualProjectDAOProvider = kingdomRitualProjectDAOProvider;
        this.botUserDAOProvider = botUserDAOProvider;
        this.notificationDAOProvider = notificationDAOProvider;
        this.delivererProvider = delivererProvider;
        this.ircAccess = ircAccess;
        this.threadingManager = threadingManager;
        this.eventBus = eventBus;
    }

    @Subscribe
    public void onKingdomRitualActionEvent(final KingdomRitualActionEvent event) {
        final BotUser botUser = botUserDAOProvider.get().getUser(event.getUserMainNick());
        if (botUser == null) {
            NewKingdomRitualActionsListener.log.error("Could not identify user");
            return;
        }

        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventPoster) {
                        try {
                            BotUser user = botUserDAOProvider.get().getUser(botUser.getId());
                            String reply = registerKingdomRitualAction(event.getProjectType(), user, event.getContribution());

                            if (reply != null && event.getContext() != null) {
                                ircAccess.sendNoticeOrPM(event.getContext(), reply);
                            }
                        } catch (Exception e) {
                            NewKingdomRitualActionsListener.log.error("Kingdom Ritual action event could not be handled", e);
                        }
                    }
                });
            }
        });
    }

    private String registerKingdomRitualAction(final KingdomRitualProjectType type, final BotUser user, final int contribution) {
        try {
            KingdomRitualProjectDAO dao = kingdomRitualProjectDAOProvider.get();
            KingdomRitualProject project = dao.getProjectOfType(type);
            if (project == null || project.getStatus() <= 0) {
                return null;
            }

            int actualContribution = Math.min(contribution, project.getStatus());
            if (actualContribution == 0) {
                return "This kingdom ritual is already finished";
            }
            registerKingdomRitualStats("Kingdom Ritual " + prettifyEnumName(project.getType()), actualContribution, user);

            Set<KingdomRitualAction> actions = project.getActions();
            Date now = new Date();
            for (KingdomRitualAction action : actions) {
                if (action.getUser().equals(user)) {
                    action.setContribution(action.getContribution() + actualContribution);
                    project.setStatus(Math.max(0, project.getStatus() - contribution));
                    project.setUpdated(now);
                    action.setUpdated(now);
                    eventBus.post(new KingdomRitualActionRegisteredEvent(user.getMainNick(), type, contribution));
                    return "Contribution saved";
                }
            }

            actions.add(new KingdomRitualAction(user, actualContribution, now, project));
            project.setStatus(Math.max(0, project.getStatus() - contribution));
            project.setUpdated(now);
            eventBus.post(new KingdomRitualActionRegisteredEvent(user.getMainNick(), type, contribution));
            return "Contribution saved";
        } catch (HibernateException e) {
            NewKingdomRitualActionsListener.log.error("", e);
        }
        return "Failed to save kingdom ritual actions";
    }

    private static void registerKingdomRitualStats(final String statName, final int statValue, final BotUser user) {
        try {
            if (user != null) {
                user.incrementStat(statName, statValue);
            }
        } catch (HibernateException e) {
            NewKingdomRitualActionsListener.log.error("", e);
        }
    }

    @Subscribe
    public void onKingdomRitualProjectUpdateEvent(final KingdomRitualProjectUpdateEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventPoster) {
                        try {
                            String reply = registerKingdomRitualProjectStatus(event.getType(), event.getStatus());
                            if (event.getContext() != null) {
                                ircAccess.sendNoticeOrPM(event.getContext(), reply);
                            }
                        } catch (Exception e) {
                            NewKingdomRitualActionsListener.log.error("Kingdom Ritual update could not be handled", e);
                        }
                    }
                });
            }
        });
    }

    private String registerKingdomRitualProjectStatus(final KingdomRitualProjectType type, final int status) {
        try {
            KingdomRitualProjectDAO KingdomRitualProjectDAO = kingdomRitualProjectDAOProvider.get();
            KingdomRitualProject project = KingdomRitualProjectDAO.getProjectOfType(type);
            if (project == null || project.getStatus() == 0) {
                project = new KingdomRitualProject(type, status);
                KingdomRitualProjectDAO.save(project);
                return "New kingdom ritual project created";
            } else {
                project.setStatus(status);
                project.setUpdated(new Date());
                return "Kingdom Ritual project status updated";
            }
        } catch (HibernateException e) {
            NewKingdomRitualActionsListener.log.error("Kingdom Ritual project status could not be updated", e);
        }
        return "Failed to update";
    }

    @Subscribe
    public void onKingdomRitualNewsEvent(final KingdomRitualNewsEvent event) {
        threadingManager.execute(new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventPoster) {
                        try {
                            String reply = registerKingdomRitualNewsEvent(event.getDate(), event.getType(), event.getStatus());
                            NotificationDeliverer notificationDeliverer = delivererProvider.get();
                            if (event.getNewsType() == "Kingdom ritual expired") {
                                for (Notification notification : notificationDAOProvider.get().getNotifications(NotificationType.KINGDOM_RITUAL_EXPIRED)) {
                                    notification.getMethod()
                                            .deliver(notificationDeliverer, notification.getUser(), "Kingdom Ritual expired",
                                                    "Kingdom ritual was expired as of " + event.getDate());
                                }
                            }
                            if (event.getNewsType() == "Kingdom ritual completed") {
                                for (Notification notification : notificationDAOProvider.get().getNotifications(NotificationType.KINGDOM_RITUAL_COMPLETED)) {
                                    notification.getMethod()
                                            .deliver(notificationDeliverer, notification.getUser(), "Kingdom Ritual completed",
                                                    "Kingdom Ritual has begun ravaging our kingdom as of " + event.getDate());
                                }
                            }
                        } catch (Exception e) {
                            NewKingdomRitualActionsListener.log.error("KingdomRitual news event could not be handled", e);
                        }
                    }
                });
            }
        });
    }

    private String registerKingdomRitualNewsEvent(final Date date, final KingdomRitualProjectType type, final int status) {
        try {
            KingdomRitualProjectDAO KingdomRitualProjectDAO = kingdomRitualProjectDAOProvider.get();
            KingdomRitualProject project = KingdomRitualProjectDAO.getProjectOfType(type);
            if (project != null && date.after(project.getCreated()) && project.getStatus() > 0) {
                project.setStatus(status);
                project.setUpdated(new Date());
                return "Kingdom Ritual project closed out.  Received news item indicating it has been completed/cancelled.";
            }
        }
        catch (HibernateException e) {
            NewKingdomRitualActionsListener.log.error("Kingdom ritual news event could not be handled", e);
        }
        return "Failed to action news update";
    }

}
