package listeners;


import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.runtime.ThreadingManager;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.CommonEntitiesAccess;
import database.daos.ProvinceDAO;
import database.models.Province;
import database.models.SpellType;
import events.AbolishRitualEvent;
import lombok.extern.log4j.Log4j;
import spi.events.EventListener;

import javax.inject.Inject;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static api.database.transactions.Transactions.inTransaction;

/**
 * Created by Kyle on 7/4/2017.
 */

@Log4j
class AbolishRitualListener implements EventListener {
    private final ThreadingManager threadingManager;
    private final Provider<ProvinceDAO> provinceDAOProvider;

    @Inject
    AbolishRitualListener(final ThreadingManager threadingManager,
                          final Provider<ProvinceDAO> provinceDAOProvider) {
        this.threadingManager = threadingManager;
        this.provinceDAOProvider = provinceDAOProvider;

    }

    @Subscribe
    public void onAbolishRitualEvent(final AbolishRitualEvent event) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                inTransaction(new SimpleTransactionTask() {
                    @Override
                    public void run(final DelayedEventPoster delayedEventBus) {
                        registerAbolishRitual(event.getProvName(), event.getDestroyed(), event.getAbolishRitualsLeft());
                    }
                });
            }
        };
        threadingManager.execute(runnable);
    }

    private String registerAbolishRitual(final String provName, final int destroyed, final int abolishRitualsLeft) {
        ProvinceDAO provinceDAO = provinceDAOProvider.get();
        Province prov = provinceDAO.getProvince(provName);
        prov.setAbolishRitualsLeft(abolishRitualsLeft);
        prov.getKingdom().setKingdomRitualDestroyed(destroyed);

        return "Registered Abolish Ritual: " + destroyed + "%" + " Destroyed | " + abolishRitualsLeft + " ARs remaining on target";
    }

}

