/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package announcements;

import api.database.daos.BotInstanceSettingsDAO;
import api.database.models.ChannelType;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.irc.IRCEntityManager;
import api.irc.communication.IRCAccess;
import api.templates.TemplateManager;
import api.tools.collections.MapFactory;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.TickChannelMessageDAO;
import database.daos.AidDAO;
import database.models.Aid;
import database.models.AidImportanceType;
import database.models.TickChannelMessage;
import lombok.extern.log4j.Log4j;
import events.TickEvent;
import spi.events.EventListener;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import static api.database.transactions.Transactions.inTransaction;

@Log4j
@ParametersAreNonnullByDefault
public class AidRequestAnnouncer extends AbstractAnnouncer implements EventListener {
    private final TickChannelMessageDAO channelMessageDAO;
    private final Provider<AidDAO> aidDAOProvider;

    @Inject
    public AidRequestAnnouncer(final TemplateManager templateManager,
                         final IRCEntityManager ircEntityManager,
                         final IRCAccess ircAccess, final Provider<BotInstanceSettingsDAO> botInstanceSettingsDAOProvider,
                         final TickChannelMessageDAO channelMessageDAO,
                         final Provider<AidDAO> aidDAOProvider) {
        super(templateManager, ircEntityManager, ircAccess, botInstanceSettingsDAOProvider);
        this.channelMessageDAO = channelMessageDAO;
        this.aidDAOProvider = aidDAOProvider;
    }


    @Subscribe
    public void onTick(final TickEvent event) {
        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventBus) {
                try {
                    AidDAO aidDAO = aidDAOProvider.get();
                    Map<String, String[]> output = compileTemplateOutput(MapFactory.newMapWithNamedObjects("utodate", event.getUtoDate()), "announcement-aid-request");
                    List<Aid> requests = new ArrayList<>();
                    List<Aid> offers = new ArrayList<>();
                    for (Aid aid : aidDAO.getAllAid()) {
                        if (aid.getImportanceType() == AidImportanceType.OFFERING_AID) offers.add(aid);
                        else requests.add(aid);
                    }
                    if (!requests.isEmpty()) {
                        announce(ChannelType.PRIVATE, output);
                    }
                } catch (Exception e) {
                    log.error("Tick event to announce aid requests could not be handled", e);
                }
            }
        });
    }
}
