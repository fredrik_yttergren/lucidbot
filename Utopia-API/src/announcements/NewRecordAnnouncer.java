/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package announcements;

import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import api.database.daos.BotInstanceSettingsDAO;
import api.database.models.ChannelType;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.irc.IRCEntityManager;
import api.irc.communication.IRCAccess;
import api.templates.TemplateManager;
import api.tools.collections.MapFactory;
import database.daos.AttackDAO;
import database.daos.RecordDAO;
import database.models.Attack;
import database.models.Record;
import events.AttackAddedEvent;
import events.NewRecordEvent;
import lombok.extern.log4j.Log4j;
import spi.events.EventListener;

import java.util.Map;

import static api.database.transactions.Transactions.inTransaction;

@Log4j
@ParametersAreNonnullByDefault
public class NewRecordAnnouncer extends AbstractAnnouncer implements EventListener {

    private final Provider<RecordDAO> recordDAOProvider;

    @Inject
    public NewRecordAnnouncer(final TemplateManager templateManager,
                                final IRCEntityManager ircEntityManager,
                                final IRCAccess ircAccess,
                                final Provider<BotInstanceSettingsDAO> botInstanceSettingsDAOProvider,
                                final Provider<RecordDAO> recordDAOProvider) {
        super(templateManager, ircEntityManager, ircAccess, botInstanceSettingsDAOProvider);
        this.recordDAOProvider = recordDAOProvider;
    }

    @Subscribe
    public void onNewRecord(final NewRecordEvent event) {
        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventBus) {
                try {
                    Record record = recordDAOProvider.get().getRecordById(event.getRecordId());

                    if (record != null) {
                        Map<String, String[]> output =
                                compileTemplateOutput(MapFactory.newMapWithNamedObjects("record", record),
                                        "announcement-new-record-added");
                        announce(ChannelType.PRIVATE, output);
                    } else {
                        log.warn(
                                "Record with id: " + event.getRecordId()
                                        + " was not in the database, so it could not be announced");
                    }
                } catch (Exception e) {
                    NewRecordAnnouncer.log.error("", e);
                }
            }
        });
    }
}
