/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package announcements;

import api.database.daos.BotInstanceSettingsDAO;
import api.database.models.ChannelType;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.irc.IRCEntityManager;
import api.irc.communication.IRCAccess;
import api.settings.PropertiesCollection;
import api.templates.TemplateManager;
import api.tools.collections.MapFactory;
import api.tools.collections.Pair;
import api.tools.time.DateUtil;
import com.google.common.eventbus.Subscribe;
import com.google.inject.Provider;
import database.daos.ArmyDAO;
import database.daos.NotificationDAO;
import database.daos.TickChannelMessageDAO;
import database.models.*;
import events.PreTickEvent;
import events.TickEvent;
import spi.events.EventListener;
import tools.UtopiaPropertiesConfig;
import tools.calculators.IncomeCalculator;
import tools.communication.NotificationDeliverer;

import lombok.extern.log4j.Log4j;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import java.util.*;

import static api.database.transactions.Transactions.inTransaction;

@Log4j
@ParametersAreNonnullByDefault
public class NeedResourcesAnnouncer extends AbstractAnnouncer implements EventListener {
    private final PropertiesCollection properties;
    private final TickChannelMessageDAO channelMessageDAO;
    private final ArmyDAO armyDAO;
    private final Provider<NotificationDAO> notificationDAOProvider;
    private final Provider<NotificationDeliverer> notificationDelivererProvider;
    private final IncomeCalculator incomeCalculator;

    @Inject
    public NeedResourcesAnnouncer(final TemplateManager templateManager,
                                  final IRCEntityManager ircEntityManager,
                                  final IRCAccess ircAccess,
                                  final Provider<BotInstanceSettingsDAO> botInstanceSettingsDAOProvider,
                                  final TickChannelMessageDAO channelMessageDAO,
                                  final ArmyDAO armyDAO,
                                  final PropertiesCollection properties,
                                  final Provider<NotificationDAO> notificationDAOProvider,
                                  final Provider<NotificationDeliverer> notificationDelivererProvider,
                                  final IncomeCalculator incomeCalculator) {
        super(templateManager, ircEntityManager, ircAccess, botInstanceSettingsDAOProvider);
        this.channelMessageDAO = channelMessageDAO;
        this.armyDAO = armyDAO;
        this.properties = properties;
        this.notificationDAOProvider = notificationDAOProvider;
        this.notificationDelivererProvider = notificationDelivererProvider;
        this.incomeCalculator = incomeCalculator;
    }

    private boolean checkOutOfMoney(final Province province) {
        StateCouncil sc = province.getStateCouncil();
        if (sc != null) {
            return (province.getMoney() + (sc.getNetIncome() * 3) <= 0);
        }
        return province.getMoney() + incomeCalculator.calcIncome(province) * 3 <= 0;
    }

    @Subscribe
    public void onPreTick(final PreTickEvent event) {
        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventBus) {
                String selfKD = properties.get(UtopiaPropertiesConfig.INTRA_KD_LOC);
                List<Army> armies = armyDAO.getAllArmiesOutForKD(selfKD);
                SortedMap<String, Pair<Province, Double>> needsResources = new TreeMap<>();

                long currentTime = System.currentTimeMillis();

                if (armies == null) return;

                for (Army army : armies) {
                    final Province province = army.getProvince();
                    if (army.getReturningDate().getTime() - currentTime <= 7200000 && checkOutOfMoney(province)) {
                        final Double hoursUntilArmyReturn = (double) (army.getReturningDate().getTime() - currentTime) / 3600000;

                        if (needsResources.get(province.getName()) != null) {
                            final Double hoursUntilExistingArmyReturn = needsResources.get(province.getName()).getRight();
                            if (hoursUntilExistingArmyReturn < hoursUntilArmyReturn) {
                                needsResources.put(province.getName(), new Pair<>(province, hoursUntilArmyReturn));
                            }
                        } else needsResources.put(province.getName(), new Pair<>(province, hoursUntilArmyReturn));
                    }
                }

                NotificationDeliverer notificationDeliverer = notificationDelivererProvider.get();
                for (Notification notification : notificationDAOProvider.get().getNotifications(NotificationType.RESOURCES_NEEDED)) {
                    for (Map.Entry<String, Pair<Province, Double>> resource : needsResources.entrySet()) {
                        final Province province = resource.getValue().getLeft();
                        final String returningTime = String.format(resource.getValue().getRight().toString(), "%.2f");
                        notification.getMethod()
                                .deliver(notificationDeliverer, notification.getUser(), "Provinces Need Resources",
                                        "WARNING! " + province.getProvinceOwner().getMainNick() + " has armies returning in " + returningTime + " hours and they have 0 gc.  " +
                                                "They need " + province.getWages() + " gc");
                    }
                }
                Map<String, String[]> output = compileTemplateOutput(MapFactory.newMapWithNamedObjects("provinces", needsResources), "announcement-needs-resources");
                announce(ChannelType.PRIVATE, output);
            }
        });
    }

}
