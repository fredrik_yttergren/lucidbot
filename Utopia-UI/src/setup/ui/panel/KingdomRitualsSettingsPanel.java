/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package setup.ui.panel;

import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.tools.collections.ListUtil;
import com.google.inject.Provider;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Validator;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.ui.*;
import database.daos.BonusDAO;
import database.daos.KingdomRitualDAO;
import database.models.Bonus;
import database.models.KingdomRitual;
import setup.tools.KingdomRituals;
import setup.tools.VaadinUtil;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static api.database.transactions.Transactions.inTransaction;
import static setup.tools.VaadinUtil.validate;

public class KingdomRitualsSettingsPanel extends VerticalLayout {
    private static final String POPUP_HEIGHT = "450px";
    private static final String POPUP_WIDTH = "400px";

    private final Provider<KingdomRitualDAO> KingdomRitualDAOProvider;

    private final Label description;
    private final Button addButton;
    private final Table kingdomRitualTable;
    private final Button loadDefaultsButton;

    private final Map<Object, KingdomRitual> kingdomRitualMap = new HashMap<>();
    private final Map<String, Bonus> bonusMap = new HashMap<>();

    @Inject
    public KingdomRitualsSettingsPanel(final Provider<KingdomRitualDAO> KingdomRitualDAOProvider, final Provider<BonusDAO> bonusDAOProvider,
                                       final KingdomRituals kingdomRitualsSetup) {
        this.KingdomRitualDAOProvider = KingdomRitualDAOProvider;

        setSpacing(true);
        setMargin(true);

        loadBonuses(bonusDAOProvider);

        description = new Label("Add and save kingdom rituals");

        addButton = new Button("Add Kingdom Ritual", new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                getWindow().addWindow(VaadinUtil.createPopupWindow(new EditPopupContent(null), POPUP_HEIGHT, POPUP_WIDTH));
            }
        });

        loadDefaultsButton = new Button("Load defaults", new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                kingdomRitualsSetup.loadIntoDatabase();
                loadBonuses(bonusDAOProvider);
                kingdomRitualTable.setContainerDataSource(getKingdomRitualContainer());
            }
        });

        kingdomRitualTable = new Table();
        kingdomRitualTable.setContainerDataSource(getKingdomRitualContainer());
        kingdomRitualTable.setColumnHeaders(new String[]{"Name", "Bonuses", "Edit", "Delete"});
        kingdomRitualTable.setHeight("100%");
        kingdomRitualTable.setWidth("650px");
        kingdomRitualTable.setColumnExpandRatio("name", 0.2f);
        kingdomRitualTable.setColumnExpandRatio("bonuses", 0.6f);

        addComponent(description);
        addComponent(loadDefaultsButton);
        addComponent(addButton);
        addComponent(kingdomRitualTable);
    }

    private void loadBonuses(final Provider<BonusDAO> bonusDAOProvider) {
        bonusMap.clear();
        for (Bonus bonus : bonusDAOProvider.get().getAllBonuses()) {
            bonusMap.put(bonus.getName(), bonus);
        }
    }

    private IndexedContainer getKingdomRitualContainer() {
        final IndexedContainer container = new IndexedContainer();

        container.addContainerProperty("name", String.class, null);
        container.addContainerProperty("bonuses", VerticalLayout.class, null);
        container.addContainerProperty("edit", Button.class, null);
        container.addContainerProperty("delete", Button.class, null);

        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventPoster) {
                for (KingdomRitual kingdomRitual : KingdomRitualDAOProvider.get().getAllKingdomRituals()) {
                    addKingdomRitual(kingdomRitual, container);
                }
            }
        });

        return container;
    }

    private void addKingdomRitual(final KingdomRitual kingdomRitual, final Container container) {
        final Object itemId = container.addItem();
        kingdomRitualMap.put(itemId, kingdomRitual);
        final Item item = container.getItem(itemId);
        item.getItemProperty("name").setValue(kingdomRitual.getName());
        item.getItemProperty("bonuses").setValue(createBonusList(kingdomRitual));
        item.getItemProperty("edit").setValue(new Button("Edit", new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                getWindow().addWindow(VaadinUtil.createPopupWindow(new EditPopupContent(itemId), POPUP_HEIGHT, POPUP_WIDTH));
            }
        }));
        item.getItemProperty("delete").setValue(new Button("Delete", new Button.ClickListener() {
            @Override
            public void buttonClick(final Button.ClickEvent event) {
                try {
                    KingdomRitual remove = kingdomRitualMap.get(itemId);
                    if (remove.getId() != null) {
                        KingdomRitualDAOProvider.get().delete(remove);
                        kingdomRitualMap.remove(itemId);
                        container.removeItem(itemId);
                    }
                } catch (final Exception e) {
                    getWindow().showNotification(e.getMessage(), Window.Notification.TYPE_ERROR_MESSAGE);
                }
            }
        }));
    }

    private static VerticalLayout createBonusList(final KingdomRitual kingdomRitual) {
        VerticalLayout bonusList = new VerticalLayout();
        for (Bonus bonus : kingdomRitual.getBonuses()) {
            bonusList.addComponent(
                    new Label(bonus.getName() + " (Value: " + bonus.getBonusValue() + ", Type: " + bonus.getType().getName() + ')'));
        }
        return bonusList;
    }

    private class EditPopupContent extends VerticalLayout {
        private final TextField nameField;
        private final TwinColSelect bonusesSelect;
        private final Button saveButton;
        private final Button cancelButton;

        private EditPopupContent(final Object itemId) {
            setSizeUndefined();
            setSpacing(true);
            setMargin(true);
            setWidth("100%");

            KingdomRitual kingdomRitual = null;
            if (itemId != null) {
                kingdomRitual = kingdomRitualMap.get(itemId);
            }

            nameField = new TextField("Name");
            nameField.setRequired(true);
            if (kingdomRitual != null) nameField.setValue(kingdomRitual.getName());

            bonusesSelect = new TwinColSelect("Bonuses", bonusMap.keySet());
            bonusesSelect.setRows(10);
            bonusesSelect.setNullSelectionAllowed(true);
            bonusesSelect.setMultiSelect(true);
            bonusesSelect.setWidth("100%");
            if (kingdomRitual != null) bonusesSelect.setValue(ListUtil.getNames(kingdomRitual.getBonuses()));

            saveButton = new Button("Save", new Button.ClickListener() {
                @Override
                public void buttonClick(final Button.ClickEvent event) {
                    try {
                        String name = validate(nameField, String.class);
                        Set<?> bonusIds = (Set<?>) bonusesSelect.getValue();
                        Set<Bonus> bonuses = new HashSet<>(bonusIds.size());
                        for (Object bonusId : bonusIds) {
                            bonuses.add(bonusMap.get(bonusId));
                        }

                        KingdomRitual kingdomRitual;
                        if (itemId == null) {
                            kingdomRitual = new KingdomRitual(name, bonuses);
                            KingdomRitualDAOProvider.get().save(kingdomRitual);
                            addKingdomRitual(kingdomRitual, kingdomRitualTable.getContainerDataSource());
                        } else {
                            kingdomRitual = kingdomRitualMap.get(itemId);
                            kingdomRitual.setName(name);
                            kingdomRitual.setBonuses(bonuses);
                            Container container = kingdomRitualTable.getContainerDataSource();
                            Item item = container.getItem(itemId);
                            item.getItemProperty("name").setValue(name);
                            item.getItemProperty("bonuses").setValue(createBonusList(kingdomRitual));
                            KingdomRitualDAOProvider.get().save(kingdomRitual);
                        }
                        removePopup(event);
                    } catch (Validator.InvalidValueException e) {
                        getWindow().showNotification(e.getMessage(), Window.Notification.TYPE_ERROR_MESSAGE);
                    }
                }
            });

            cancelButton = new Button("Cancel", new Button.ClickListener() {
                @Override
                public void buttonClick(final Button.ClickEvent event) {
                    removePopup(event);
                }
            });

            addComponent(nameField);
            addComponent(bonusesSelect);
            HorizontalLayout buttons = new HorizontalLayout();
            buttons.addComponent(saveButton);
            buttons.addComponent(cancelButton);
            addComponent(buttons);
        }

        private void removePopup(final Button.ClickEvent event) {
            Window parent = getWindow();
            parent.getParent().removeWindow(parent);
        }
    }
}
