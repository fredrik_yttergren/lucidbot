/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package setup.tools;

import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.inject.Provider;
import database.daos.KingdomRitualDAO;
import database.daos.KingdomRitualDAO;
import database.models.*;
import spi.settings.EntitySetup;

import javax.inject.Inject;

import static api.database.transactions.Transactions.inTransaction;

public class KingdomRituals implements EntitySetup {
    public static final KingdomRitual barrier = new KingdomRitual("Barrier", Sets.newHashSet(
            new Bonus("Kingdom Ritual Gains", BonusType.GAIN, BonusApplicability.DEFENSIVELY, false, 0.1)));
    public static final KingdomRitual affluent = new KingdomRitual("Affluent", Sets.<Bonus>newHashSet());
    public static final KingdomRitual havoc = new KingdomRitual("Havoc", Sets.<Bonus>newHashSet());
    public static final KingdomRitual onslaught = new KingdomRitual("Onslaught", Sets.<Bonus>newHashSet());
    public static final KingdomRitual resilient = new KingdomRitual("Resilient", Sets.<Bonus>newHashSet());
    public static final KingdomRitual godspeed = new KingdomRitual("Godspeed", Sets.<Bonus>newHashSet());
    public static final KingdomRitual expedient = new KingdomRitual("Expedient", Sets.<Bonus>newHashSet());

    private final Provider<KingdomRitualDAO> kingdomRitualDAOProvider;

    @Inject
    public KingdomRituals(final Provider<KingdomRitualDAO> kingdomRitualDAOProvider) {
        this.kingdomRitualDAOProvider = kingdomRitualDAOProvider;
    }

    @Override
    public void loadIntoDatabase() {
        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventBus) {
                KingdomRitualDAO kingdomRitualDAO = kingdomRitualDAOProvider.get();
                if (!kingdomRitualDAO.getAllKingdomRituals().isEmpty()) return;
                kingdomRitualDAO.save(Lists.newArrayList(barrier, affluent, onslaught, havoc, resilient,
                        godspeed, expedient));
            }
        });
    }
}
