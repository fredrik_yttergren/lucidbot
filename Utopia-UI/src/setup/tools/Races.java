/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package setup.tools;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.inject.Provider;

import javax.inject.Inject;

import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import database.daos.RaceDAO;
import database.models.Bonus;
import database.models.BonusApplicability;
import database.models.BonusType;
import database.models.IntelAccuracySpecification;
import database.models.Race;
import database.models.SpellType;
import spi.settings.EntitySetup;

import static api.database.transactions.Transactions.inTransaction;

public class Races implements EntitySetup {

  public static final Race avian =
      new Race("Avian", "AV", 1, .5, "Griffins", 6, "Harpies", 6, "Drakes", 7, 3, 8, 100, 2,
               IntelAccuracySpecification.NEVER, false, null, null,
              Sets.<Bonus>newHashSet(),
              Lists.<SpellType>newArrayList());

  public static final Race darkElf =
          new Race("Dark Elf", "DE", 1, .5, "Night Rangers", 8, "Druids", 6, "Drows", 4, 8, 10.5, 5, 2,
                  IntelAccuracySpecification.NEVER, false, null, null,
                  Sets.<Bonus>newHashSet(),
                  Lists.<SpellType>newArrayList());

  public static final Race dwarf =
      new Race("Dwarf", "DW", 1, .5, "Warriors", 6, "Axemen", 6, "Berserkers", 6, 6, 8.5, 100, 2,
               IntelAccuracySpecification.NEVER, false, null, null,
               Sets.<Bonus>newHashSet(),
               Lists.<SpellType>newArrayList());

  public static final Race elf =
      new Race("Elf", "EL", 1, .5, "Rangers", 6, "Archers", 6, "Elf Lords", 7, 4, 9.5, 100, 2,
               IntelAccuracySpecification.NEVER, false, null, null,
              Sets.<Bonus>newHashSet(new Bonus("Elf WPA", BonusType.WPA, BonusApplicability.BOTH, true, .35)),
               Lists.<SpellType>newArrayList());

  public static final Race faery =
      new Race("Faery", "FA", 1, .5, "Magicians", 6, "Druids", 6, "Beastmasters", 4, 8, 10, 5, 2,
               IntelAccuracySpecification.NEVER, false, null, null,
              Sets.<Bonus>newHashSet(new Bonus("Faery WPA", BonusType.INCOME, BonusApplicability.BOTH, true, .2)),
               Lists.<SpellType>newArrayList());

  public static final Race halfling =
      new Race("Halfling", "HA", 2, 1, "Strongarms", 6, "Slingers", 6, "Brutes", 5, 7, 9, 100, 2,
               IntelAccuracySpecification.NEVER, false, null, null,
               Sets.<Bonus>newHashSet(new Bonus("Halfling TPA", BonusType.TPA, BonusApplicability.BOTH, true, .5)),
              Lists.<SpellType>newArrayList());

  public static final Race human =
      new Race("Human", "HU", 1, .5, "Swordsmen", 7, "Archers", 6, "Knights", 5, 7, 10, 100, 2,
               IntelAccuracySpecification.NEVER, false, null, null,
               Sets.<Bonus>newHashSet(),
                       Lists.<SpellType>newArrayList());

  public static final Race orc = new Race("Orc", "OR", 1, .5, "Goblins", 6, "Trolls", 6, "Ogres", 9, 2, 9, 100, 2,
                                          IntelAccuracySpecification.NEVER, false, null, null,
                                          Sets.newHashSet(new Bonus("Orc Gains", BonusType.GAIN,
                                                  BonusApplicability.OFFENSIVELY, true, 0.20),
                                                  new Bonus("Orc TPA", BonusType.TPA,
                                                          BonusApplicability.BOTH, false, .15)),
                                          Lists.<SpellType>newArrayList());

  public static final Race undead =
      new Race("Undead", "UD", 1, .5, "Skeletons", 5, "Zombies", 5, "Ghouls", 11, 3, 10.5, 100, 2,
              IntelAccuracySpecification.NEVER, false, null, null,
              Sets.<Bonus>newHashSet(),
               Lists.<SpellType>newArrayList());

  public static final Race bocan =
          new Race("Bocan", "BO", 1, .5, "Marauders", 6, "Imps", 6, "Tricksters", 5, 8, 9.5, 10, 2,
                  IntelAccuracySpecification.NEVER, false, null, null,
                  Sets.<Bonus>newHashSet(new Bonus("Bocan Honor", BonusType.HONOR, BonusApplicability.BOTH, true, .5)),
                  Lists.<SpellType>newArrayList());

  public static final Race dryad =
          new Race("Dryad", "DR", 1, .5, "Huldra", 6, "Nymph", 6, "Will O The Wisp", 10, 1, 9, 100, 3,
                  IntelAccuracySpecification.NEVER, false, null, null, Sets.<Bonus>newHashSet(),
                  Lists.<SpellType>newArrayList());

  private final Provider<RaceDAO> raceDAOProvider;

  @Inject
  public Races(final Provider<RaceDAO> raceDAOProvider) {
    this.raceDAOProvider = raceDAOProvider;
  }

  @Override
  public void loadIntoDatabase() {
    inTransaction(new SimpleTransactionTask() {
      @Override
      public void run(final DelayedEventPoster delayedEventBus) {
        RaceDAO raceDAO = raceDAOProvider.get();
        if (!raceDAO.getAllRaces().isEmpty()) {
          return;
        }
        raceDAO.save(Lists.newArrayList(avian, darkElf, dwarf, elf, faery, halfling, human, orc, undead, bocan, dryad));
      }
    });
  }
}
