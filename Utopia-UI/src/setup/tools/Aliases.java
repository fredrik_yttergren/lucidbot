package setup.tools;

import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import com.google.common.collect.Lists;
import com.google.inject.Provider;
import api.database.models.Alias;
import api.database.daos.AliasDAO;
import spi.settings.EntitySetup;

import javax.inject.Inject;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static api.database.transactions.Transactions.inTransaction;

/**
 * Created by Kyle on 4/10/2016.
 */
public class Aliases implements EntitySetup {
    public static final Alias auth = new Alias("[!.]auth", "!authenticate", "");
    public static final Alias calc = new Alias("[!.]c (.*)", "!calc {1}", "");
    public static final Alias prov = new Alias("[!.]prov(?: (.+))?", "!province {1}", "");
    public static final Alias addprov = new Alias("[!.]addprov(?: (.+))?", "!addprovince {1}", "");
    public static final Alias fb = new Alias("[!.]fb(?: (.+))?", "!fireball {1}", "");
    public static final Alias removenote = new Alias("[!.]removenote (.*)", "!removenotes {1}", "");
    public static final Alias addaid = new Alias("[!.]aid (.*)", "!addaid {1}", "");
    public static final Alias addaiduser = new Alias("[!.]aid ([a-zA-Z]+) (\\d+.)", "!addaid {2} {1}", "");
    public static final Alias edef = new Alias("[!.]edef(?: (.+))?", "!estimateddef {1}", "");
    public static final Alias onlogin = new Alias("[!.]onlogin", "!messages !orders", "");
    public static final Alias addmsg = new Alias("[!.]msg (.*)", "!message {1}", "");
    public static final Alias msg = new Alias("[!.]msgs", "!messages", "");
    public static final Alias whois = new Alias("[!.]whois (.*)", "!province {1}", "");
    public static final Alias aidlist = new Alias("[!.]aidlist", "!aid", "");
    public static final Alias check = new Alias("[!.]check(?: (.+))?", "!province {1}", "");
    public static final Alias dragons = new Alias("[!.]dragons", "!dragonprojects", "");
    public static final Alias aidremove = new Alias("[!.]aidremove(?: (.+))?", "!removeaid {1}", "");
    public static final Alias contact = new Alias("[!.]contact(?: (.+))?", "!contacts {1}", "");
    public static final Alias def = new Alias("[!.]def(?: (.+))?", "!estimateddef {1}", "");
    public static final Alias sur = new Alias("[!.]sur(?: (.+))?", "!survey {1}", "");
    public static final Alias removeprov = new Alias("[!.]removeprov(?: (.+))?", "!removeprovince {1}", "");
    public static final Alias delaid = new Alias("[!.]delaid(?: (.+))?", "!removeaid {1}", "");
    public static final Alias calcwpa = new Alias("[!.]calcwpa(?: (.+))?", "!wpacalc {1}", "");
    public static final Alias cwpa = new Alias("[!.]cwpa(?: (.+))?", "!wpacalc {1}", "");
    public static final Alias eoff = new Alias("[!.]eoff(?: (.+))?", "!estimatedoff {1}", "");
    public static final Alias moff = new Alias("[!.]moff(?: (.+))?", "!maxoff {1}", "");
    public static final Alias addsyntax = new Alias("[!.]addsyntax(?: (.+))?", "!setsyntax {1}", "");

    private final Provider<AliasDAO> aliasDAOProvider;

    @Inject
    public Aliases(final Provider<AliasDAO> aliasDAOProvider) {
        this.aliasDAOProvider = aliasDAOProvider;
    }

    @Override
    public void loadIntoDatabase() {
        final List<Alias> objects = Lists
                .newArrayList(auth, calc, prov, addprov, fb, removenote, addaid, addaiduser, edef, onlogin, addmsg, msg, whois, aidlist, check, dragons,
                        aidremove, contact, def, sur, removeprov, delaid, calcwpa, cwpa, eoff, moff, addsyntax);
        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventBus) {
                AliasDAO aliasDAO = aliasDAOProvider.get();
                for (Alias alias : aliasDAO.getAllAliases()) {
                    objects.remove(alias);
                }
                if (objects.isEmpty()) return;
                aliasDAO.save(objects);
            }
        });
    }

}
