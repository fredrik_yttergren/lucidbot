/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package setup.tools;

import api.database.GenericDAO;
import api.database.models.UserStatistic;
import api.database.transactions.SimpleTransactionTask;
import api.events.DelayedEventPoster;
import api.timers.TimerManager;
import com.google.inject.Provider;
import database.models.*;
import lombok.extern.log4j.Log4j;

import javax.inject.Inject;
import org.hibernate.criterion.Restrictions;

import static api.database.transactions.Transactions.inTransaction;

@Log4j
public class DatabaseCleaner {
    private final Provider<GenericDAO> genericDAOProvider;
    private final Provider<TimerManager> timerManagerProvider;

    @Inject
    public DatabaseCleaner(final Provider<GenericDAO> genericDAOProvider, final Provider<TimerManager> timerManagerProvider) {
        this.genericDAOProvider = genericDAOProvider;
        this.timerManagerProvider = timerManagerProvider;
    }

    public void clean() {
        inTransaction(new SimpleTransactionTask() {
            @Override
            public void run(final DelayedEventPoster delayedEventBus) {
                timerManagerProvider.get().cancelAllTimers();

                GenericDAO genericDAO = genericDAOProvider.get();

                genericDAO.deleteAll(Aid.class);
                genericDAO.deleteAll(Army.class);
                genericDAO.deleteAll(Attack.class);
                genericDAO.deleteAll(NewsItem.class);
                genericDAO.deleteAll(TargetHitter.class);
                genericDAO.deleteAll(Target.class);

                genericDAO.deleteAll(SoM.class);
                genericDAO.deleteAll(SoSEntry.class);
                genericDAO.deleteAll(SoS.class);
                genericDAO.deleteAll(SoT.class);
                genericDAO.deleteAll(SurveyEntry.class);
                genericDAO.deleteAll(Survey.class);
                genericDAO.deleteAll(StateCouncil.class);
                genericDAO.deleteAll(Military.class);

                genericDAO.deleteAll(DurationSpell.class);
                genericDAO.deleteAll(InstantSpell.class);
                genericDAO.deleteAll(DurationOp.class);
                genericDAO.deleteAll(InstantOp.class);
                genericDAO.deleteAll(UserSpellOpTarget.class);

                genericDAO.deleteAll(DragonAction.class);
                genericDAO.deleteAll(DragonProject.class);

                genericDAO.deleteAll(KingdomRitualAction.class);
                genericDAO.deleteAll(KingdomRitualProject.class);

                genericDAO.deleteAll(UserStatistic.class);
                genericDAO.deleteAll(UserCheckIn.class);
                genericDAO.deleteAll(Province.class);
                genericDAO.deleteAll(Kingdom.class);

                for (Record record : genericDAO.find(Record.class, Restrictions.eq("allTimeRecord", false))) {
                    genericDAO.delete(record);
                }
            }
        });
    }
}
