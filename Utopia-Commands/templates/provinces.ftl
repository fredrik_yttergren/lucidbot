<@ircmessage type="reply_notice">
    <#list provinces as province>
    ${OLIVE+province.name+NORMAL}<#if province.race?? && province.personality??> - ${BLUE+province.race.name} ${province.personality.name+NORMAL}<#else></#if><#if province.provinceOwner??> played by ${RED+province.provinceOwner.mainNick+NORMAL}
    <#else>

    </#if>
    </#list>
</@ircmessage>
