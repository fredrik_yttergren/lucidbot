<@ircmessage type="reply_notice">
NW/Land Range for >> ${RED+province.getName()+NORMAL} << ${OLIVE+province.getNetworth()*0.8+NORMAL} | ${OLIVE+province.getLand()*0.8+NORMAL} 80% : ${DARK_GREEN+province.getNetworth()*0.9+NORMAL} | ${DARK_GREEN+province.getLand()*0.9+NORMAL} 90% : ${DARK_GREEN+province.getNetworth()*1.1+NORMAL} | ${DARK_GREEN+province.getLand()*1.1+NORMAL} 110% : ${OLIVE+province.getNetworth()*1.2+NORMAL} | ${OLIVE+province.getLand()*1.2+NORMAL} 120%
</@ircmessage>
