<@ircmessage type="reply_notice">
<#if nubName??>
-- Nub Stats for ${nubName} --
    <#list nubhof as nub>
        <#if nub.nubName == nubName>
        id:${nub.id} ${NORMAL}Reason: ${BLUE+nub.reason+NORMAL}   By: ${OLIVE+nub.setter+NORMAL}   On: ${PURPLE+nub.setTime}
        </#if>
    </#list>
<#else>
-- Nub Hall of Fame --
    <#list nubhof?keys as nub>
        ${BLUE+nub+NORMAL} has been nub (${OLIVE+nubhof[nub]+NORMAL}) times.
    </#list>
</#if>
</@ircmessage>