<@ircmessage type="reply_notice">
                  <#if intel.province.race??><#assign race=intel.province.race.name/></#if>
                  <#if intel.province.personality??><#assign pers=intel.province.personality.name/></#if>
                  <#assign name=intel.province.name loc=intel.province.kingdom.location/>
                  <#if intel.accuracy != 100>${RED}WARNING! INTEL IS NOT 100% ACCURATE!${NORMAL} Accuracy: ${BLUE+intel.accuracy}</#if>
              >> ${OLIVE+name+" "+loc+NORMAL} << ${OLIVE+race!} ${pers!}${NORMAL} Added: ${timeUtil.compareDateToCurrent(intel.lastUpdated)} ago by o${RED+intel.savedBy+NORMAL}o
                 ${BLUE}DME: <#if intel.province.defensiveME??>${PURPLE+intel.province.defensiveME!"?"}${BLUE}<#else>?</#if> Defense at Home: ${OLIVE}<#if intel.province.estimatedCurrentDefense??>${intel.province.estimatedCurrentDefense!"?"}<#elseif intel.netDefense??>${intel.netDefense!"?"}<#else>?</#if>${BLUE} OME: <#if intel.province.offensiveME??>${PURPLE+intel.province.offensiveME!"?"}${BLUE}<#else>?</#if> Offense at Home: <#if intel.netOffense??>${OLIVE+intel.netOffense!"?"}<#else>?</#if>${BLUE}
                  <#list intel.sortedArmies as army>
                      <#if army.type.typeName = "Army Home">
                      ${BLUE}Army home: Solds: ${OLIVE+army.soldiers+BLUE} DS: ${OLIVE+army.defSpecs+BLUE} OS: ${OLIVE+army.offSpecs+BLUE} Elites: ${OLIVE+army.elites+BLUE} WH: ${OLIVE+army.warHorses}
                      <#elseif army.type.typeName = "Army Training">
                      ${BLUE}In training: DS: ${OLIVE+army.defSpecs+BLUE} OS: ${OLIVE+army.offSpecs+BLUE} Elites: ${OLIVE+army.elites+BLUE} Thieves: ${OLIVE+army.thieves}
                      <#else>
                      ${BLUE}Army # ${OLIVE+army.armyNumber+BLUE} with ${RED+army.generals+BLUE} gens returns: ${RED+timeUtil.compareDateToCurrent(army.returningDate)+BLUE} Solds: ${OLIVE+army.soldiers+BLUE} OS: ${OLIVE+army.offSpecs+BLUE} Elites: ${OLIVE+army.elites+BLUE} Land: ${OLIVE+army.landGained+BLUE} WH: ${OLIVE+army.warHorses+BLUE} Ambush: ${OLIVE+army.calculateAmbush()}
                      </#if>
                  </#list>
              <#if intel.exportLine??>${BLUE}Export: ${DARK_GREEN+intel.exportLine!""+NORMAL}<#else></#if>
              </@ircmessage>