<@ircmessage type="reply_message">
    <#if !isnub>
    ${RED+nub.nubName+NORMAL} was last a nub due to: ${BLUE+nub.reason+NORMAL}   By: ${OLIVE+nub.setter+NORMAL}   On: ${PURPLE+nub.setTime}
    <#else>
    Current Nub: ${RED}${nub.nubName}   ${NORMAL}Reason: ${BLUE}${nub.reason}   ${NORMAL}By: ${OLIVE}${nub.setter}
    </#if>
</@ircmessage>
