<@ircmessage type="reply_message">
    <#if (quotes?size > 5) >
    Found over five matching quotes, please narrow your search term - here are the top five matches:
        <#list quotes[0..4] as quote>
        [id:${quote.id} added by ${quote.addedBy} ${timeUtil.compareDateToCurrent(quote.added)} ago] -- ${quote.quote}
        </#list>
    <#else>
    Found the following Quotes:
        <#list quotes as quote>
        [id:${quote.id} added by ${quote.addedBy} ${timeUtil.compareDateToCurrent(quote.added)} ago] -- ${quote.quote}
        </#list>
    </#if>
</@ircmessage>