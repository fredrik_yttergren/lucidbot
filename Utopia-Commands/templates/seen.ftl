<@ircmessage type="reply_notice">
    <#if provs??>
        <#list provs?keys as province>
        Province: ${OLIVE+province+NORMAL} last seen ${provs[province]} ago
        </#list>
    <#elseif province??>
    Province: ${OLIVE+province.name+NORMAL} ${OLIVE+province.kingdom.location+NORMAL} last seen ${seen} ago
    <#elseif online>
    User: ${user.mainNick} is already connected with the following nicknames: ${stringUtil.merge(nicks, ", ")}
    <#else>
    User: ${user.mainNick} was last seen ${seen} ago
    </#if>
</@ircmessage>