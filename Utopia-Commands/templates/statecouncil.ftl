<@ircmessage type="reply_notice">
    <#assign name=intel.province.name loc=intel.province.kingdom.location/>
State Council for >> ${OLIVE+name+" "+loc+NORMAL} << ${OLIVE+race!} ${pers!}${NORMAL} Added: ${timeUtil.compareDateToCurrent(intel.lastUpdated)} ago by o${RED+intel.savedBy+NORMAL}o
Income: ${PURPLE+intel.income+NORMAL} Wages: ${PURPLE+intel.wages+NORMAL} Draft Cost: ${PURPLE+intel.draftCost+NORMAL} Net Income: ${PURPLE+intel.netIncome+NORMAL}
Food Grown: ${GREEN+intel.foodGrown+NORMAL} Needed: ${GREEN+intel.foodNeeded+NORMAL} Decayed: ${GREEN+intel.foodDecayed+NORMAL} Net Food: ${GREEN+intel.netFood+NORMAL}
Runes Produced: ${MAGENTA+intel.runesProduced+NORMAL} Decayed: ${MAGENTA+intel.runesDecayed+NORMAL} Net Runes: ${MAGENTA+intel.netRunes+NORMAL}
    <@compress  single_line=true>
    Peasants Born: ${BLUE+intel.peasantsBorn+NORMAL} Honor: ${BLUE+intel.honor+NORMAL} Total Pop: ${BLUE+intel.totalPopulation+NORMAL} Max Pop: ${BLUE+intel.maxPopulation+NORMAL}
    Employment: ${BLUE+intel.employment+NORMAL} Unemployed: ${BLUE+intel.unemployedPeasants+NORMAL} Unfilled Jobs: ${BLUE+intel.unfilledJobs+NORMAL}
    </@compress>
</@ircmessage>