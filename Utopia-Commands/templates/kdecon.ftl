<@ircmessage type="reply_message">
Kingdom: ${DARK_GREEN+kingdom.name} ${kingdom.location+NORMAL}
${OLIVE+kingdom.getTotalLand()+NORMAL} Acres | ${OLIVE+kingdom.getTotalNw()+NORMAL} NW | ${OLIVE+kingdom.getTpaWpaNw()+NORMAL} TPA+WPA NW | ${OLIVE+kingdom.getScienceNw()+NORMAL} Sci Nw | ${OLIVE+kingdom.getTotalMilitaryNw()+NORMAL} Military NW | ${OLIVE+kingdom.getLandNw()+NORMAL} Land NW
${MAGENTA+kingdom.getTotalPeasants()+NORMAL} Peasants | ${MAGENTA+kingdom.getTotalTrainedMilitary()+NORMAL} Trained Military | ${MAGENTA+kingdom.getTotalSoldiers()+NORMAL} Soldiers
${RED+totalIncome+NORMAL} Income | ${RED+kingdom.getTotalWages()+NORMAL} Wages
${BLUE+kingdom.getTotalGc()+NORMAL} GC | ${BLUE+kingdom.getTotalRunes()+NORMAL} Runes | ${BLUE+kingdom.getTotalFood()+NORMAL} Food
</@ircmessage>