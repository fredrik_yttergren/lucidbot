<@ircmessage type="reply_notice">
    <#if province??>
    ${type} results for ${province.name}:
    ${totalDamage} in ${totalAttempts} tries
        <#list spellsAndOps as info>
        Totals for ${BLUE+info.committer.mainNick+NORMAL} = ${info.damage} in ${info.amount} tries<#if type == "Propaganda"> of type ${info.spellOpDamageType}<#else></#if>

        </#list>
    <#else>
        <@columns underlined=true colLengths=columnLengths>
        Province\Number
        Total damage
        Total attempts
        Last Action
        </@columns>
        <#list infoList as info>
            <@columns underlined=true colLengths=columnLengths>
            ${info.province.name}
            ${info.totalDamage}
            ${info.totalAttempts}
            ${info.lastUpdated}
            </@columns>
        </#list>
    </#if>
</@ircmessage>