<@ircmessage type="reply_message">
<#if records?size gt 5>
Too many records found.  Please filter your query further.
<#else>
<#list records as record>
    ***${DARK_GREEN}<#if record.allTimeRecord == true>All Time<#else>Current Age</#if>${NORMAL} record biggest ${PURPLE+record.recordName+NORMAL} of ${RED+record.recordValue+NORMAL} set by ${OLIVE+record.userName+NORMAL} (id:${record.id})***
</#list>
</#if>
</@ircmessage>