<@ircmessage type="reply_notice">
    <#if intel.province.race??><#assign race=intel.province.race.name/></#if>
    <#if intel.province.personality??><#assign pers=intel.province.personality.name/></#if>
    <#assign name=intel.province.name loc=intel.province.kingdom.location/>
    <#if intel.accuracy != 100>${RED}WARNING! INTEL IS NOT 100% ACCURATE!${NORMAL} Accuracy: ${BLUE+intel.accuracy}</#if>
>> ${OLIVE+name+" "+loc+NORMAL} << ${OLIVE+race!} ${pers!}${NORMAL} Added: ${timeUtil.compareDateToCurrent(intel.lastUpdated)} ago by o${RED+intel.savedBy+NORMAL}o
    <@columns underlined=true colLengths=[14, 7, 13]>
        <#list intel.sciInfo?keys as key>
        ${BLUE+key}
        <#if intel.sciInfo[key].scientists??>
        ${OLIVE+intel.sciInfo[key].scientists}
        <#else>
        ${OLIVE+intel.sciInfo[key].effect}%
        </#if>
        <#if intel.sciInfo[key].unallocatedBooks gt 0>
        ${DARK_GREEN}${intel.sciInfo[key].unallocatedBooks}
        <#else>
        ${DARK_GREEN}${intel.sciInfo[key].books}
        </#if>
        </#list>
    </@columns>
${BLUE}Total Books: ${OLIVE+intel.totalBooks+BLUE} Total Unallocated: ${OLIVE+intel.getTotalUnallocatedBooks()}
${BLUE}Export: ${DARK_GREEN+intel.exportLine!""+NORMAL}
</@ircmessage>