/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.spells_ops;

import database.models.Province;
import database.models.SpellsOpsDamageInfo;

import java.util.Date;
import java.util.List;

import tools.time.UtopiaTimeFactory;
import tools.time.UtopiaTime;

public class ProvinceWithInstantSpellsOrOps {
    private final Province province;
    private int totalDamage;
    private int totalAttempts;
    private final UtopiaTime lastUpdated;

    public static <E extends SpellsOpsDamageInfo> ProvinceWithInstantSpellsOrOps create(final Province province,
                                                                                        final List<E> list,
                                                                                        final UtopiaTimeFactory utopiaTimeFactory) {
        int totalDamage = 0, totalAttempts = 0;
        Date lastUpdated = list.get(0).getLastUpdated();
        for (E info : list) {
            totalDamage += info.getDamage();
            totalAttempts += info.getAmount();
            if (info.getLastUpdated().after(lastUpdated)) {
                lastUpdated = info.getLastUpdated();
            }
        }
        return new ProvinceWithInstantSpellsOrOps(province, totalDamage, totalAttempts, utopiaTimeFactory.newUtopiaTime(lastUpdated.getTime()));
    }

    private ProvinceWithInstantSpellsOrOps(final Province province, final int totalDamage,
                                           final int totalAttempts, final UtopiaTime lastUpdated) {
        this.province = province;
        this.totalDamage = totalDamage;
        this.totalAttempts = totalAttempts;
        this.lastUpdated = lastUpdated;
    }

    public Province getProvince() {
        return province;
    }

    public int getTotalDamage() {
        return totalDamage;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public UtopiaTime getLastUpdated() { return lastUpdated; }
}
