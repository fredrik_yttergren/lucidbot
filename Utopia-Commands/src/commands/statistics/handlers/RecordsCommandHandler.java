/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.statistics.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.database.daos.NubDAO;
import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.database.models.Nub;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.RecordDAO;
import database.daos.SpellDAO;
import database.models.AttackType;
import database.models.OpType;
import database.models.Record;
import database.CommonEntitiesAccess;

import database.models.SpellType;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import java.util.*;
import javax.inject.Inject;

public class RecordsCommandHandler implements CommandHandler {
    private final RecordDAO recordDAO;
    private final BotUserDAO userDAO;
    private final CommonEntitiesAccess commonEntitiesAccess;

    @Inject
    public RecordsCommandHandler(final RecordDAO recordDAO, final BotUserDAO userDAO, final CommonEntitiesAccess commonEntitiesAccess) {
        this.recordDAO = recordDAO;
        this.userDAO = userDAO;
        this.commonEntitiesAccess = commonEntitiesAccess;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {

        try {
            if (params.isEmpty()) {
                List<Record> records = recordDAO.getCurrentAgeRecords();
                if (records.isEmpty()) {
                    return CommandResponse.errorResponse("No records yet.  You better go do something awesome.");
                }
                return CommandResponse.resultResponse("records", records);
            }

            if (params.getParameter("userOrOp").equals("alltime")) {
                List<Record> records = recordDAO.getAllTimeRecords();
                if (records.isEmpty()) {
                    return CommandResponse.errorResponse("No records yet.  You better go do something awesome.");
                }
                return CommandResponse.resultResponse("records", records);
            }

            HashMap<String, String> names = new HashMap<>();
            for (AttackType attackType : AttackType.values()) {
                names.put(attackType.getName(), attackType.getShortName());
            }
            for (SpellType spellType : commonEntitiesAccess.getAllSpellTypes()) {
                names.put(spellType.getName(), spellType.getShortName());
            }
            for (OpType optype : commonEntitiesAccess.getAllOpTypes()) {
                names.put(optype.getName(), optype.getShortName());
            }
            for (Map.Entry<String, String> entry : names.entrySet()) {
                if (params.getParameter("userOrOp").equalsIgnoreCase(entry.getKey()) || params.getParameter("userOrOp").equalsIgnoreCase(entry.getValue())) {
                    String recordName = entry.getKey();
                    List<Record> records = recordDAO.getRecordsOfType(recordName);
                    if (records.isEmpty()) {
                        return CommandResponse.errorResponse("No records for that user or op.");
                    }
                    return CommandResponse.resultResponse("records", records);
                }
            }

            BotUser botUser = userDAO.getClosestMatch(params.getParameter("userOrOp"));
            if (botUser != null) {
                List<Record> records = recordDAO.getRecordsForUser(botUser.getMainNick());
                if (records.isEmpty()) {
                    return CommandResponse.errorResponse("No records yet.  Tell that person to go do something awesome");
                }
                return CommandResponse.resultResponse("records", records);
            }

            return CommandResponse.errorResponse("Could not validate user or op specified");

        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
