package commands.statistics.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.RecordDAO;
import database.models.Record;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import java.util.Collection;
import javax.inject.Inject;

/**
 * Created by Kyle on 6/26/2016.
 */
public class RemoveRecordCommandHandler implements CommandHandler {
    private final RecordDAO recordDAO;

    @Inject
    public RemoveRecordCommandHandler(final RecordDAO recordDAO) {
        this.recordDAO = recordDAO;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Record record = recordDAO.getRecordById(params.getLongParameter("id"));
            if (record == null) return CommandResponse.errorResponse("No such record found");
            recordDAO.delete(record);
            return CommandResponse.resultResponse("id", record.getId());
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}