/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.statistics.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.database.daos.NubDAO;
import api.database.daos.BotUserDAO;
import api.database.models.Nub;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import java.util.*;
import javax.inject.Inject;

public class NubHofCommandHandler implements CommandHandler {
    private final NubDAO nubDAO;
    private final BotUserDAO userDAO;

    @Inject
    public NubHofCommandHandler(final NubDAO nubDAO, final BotUserDAO userDAO) {
        this.nubDAO = nubDAO;
        this.userDAO = userDAO;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            if (params.getParameter("nub") != null) {
                String nub = userDAO.getClosestMatch(params.getParameter("nub")).getMainNick();
                List<Nub> nubList = nubDAO.getUser(nub);
                return CommandResponse.resultResponse("nubName", nub, "nubhof", nubList);
            }

            List<Nub> nubList = nubDAO.getAllNubs();
            if (nubList.isEmpty()) { return CommandResponse.errorResponse("There are no nubs!  I find that hard to believe."); }

            HashMap<String, Integer> nubMap = new HashMap<>();
            for (Nub nub : nubList) {
                if (nubMap.containsKey(nub.getNubName())) {
                    nubMap.put(nub.getNubName(), nubMap.get(nub.getNubName()) + 1);
                } else {
                    nubMap.put(nub.getNubName(), 1);
                }
            }
            return CommandResponse.resultResponse("nubhof", nubMap);

        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
