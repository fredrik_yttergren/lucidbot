/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.user.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.settings.PropertiesCollection;
import api.tools.collections.Params;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Collection;

public class SetKeyCommandHandler implements CommandHandler {
    private final PropertiesCollection properties;
    private final BotUserDAO botUserDAO;

    @Inject
    public SetKeyCommandHandler(final PropertiesCollection properties, BotUserDAO botUserDAO) {
        this.properties = properties;
        this.botUserDAO = botUserDAO;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            BotUser targetUser;
            BotUser user = context.getBotUser();

            if (params.containsKey("nick") && context.getBotUser().isAdmin()) {
                targetUser = botUserDAO.getUser(params.getParameter("nick"));
            } else if (params.containsKey("nick") && !context.getBotUser().isAdmin()) {
                return CommandResponse.errorResponse("You are not an admin and so you cannot set a key for another user.");
            } else { targetUser = context.getBotUser(); }

            if (targetUser == null) { return CommandResponse.errorResponse("Could not find username supplied"); }
            String key = params.getParameter("key");

            if (botUserDAO.getUserByKey(key) != null) {
                return CommandResponse.errorResponse("Key is already in use - please select a different one");
            }
            targetUser.setInGameKey(key);
            return CommandResponse.resultResponse("user", user);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
