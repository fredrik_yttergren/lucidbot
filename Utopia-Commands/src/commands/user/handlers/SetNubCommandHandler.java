/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.user.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.database.daos.BotUserDAO;
import api.database.daos.NubDAO;
import api.database.models.BotUser;
import api.database.models.Nickname;
import api.database.models.Nub;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.settings.PropertiesCollection;
import api.tools.collections.Params;
import database.daos.UserActivitiesDAO;
import database.models.UserActivities;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Date;

import static api.settings.PropertiesConfig.BCRYPT;

public class SetNubCommandHandler implements CommandHandler {
    private final BotUserDAO userDAO;
    private final NubDAO nubDAO;

    @Inject
    public SetNubCommandHandler(final BotUserDAO userDAO,
                                 final NubDAO nubDAO) {
        this.userDAO = userDAO;
        this.nubDAO = nubDAO;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            if (params.getParameter("reason") == null) {
                return CommandResponse.errorResponse("Please specify a reason that user is a nub");
            }
            BotUser setter = context.getBotUser();
            BotUser nub = userDAO.getClosestMatch(params.getParameter("nub"));
            if (nub == null) { return CommandResponse.errorResponse("Could not find user specified");
            }

            Nub newNub = nubDAO.createNub(nub.getMainNick(), params.getParameter("reason"), setter.getMainNick());
            nubDAO.save(newNub);

            return CommandResponse.resultResponse("nub", newNub);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
