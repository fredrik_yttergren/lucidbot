/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.calculator.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import api.tools.numbers.NumberUtil;
import database.daos.ProvinceDAO;
import database.models.AttackType;
import database.models.Province;
import database.models.SoT;
import spi.commands.CommandHandler;
import spi.filters.Filter;
import tools.BestMatchFinder;
import tools.CalculatedAttack;
import tools.calculators.AttackCalculator;

import javax.inject.Inject;
import java.util.Collection;

public class ConstrainedOffenseCommandHandler implements CommandHandler {
    private final BestMatchFinder bestMatchFinder;

    @Inject
    public ConstrainedOffenseCommandHandler(final BestMatchFinder bestMatchFinder) {
        this.bestMatchFinder = bestMatchFinder;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Province province = bestMatchFinder.findBestMatch(params.containsKey("nickOrProvince") ? params.getParameter("nickOrProvince").trim() : context.getBotUser().getMainNick());
            if (province == null) return CommandResponse.errorResponse("Found no matching user/province");
            if (province.getSot() == null)
                return CommandResponse.errorResponse("No SoT to calculate with for " + province.getName());

            SoT sot = province.getSot();
            int eliteSendOutPercentage;
            int defenseToKeep = (int) NumberUtil.parseDoubleWithKOrM(params.getParameter("defense"));

            if (defenseToKeep <= sot.getPmd(100)) {
                return CommandResponse.errorResponse("You can send out ALL elites and maintain the amount of defense specified: " + params.getParameter("defense"));
            }

            for (eliteSendOutPercentage = 0; defenseToKeep < sot.getPmd(eliteSendOutPercentage); eliteSendOutPercentage++) {
                if (eliteSendOutPercentage > 100) {
                    return CommandResponse.errorResponse("You do not have enough elites to maintain the amount of defense specified: " + params.getParameter("defense"));
                }
            }

            int elitesToUse = (int) (sot.getElites() * (eliteSendOutPercentage - 1) / 100.0);
            int constrainedOffense = sot.maxOffense(sot.getOffSpecs(), Math.max(elitesToUse, 0));
            int pmd = sot.getPmd(eliteSendOutPercentage - 1);
            return CommandResponse.resultResponse("constrainedOffense", constrainedOffense, "pmd", pmd, "province", province);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
