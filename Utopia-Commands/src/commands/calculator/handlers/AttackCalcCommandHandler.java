/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.calculator.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.daos.ProvinceDAO;
import database.models.AttackType;
import database.models.Province;
import spi.commands.CommandHandler;
import spi.filters.Filter;
import tools.CalculatedAttack;
import tools.calculators.AttackCalculator;

import javax.inject.Inject;
import java.util.Collection;

public class AttackCalcCommandHandler implements CommandHandler {
    private final ProvinceDAO provinceDAO;
    private final AttackCalculator attackCalculator;

    @Inject
    public AttackCalcCommandHandler(final ProvinceDAO provinceDAO, final AttackCalculator attackCalculator) {
        this.provinceDAO = provinceDAO;
        this.attackCalculator = attackCalculator;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            if (context.getBotUser() == null) return CommandResponse.errorResponse("You're not registered");
            if (provinceDAO.getProvinceForUser(context.getBotUser()) == null) CommandResponse.errorResponse("Your province isn't registered");

            Province province = provinceDAO.getClosestMatch(params.getParameter("province"));
            if (province == null) return CommandResponse.errorResponse("No province found");
            if (province.getRace() == null)
                return CommandResponse.errorResponse("No race set for that province, so it's impossible to calculate");

            if (province.getEstimatedCurrentDefense() > provinceDAO.getProvinceForUser(context.getBotUser()).getMaxOff()) return CommandResponse.errorResponse("You do not have enough offense to break");

            CalculatedAttack calculatedAttack = attackCalculator.calcAttack(AttackType.TM, province, provinceDAO.getProvinceForUser(context.getBotUser()),
                    false, false);

            return CommandResponse.resultResponse("calculatedAttack", calculatedAttack, "province", province);
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
