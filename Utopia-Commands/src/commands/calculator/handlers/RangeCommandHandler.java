package commands.calculator.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import api.tools.numbers.NumberUtil;
import database.daos.ProvinceDAO;
import database.models.Army;
import database.models.Province;
import database.models.Race;
import database.models.SoT;
import database.models.SoM;
import database.models.SoS;
import database.models.Survey;
import spi.commands.CommandHandler;
import spi.filters.Filter;
import tools.BestMatchFinder;
import tools.calculators.WPACalculator;
import tools.GameMechanicCalculator;
import tools.time.UtopiaTimeFactory;
import tools.time.UtopiaTime;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

import lombok.extern.log4j.Log4j;

@Log4j
public class RangeCommandHandler implements CommandHandler {
    private final ProvinceDAO provinceDAO;
    private final BestMatchFinder bestMatchFinder;

    @Inject
    public RangeCommandHandler(final ProvinceDAO provinceDAO, final BestMatchFinder bestMatchFinder) {
        this.provinceDAO = provinceDAO;
        this.bestMatchFinder = bestMatchFinder;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            Province province = bestMatchFinder.findBestMatch(params.isEmpty() ? context.getBotUser().getMainNick() : params.getParameter("nickOrProvince").trim());

            if (province == null) return CommandResponse.errorResponse("Found no matching user/province");

            return CommandResponse.resultResponse("province", province);

        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}