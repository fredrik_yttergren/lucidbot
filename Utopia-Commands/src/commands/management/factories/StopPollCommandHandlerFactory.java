package commands.management.factories;

import api.commands.Command;
import api.commands.CommandBuilder;
import api.commands.CommandParser;
import api.commands.ParamParsingSpecification;
import api.database.models.AccessLevel;
import api.irc.ValidationType;
import com.google.inject.Provider;
import commands.CommandTypes;
import commands.management.handlers.StopPollCommandHandler;
import spi.commands.CommandHandler;
import spi.commands.CommandHandlerFactory;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StopPollCommandHandlerFactory implements CommandHandlerFactory {
    private final Command handledCommand = CommandBuilder.forCommand("stoppoll").ofType(CommandTypes.KD_MANAGEMENT).requiringAccessLevel(AccessLevel.ADMIN).withNonDowngradableAccessLevel().build();
    private final List<CommandParser> parsers = new ArrayList<>();

    private final Provider<StopPollCommandHandler> handlerProvider;

    @Inject
    public StopPollCommandHandlerFactory(final Provider<StopPollCommandHandler> handlerProvider) {
        this.handlerProvider = handlerProvider;

        handledCommand.setHelpText("Closes a poll, meaning voting is stopped");

        ParamParsingSpecification id = new ParamParsingSpecification("id", ValidationType.INT.getPattern());
        parsers.add(new CommandParser(id));
    }

    @Override
    public Command getHandledCommand() {
        return handledCommand;
    }

    @Override
    public List<CommandParser> getParsers() {
        return Collections.unmodifiableList(parsers);
    }

    @Override
    public CommandHandler getCommandHandler() {
        return handlerProvider.get();
    }
}
