package commands.management.factories;

import api.commands.Command;
import api.commands.CommandBuilder;
import api.commands.CommandParser;
import api.commands.ParamParsingSpecification;
import api.database.models.AccessLevel;
import api.irc.ValidationType;
import com.google.inject.Provider;
import commands.CommandTypes;
import commands.management.handlers.AddPollOptionCommandHandler;
import spi.commands.CommandHandler;
import spi.commands.CommandHandlerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Singleton
public class AddPollOptionCommandHandlerFactory implements CommandHandlerFactory {
    private final Command handledCommand = CommandBuilder.forCommand("addpolloption").ofType(CommandTypes.KD_MANAGEMENT).requiringAccessLevel(AccessLevel.ADMIN).withNonDowngradableAccessLevel().build();
    private final List<CommandParser> parsers = new ArrayList<>();

    private final Provider<AddPollOptionCommandHandler> handlerProvider;

    @Inject
    public AddPollOptionCommandHandlerFactory(final Provider<AddPollOptionCommandHandler> handlerProvider) {
        this.handlerProvider = handlerProvider;

        handledCommand.setHelpText("Adds a poll option users can vote for. Fails if an option with the same description is already added or the " +
                "voting has already started");

        ParamParsingSpecification id = new ParamParsingSpecification("id", ValidationType.INT.getPattern());
        ParamParsingSpecification descriptions = new ParamParsingSpecification("description", ".+");
        parsers.add(new CommandParser(id, descriptions));
    }

    @Override
    public Command getHandledCommand() {
        return handledCommand;
    }

    @Override
    public List<CommandParser> getParsers() {
        return Collections.unmodifiableList(parsers);
    }

    @Override
    public CommandHandler getCommandHandler() {
        return handlerProvider.get();
    }
}
