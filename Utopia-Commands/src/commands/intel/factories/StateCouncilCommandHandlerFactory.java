/**
 * Created by Kyle on 4/12/2017.
 */

package commands.intel.factories;

import api.commands.*;
import commands.CommandTypes;
import commands.intel.handlers.StateCouncilCommandHandler;
import spi.commands.CommandHandler;
import spi.commands.CommandHandlerFactory;
import tools.parsing.UtopiaValidationType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.inject.Provider;
import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StateCouncilCommandHandlerFactory implements CommandHandlerFactory {
    private final Command handledCommand = CommandBuilder.forCommand("statecouncil").ofType(CommandTypes.INTEL).build();
    private final List<CommandParser> parsers = new ArrayList<>();

    private final Provider<StateCouncilCommandHandler> handlerProvider;

    @Inject
    public StateCouncilCommandHandlerFactory(final Provider<StateCouncilCommandHandler> handlerProvider) {
        this.handlerProvider = handlerProvider;

        handledCommand.setHelpText("Displays the State Council for the specified user or province, if one exists");

        parsers.add(CommandParser.getEmptyParser());
        ParamParsingSpecification command = new ParamParsingSpecification("nickOrProvince", "[^(]+");
        ParamParsingSpecification kd = new ParamParsingSpecification("kd", UtopiaValidationType.LENIENT_KDLOC.getPatternString(),
                CommandParamGroupingSpecification.OPTIONAL);
        parsers.add(new CommandParser(command, kd));
    }

    @Override
    public Command getHandledCommand() {
        return handledCommand;
    }

    @Override
    public List<CommandParser> getParsers() {
        return Collections.unmodifiableList(parsers);
    }

    @Override
    public CommandHandler getCommandHandler() {
        return handlerProvider.get();
    }
}
