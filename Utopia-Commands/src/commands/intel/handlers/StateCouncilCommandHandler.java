package commands.intel.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.events.DelayedEventPoster;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import database.models.StateCouncil;
import intel.IntelManager;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import java.util.Collection;
import javax.inject.Inject;

/**
 * Created by Kyle on 4/12/2017.
 */
public class StateCouncilCommandHandler extends AbstractIntelHandler implements CommandHandler {
    private final IntelManager intelManager;

    @Inject
    public StateCouncilCommandHandler(final IntelManager intelManager) {
        this.intelManager = intelManager;
    }

    @Override
    public CommandResponse handleCommand(IRCContext context, Params params, Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        return getIntelSource(params.isEmpty() ? "" : params.getParameter("nickOrProvince").trim(), StateCouncil.class, context.getBotUser(),
                intelManager);
    }
}