/*
 * Copyright (c) 2012, Fredrik Yttergren
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name LucidBot nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Fredrik Yttergren BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package commands.activity.handlers;

import api.commands.CommandHandlingException;
import api.commands.CommandResponse;
import api.database.daos.BotUserDAO;
import api.database.models.BotUser;
import api.events.DelayedEventPoster;
import api.irc.IRCEntityManager;
import api.runtime.IRCContext;
import api.tools.collections.Params;
import api.tools.time.TimeUtil;
import database.daos.KingdomDAO;
import database.daos.ProvinceDAO;
import database.daos.UserActivitiesDAO;
import database.models.Province;
import database.models.UserActivities;
import spi.commands.CommandHandler;
import spi.filters.Filter;

import com.google.inject.Provider;
import javax.inject.Inject;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class SeenCommandHandler implements CommandHandler {
    private final BotUserDAO userDAO;
    private final UserActivitiesDAO userActivitiesDAO;
    private final IRCEntityManager entityManager;
    private final ProvinceDAO provinceDAO;
    private final KingdomDAO kingdomDAO;

    @Inject
    public SeenCommandHandler(final BotUserDAO userDAO, final UserActivitiesDAO userActivitiesDAO,
                              final IRCEntityManager entityManager, final ProvinceDAO provinceDAO,
                              final KingdomDAO kingdomDAO) {
        this.userDAO = userDAO;
        this.userActivitiesDAO = userActivitiesDAO;
        this.entityManager = entityManager;
        this.provinceDAO = provinceDAO;
        this.kingdomDAO = kingdomDAO;
    }

    @Override
    public CommandResponse handleCommand(final IRCContext context, final Params params, final Collection<Filter<?>> filters,
                                         final DelayedEventPoster delayedEventPoster) throws CommandHandlingException {
        try {
            if (params.containsKey("location")) {
                HashMap<String, String> provs = new HashMap<>();
                for (Province province : kingdomDAO.getKingdom(params.getParameter("location")).getSortedProvinces()) {
                    if (province.getLastSeen() != null) {
                        provs.put(province.getName(), TimeUtil.compareTimeToCurrent(province.getLastSeen().getTime()));
                    } else provs.put(province.getName(), "Never");
                }
                return CommandResponse
                        .resultResponse("provs", provs);
            } else if (userDAO.getClosestMatch(params.getParameter("nickOrProvince")) != null) {
                BotUser user = userDAO.getClosestMatch(params.getParameter("nickOrProvince"));
                if (user == null) return CommandResponse.errorResponse("No such user");
                boolean isOnline = entityManager.userIsOnlineMainNick(user.getMainNick());
                if (isOnline) {
                    Collection<String> nicks = entityManager.getAllOfUsersNicks(user.getMainNick());
                    return CommandResponse.resultResponse("online", isOnline, "user", user, "nicks", nicks);
                } else {
                    UserActivities userActivities = userActivitiesDAO.getUserActivities(user);
                    return CommandResponse
                            .resultResponse("user", user, "seen", TimeUtil.compareTimeToCurrent(userActivities.getLastSeen().getTime()),
                                    "online", isOnline);
                }
            } else if (provinceDAO.getClosestMatch(params.getParameter("nickOrProvince")) != null) {
                Province province = provinceDAO.getClosestMatch(params.getParameter("nickOrProvince"));
                if (province.getLastSeen() != null) {
                    return CommandResponse
                            .resultResponse("province", province, "seen", TimeUtil.compareTimeToCurrent(province.getLastSeen().getTime()));
                } else {
                    return CommandResponse
                            .resultResponse("province", province, "seen", "Never");
                }
            } else return CommandResponse.errorResponse("Could not find any user, province, or kingdom");
        } catch (final Exception e) {
            throw new CommandHandlingException(e);
        }
    }
}
